Hi Sudo, thanks for the solution. Now finally I successfully integrate the renderSDK package in my project for that i perform some changes in my project build.gradle files and your package build.gradle file also.
I'll show you what i do i simply add 
```
     implementation 'com.google.android.gms:play-services-base:15.0.1'
     implementation 'com.google.android.gms:play-services-safetynet:15.0.1'
```
these two lines in your package build.gradle file and In my build.gradle file i add multidex support with the help of 

```
 multiDexEnabled true
 implementation 'com.android.support:multidex:1.0.3'
```

these two files.

and one more thing which i do right now i am working on Window but i did't create gradle.propertices in C:\Users\{username}\.gradle\gradle.properties as you told because i am getting errors . I just add the username and password key in my project root gradle.propertices file and change 
```
  maven {
        credentials {
            // Set in ~/.gradle/gradle.properties (DON'T forget to set this)
            username getEnvValueForKey("SQUARE_REPO_USERNAME") // Repository username provided by Square
            password getEnvValueForKey("SQUARE_REPO_PASSWORD") // Repository password provided by Square
        }
        url "https://sdk.squareup.com/android"
    }
```
with
```
 maven {
            url "https://sdk.squareup.com/android"
            credentials {
                username SQUARE_READER_SDK_APPLICATION_ID
                password SQUARE_READER_SDK_REPOSITORY_PASSWORD
            }
        }
```
and i also past this code in my project root build.gradle.

Thank you.