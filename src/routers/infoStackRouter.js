import React, { Component } from "react";
import AddItemScreen from "views/AddItem/addItemScreen";
import { StackNavigator } from "react-navigation";
import AddAddonsScreen from "../views/items/itemsTabs/addAddon";

export default (StackNav = StackNavigator({
  AddItemScreen: { screen: AddItemScreen },
  AddAddonsScreen:{screen:AddAddonsScreen},
}));
