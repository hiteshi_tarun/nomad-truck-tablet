import React from 'react';
import { TabNavigator,TabBarBottom  } from 'react-navigation';
import AddItemScreen from "views/AddItem/addItemScreen";
import OrderListScreen from "views/orders/orderListScreen";
import PastOrderListScreen from "views/orders/pastOrderListScreen";
import { Icon } from "native-base";
import { Alert } from 'react-native';
import Icons from "react-native-vector-icons/FontAwesome.js";

// const routes = {
//   OrderListScreen: { screen: OrderListScreen },
//   PastOrderListScreen: { screen: PastOrderListScreen },
// }
// let iconName = 'lock';
// const configs = {
//     navigationOptions: ({ navigation }) => ({
//       tabBarIcon: ({ focused }) => (
//         focused ?
//             <Icon name={'lock'} size={24} /> :
//             <Icon name={'home'}  size={24} />),
//     //   {
//     //     let iconName = 'lock';
//     //     const { routeName } = navigation.state;
//     //     if (routeName === 'OrderListScreen') {
//     //       iconName = 'cart'
//     //     }else if (routeName === 'PastOrderListScreen') {
//     //       iconName = 'lock'
//     //     } 
//     //    {focused ?
//     //     <Icon name={iconName} size={24} iconStyle={{paddingBottom:0,paddingTop:0}} color={'white'} /> :
//     //     <Icon name={iconName}  size={24} iconStyle={{paddingBottom:0,paddingTop:0}} color={'red'} />}
//     //  } ,
//       // tabBarIcon: ({ focused, tintColor }) => {

//       //   const { routeName } = navigation.state;
//       //   // let iconName;
//       //   // if (routeName === 'Home') {
//       //   //   iconName = `ios-information-circle${focused ? '' : '-outline'}`;
//       //   // } else if (routeName === 'Settings') {
//       //   //   iconName = `ios-options${focused ? '' : '-outline'}`;
//       //   // }
//       //   // You can return any component that you like here! We usually use an
//       //   // icon component from react-native-vector-icons
//       //   //return <Ionicons name={iconName} size={25} color={tintColor} />;
//       //  <Icon name='home' color={'white'}></Icon>
//       // },
//     }),

//     tabBarOptions: {

//       activeTintColor: 'red',// Label and icon color of the active tab.
//       inactiveTintColor: 'white',// Label and icon color of the inactive tab.
//       scrollEnabled: false,     // Whether to enable scrollable tabs.
//       upperCaseLabel: true,     // Whether to make label uppercase, default is true
//       style: {backgroundColor: '#2A409A'},                // Style object for the tab bar.
//       tabStyle: {},             // Style object for the tab.
//       indicatorStyle: {},       // Style object for the tab indicator
//                                 //(line at the bottom of the tab).
//       labelStyle: {fontSize:10, fontWeight:'500'},           // Style object for the tab label.
//       iconStyle: {},            // Style object for the tab icon
//       allowFontScaling: true,    // Whether label font should scale to respect Text Size
//                                 // accessibility settings, default is true.
//       showIcon: true,
//       showLabel: true,
     
//       // default tab bar on iOS tabBarComponent : TabBarBottom
//       activeBackgroundColor: 'red',  // Background color of the active tab.
//       inactiveBackgroundColor: '#2A409A'// Background color of the inactive tab.
//     },
//     //tabBarComponent: TabBarBottom, // Component to use as the tab bar, e.g. TabBarBottom 
//                                      //(this is the default on iOS), TabBarTop (this is the default on Android).
//     tabBarPosition: 'bottom',           // default- android: top , IOS: bottom
//     animationEnabled: false,
//     swipeEnabled: false,
// }
export default TabNavigator(
  {
  OrderListScreen: { screen: OrderListScreen },
  PastOrderListScreen: { screen: PastOrderListScreen },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        // alert(tintColor)
        if (routeName === 'OrderListScreen') {
          iconName = "shopping-bag";
        } else if (routeName === 'PastOrderListScreen') {
          iconName = "check";
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Icons name={iconName} size={20} style={{color:tintColor,marginVertical:20}} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'red',// Label and icon color of the active tab.
      inactiveTintColor: 'white',// Label and icon color of the inactive tab.
      scrollEnabled: false,     // Whether to enable scrollable tabs.
      upperCaseLabel: true,     // Whether to make label uppercase, default is true
      style: {backgroundColor: '#2A409A'},                // Style object for the tab bar.
      tabStyle: {},             // Style object for the tab.
      indicatorStyle: {},       // Style object for the tab indicator
                                //(line at the bottom of the tab).
      labelStyle: {fontSize:10, fontWeight:'500'},           // Style object for the tab label.
      iconStyle: {},            // Style object for the tab icon
      allowFontScaling: true,    // Whether label font should scale to respect Text Size
                                // accessibility settings, default is true.
      showIcon: true,
      showLabel: true,

    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

// export default TabNavigator(routes,configs);
