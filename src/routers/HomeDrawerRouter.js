import React, { Component } from "react";
import Home from "views/home/";
import AddItemScreen from "views/AddItem/addItemScreen";
import EditItemScreen from "views/AddItem/editItemScreen";
import AddonsScreen from "../views/items/itemsTabs/addonScreen";
import OrderListScreen from "views/orders/orderListScreen";
import OrderDetailScreen from "views/orders/orderDetailScreen";
import SettingScreen from "views/settings/settingScreen";
import ProfileScreen from "views/settings/profileScreen";
import AddAddonsScreen from "../views/items/itemsTabs/addAddon";
import EditAddonsScreen from "../views/items/itemsTabs/editAddon";
import ItemsScreen from '../views/items/Inventory';
import SquareLoginScreen from "views/settings/settingsTabs/squareLoginScreen";
import TabsRouter from "./TabsRouter";
import { DrawerNavigator } from "react-navigation";
import DrawBar from "components/DrawBar";
const DrawNav = DrawerNavigator(
  {

    Home: { screen: Home },
    // TabsRouter: { screen: TabsRouter },
    AddAddonsScreen:{screen:AddAddonsScreen},
    EditAddonsScreen:{screen:EditAddonsScreen},
    AddItemScreen: {screen: AddItemScreen},
    ItemsScreen: { screen: ItemsScreen },
    EditItemScreen: {screen: EditItemScreen},
    SettingScreen: {screen: SettingScreen},
    AddonsScreen:{screen:AddonsScreen},
    OrderListScreen: {screen: OrderListScreen},
    OrderDetailScreen: {screen: OrderDetailScreen},
    ProfileScreen: {screen: ProfileScreen},
    SquareLoginScreen: {screen: SquareLoginScreen},
  },
  {
    contentComponent: props => <DrawBar {...props}/>,
    navigationOptions: ({navigation}) => ({
      header: null
   }),
   drawerWidth:280
  },
);

export default DrawNav;
