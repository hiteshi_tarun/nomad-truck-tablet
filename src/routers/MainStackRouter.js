import React, { Component } from "react";
import Login from "views/login/loginScreen";
import ForgetPassword from "views/forgetpass/forgetpassScreen.js";
import RegisterScreen from "views/register/registerScreen.js";
// import First from "views/first/";
import SplashScreen from "views/first/SplashScreen.js";
import Home from "views/home/";
import HomeDrawerRouter from "./HomeDrawerRouter";
import TermsScreen from "views/terms/termsScreen.js";
import { StackNavigator } from "react-navigation";
import AuthStackRouter from "./AuthStackRouter";
import infoStackRouter from "./infoStackRouter";

HomeDrawerRouter.navigationOptions = ({ navigation }) => ({
  header: null
});

export default (StackNav = StackNavigator({

 
  // First: { screen: First },
  SplashScreen: {screen: SplashScreen},
  Login: { screen: Login },
  ForgetPassword: { screen: ForgetPassword },
  Register: { screen: RegisterScreen },
  Terms: { screen: TermsScreen },
  DrawStack: {
    screen: HomeDrawerRouter,
    navigationOptions: ({navigation}) => ({
      header: null
   })
  },

  authStack: {
    screen: AuthStackRouter,
    navigationOptions: ({navigation}) => ({
      header: null
   })
  },
  infoStack: {
    screen: infoStackRouter,
    navigationOptions: ({navigation}) => ({
      header: null
   }),
 }
}));
