// Native module
import React, { Component } from "react";
import { connect } from "react-redux";

// Actions
import { setUser } from "actions/user";

// Component
import forgetpassScreen from "./forgetpassScreen";

// Store props to map with current state
const mapStateToProps = (state) => {
	return {
	}
}

// Actions prop to dispatch
const mapDispatchToProps = (dispatch) => {
	return {
  }
}

// connect states and dispatchers with components
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(forgetpassScreen);
