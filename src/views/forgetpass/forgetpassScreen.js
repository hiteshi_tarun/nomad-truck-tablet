// react modules
import React, { Component } from "react";
import { Image, Dimensions, Alert, ActivityIndicator, ImageBackground, TouchableOpacity, AsyncStorage, TextInput } from "react-native";

// external modules
import { Container, Content, Form, Item, Input, Label, Button, Icon, View, Text, Thumbnail } from "native-base";

// internal components & modules
import keyStrings from '../../common/Localization';

// Orientation file
import Orientation from 'react-native-orientation';

// global and local styles
import gbStyle from "@assets/css/global";
import styles from "./styles";
// import Icon from "react-native-vector-icons/MaterialIcons.js";
// All images
import Images from '@assets/images';
import util from "../../common/Util";
const deviceWidth = Dimensions.get('window').width;
import ApiManager from "../../common/ApiManager";
import { ProgressDialog } from 'react-native-simple-dialogs';
import firebase from "react-native-firebase"
export default class forgetpassScreen extends Component {

  // Component navigation options
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      loaderVisible: false,
      height: null,
      width: null,
    }
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('height_P').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width_p').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })

  }
  forget = () => {
    firebase.auth().sendPasswordResetEmail(this.username).then(f=>{
      alert('Email end');
      this.setState({ loaderVisible: false })
      // Actions.ForgotpassowrdLink()
    })
    
  }

  _validations = () => {
    if (!this.username) {
      Alert.alert('Please enter email address');
    } else if (!util.emailPatternMatcher(this.username)) {
      Alert.alert('Please enter valid email address');
    } else {
      this.setState({ loaderVisible: true })
      this.forget();
    }
  }

  render() {
    return (<Container >
      <Content>
        <ImageBackground source={Images.bg_p} style={{ flex: 1, width: "100%", height: this.state.height }}>

          <ProgressDialog
            visible={this.state.loaderVisible}
            message={'Please wait...'}
          />
          <View style={{ width: util.getWidth(100), height: util.getHeight(8), flexDirection: 'row' }}>
            <View style={{ width: util.getWidth(10), alignItems: 'center', justifyContent: 'center' }}>
              <TouchableOpacity onPress={() =>   this.props.navigation.navigate('Login')}>
                <Icon name="md-arrow-back" size={25}  style={{color :'#1a5177'}} />

              </TouchableOpacity>
            </View>
            <View style={{ width: "70%", alignItems: 'center', justifyContent: 'center' }}>

            </View>
       
           
               <View style={{ width: util.getWidth(20), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
             
              </View>
          </View>

          <View style={styles.logoView}>
            <Image source={Images.fullLogo} style={styles.logo} />
          </View>


          <Form style={{ marginHorizontal: '8%', marginBottom: '5%' }}>

            <Item floatingLabel >
              <Icon active name='person' style={styles.appBlueColor} />
              <Label style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14 }]}>   </Label>
              <Input
                style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14, }]}
                placeholderTextColor='#1a5177'
                placeholder="Enter you register email here"
                onChangeText={(t) => this.username = t}
                returnKeyType={"next"} />
            </Item>


          </Form>
          <TouchableOpacity style={styles.btn}
            onPress={() => this._validations()}>
            <Text style={styles.logInBtnStyle}>Send</Text>
          </TouchableOpacity>


          {/* <View style={styles.bg}>
         
        </View> */}


        </ImageBackground>
      </Content>
    </Container>);
  }


}

