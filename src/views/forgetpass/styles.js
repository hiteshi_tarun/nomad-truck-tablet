
const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import theme from "src/themes/base-theme";
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import util from '../../common/Util'
export default {
  logoView: {
    height: "40%",
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  
  },
  logo: {
    width:"55%", 
    height:"75%"
  },

  bg: {
    marginTop:"5%",
    backgroundColor:'yellow'
  },
  input: {
    marginBottom: 20,
  },
  btn: {
    height:'6%',width:'84%',justifyContent:'center',alignItems:'center',
    marginTop:'5%',
    marginHorizontal:'8%',
    backgroundColor: theme.appDefaultColor,
  
   
  },
  appBlueColor:{
    color:'#1a5177'
  },
  iconStyle:{
    width:35, height:25, marginTop:10
  },
  logInBtnStyle:{
    marginTop:'2%',
    color:'white', fontWeight:'bold',fontSize: 18,
  }
};
