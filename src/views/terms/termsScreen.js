// react modules
import React, { Component } from "react";
import { Image, Dimensions, Alert, ActivityIndicator, ImageBackground, TouchableOpacity, Platform, StyleSheet, ScrollView } from "react-native";

// external modules
import { Container, Content, Form, Item, Input, Label, Button, Icon, View, Text, Thumbnail } from "native-base";

// internal components & modules
import keyStrings from '../../common/Localization';

// Orientation file
import Orientation from 'react-native-orientation';

// global and local styles
import gbStyle from "@assets/css/global";


// All images
import Images from '@assets/images';
import util from "../../common/Util";





export default class termsScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        // const {goBack} = this.props.navigation;
        return (
            <Container>
                <ImageBackground source={Images.bg_p} style={{ flex: 1 }}>
                    <View style={{ width: util.getWidth(100), height: util.getHeight(6), flexDirection: 'row'}}>
                        <View style={{ width: util.getWidth(10), alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Icon name="md-arrow-back" size={25} color='#1a5177' />

                            </TouchableOpacity>
                        </View>
                        <View style={{ width: util.getWidth(70), alignItems: 'center', justifyContent: 'center' }}>

                        </View>
                        <View style={{ width: util.getWidth(20), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                        </View>
                    </View>
                    <View style={{ height: util.getHeight(7), justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: blueColor, fontSize: 20, fontWeight: "500" }}>Terms of Services</Text>
                    </View>
                    <View style={{ height: util.getHeight(80), alignItems: 'center', backgroundColor: 'white', marginRight: util.getHeight(3), marginLeft: util.getHeight(3), marginBottom: util.getHeight(5) }}>
                        <ScrollView>
                            <Text style={styles.headline}>
                                <Text style={styles.bold}>Nomad Mobile Device Terms of Use{"\n"}</Text>
                                <Text style={styles.headline}>Last updated: September 22, 2018</Text>
                            </Text>
                            <Text style={styles.spaced}>
                                1.	Please read these Terms of Use carefully before you download, install or use our Nomad application for mobile devices (the “App”).
                                By downloading, installing or using the App, you indicate that you accept these Terms of Use and that you agree to abide by them.
                                Your download, installation or use of the App constitutes your acceptance of these Terms of Use, which takes effect on the date on which you download,
                                install or use the App. If you do not agree with these Terms of Use, you should cease downloading, installing or using the App immediately.
</Text>
                            <Text style={styles.spaced}>
                                2.	The App is operated by Bienville Software Company (and we refer to ourselves as “we”, “us” or “our”). We own and operate the App on our own behalf.
</Text>
                            <Text style={styles.spaced}>
                                3.	We reserve the right to change these Terms of Use at any time without notice to you by posting changes on the www.orderwithnomad.com website (the “Website”)
                                or by updating the App to incorporate the new terms of use. You are responsible for regularly reviewing information posted online to obtain timely notice of such changes.
                                Your continued use of the App after changes are posted constitutes your acceptance of the amended Terms of Use.
</Text>
                            <Text style={styles.spaced}>
                                4.	To download, install, access or use the App, you must be 16 years of age or over. If you are under 16 and you wish to use download, install, access or use the App,
                                you must get consent from your parent or guardian before doing so.
</Text>
                            <Text style={styles.spaced}>
                                <Text>
                                    5.	We operate the software underlying and required for your use of the App from the United States of America and it is possible that some downloads from the
                                    App could be subject to government export controls or other restrictions. If you download anything from or use the App, you represent that you are not subject
                                    to such controls or restrictions. We make no representation that anything is appropriate, permissible or available for use outside the United States, and using
                                    the App from territories in which such use or the information available from such use is illegal, restricted or not permitted, is expressly prohibited. If you
                                    choose to access or use the App from or in locations outside of the United States, you do so on your own initiative and are responsible for:
  </Text>
                                <Text style={styles.bullets}>
                                    <Text>a)	ensuring that what you are doing in that country is legal; and</Text>
                                    <Text>b)	the consequences and compliance by you with all applicable laws, regulations, bylaws, codes of practice, licenses, registrations, permits and authorizations
    (including any laws that relate to businesses providing services). </Text>
                                    <Text>c)	all access to the App through your mobile device and for bringing these Terms of Use to the attention of all such persons.</Text>
                                </Text>
                            </Text>
                            <Text style={styles.spaced}>
                                6.	Use of the App does not include the provision of a mobile device or other necessary equipment to access it. To use the App you will require Internet connectivity and appropriate telecommunication links. We shall not have any responsibility or liability for any mobile phone or other charges and costs you may incur.
</Text>

                            <Text style={styles.spaced}>
                                <Text>7.	You shall not in any way use the App or submit to us or to the App or to any user of the App anything which in any respect:{"\n"}</Text>
                                <Text>a)	is in breach of any law, statute, regulation or bylaw of any applicable jurisdiction;{"\n"}</Text>
                                <Text>b)	is fraudulent, criminal or unlawful;{"\n"}</Text>
                                <Text>c)	is inaccurate or out-of-date;{"\n"}</Text>
                                <Text>d)	may be obscene, indecent, pornographic, vulgar, profane, racist, sexist, discriminatory, offensive, derogatory, harmful,
  harassing, threatening, embarrassing, malicious, abusive, hateful, menacing, defamatory, untrue or political;{"\n"}</Text>
                                <Text>e)	impersonates any other person or body or misrepresents a relationship with any person or body;{"\n"}</Text>
                                <Text>f)	may infringe or breach the copyright or any intellectual property rights (including without limitation copyright, trademark rights and broadcasting rights)
  or privacy or other rights of us or any third party;{"\n"}</Text>
                                <Text>g)	may be contrary to our interests;{"\n"}</Text>
                                <Text>h)	is contrary to any specific rule or requirement that we stipulate on the App in relation to a particular part of the App or the App generally; or{"\n"}</Text>
                                <Text>i)	involves your use, delivery or transmission of any viruses, unsolicited emails, trojan horses, trap doors, back doors, easter eggs, worms, time bombs,
                                  cancelbots or computer programming routines that are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data
    or personal information.{"\n"}</Text>
                            </Text>
                            <Text style={styles.spaced}>
                                8.	You agree not to reproduce, duplicate, copy or re-sell the App or any part of the App save as may be permitted by these Terms of Use.
  </Text>
                            <Text style={styles.spaced}>
                                <Text>9.	You agree not to access without authority, interfere with, damage or disrupt:{"\n"}</Text>
                                <Text>a)	any part of the App;{"\n"}</Text>
                                <Text>b)	any equipment or network on which the App is stored;{"\n"}</Text>
                                <Text>c)	any software used in the provision of the App; or{"\n"}</Text>
                                <Text>d)	any equipment or network or software owned or used by any third party.{"\n"}</Text>
                            </Text>
                            <Text style={styles.spaced}>
                                10.	Commentary and other materials available on the App are not intended to amount to advice on which reliance should be placed. Subject to Paragraph 16 below, we therefore disclaim all liability and responsibility arising from any reliance placed on such materials by any user of the App, or by anyone who may be informed of any of its contents.
</Text>
                            <Text style={styles.spaced}>
                                11.	You assume sole responsibility for results obtained from the use of the App, and for conclusions drawn from such use. We shall have no liability for any damage caused by errors or omissions in any information contained in the App.
</Text>
                            <Text style={styles.spaced}>
                                12.	You agree to comply at all times with any instructions for use of the App which we make from time to time.
</Text>
                            <Text style={styles.spaced}>
                                13.	If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential, and you must not disclose it to any third party. We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our opinion you have failed to comply with any of the provisions of these Terms of Use.
</Text>
                            <Text style={styles.spaced}>
                                14.	Availability of the App, Security \& Accuracy
                                a)	We make no warranty that your access to the App will be uninterrupted, timely or error-free. Due to the nature of the Internet, this cannot be guaranteed. In addition, we may occasionally need to carry out repairs, maintenance or introduce new facilities and functions.
                                b)	Access to the App may be suspended or withdrawn to or from you personally or all users temporarily or permanently at any time and without notice. We may also impose restrictions on the length and manner of usage of any part of the App for any reason. If we impose restrictions on you personally, you must not attempt to use the App under any other name or user or on any other mobile device.
                                c)	We do not warrant that the App will be compatible with all hardware and software which you may use. We shall not be liable for damage to, or viruses or other code that may affect, any equipment (including but not limited to your mobile device), software, data or other property as a result of your download, installation, access to or use of the App or your obtaining any material from, or as a result of using, the App. We shall also not be liable for the actions of third parties.
                                d)	We may change or update the App and anything described in it without notice to you. If the need arises, we may suspend access to the App, or close it indefinitely.
                                e)	We make no representation or warranty, express or implied, that information and materials on the App are correct, no warranty or representation, express or implied, is given that they are complete, accurate, up-to-date, fit for a particular purpose and, to the extent permitted by law, we do not accept any liability for any errors or omissions. This shall not affect any obligation which we may have under any contract that we may have with you to provide you with products.
</Text>
                            <Text style={styles.spaced}>
                                15.	Independence from Platforms
a)	The App is independent of any platform on which it is located. The App is not associated, affiliated, sponsored, endorsed or in any way linked to any platform operator, including, without limitation, Apple, Google, Android or RIM Blackberry (each being an “Operator”).{"\n"}
                                b)	Your download, installation, access to or use of the App is also bound by the terms and conditions of the Operator.{"\n"}
                                c)	You and we acknowledge that these Terms of Use are concluded between you and us only, and not with an Operator, and we, not those Operators, are solely responsible for the App and the content thereof to the extent specified in these Terms of Use.{"\n"}
                                d)	The license granted to you for the App is limited to a non-transferable license to use the App on a mobile device that you own or control and as permitted by these Terms of Use.{"\n"}
                                e)	We are solely responsible for providing any maintenance and support services with respect to the App as required under applicable law. You and we acknowledge that an Operator has no obligation whatsoever to furnish any maintenance and support services with respect to the App.{"\n"}
                                f)	In the event of any failure of the App to conform to any applicable warranty, you may notify the relevant Operator and that Operator will refund the purchase price for the App (if any purchase price has been paid) to you; and, to the maximum extent permitted by applicable law, that Operator will have no other warranty obligation whatsoever with respect to the App, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be our sole responsibility.{"\n"}
                                g)	You and we acknowledge that we, not the relevant Operator, are responsible for addressing any claims of you or any third party relating to the App or your possession and/or use of the App, including, but not limited to: (i) any claim that the App fails to conform to any applicable legal or regulatory requirement; and (ii) claims arising under consumer protection or similar legislation.{"\n"}
                                h)	You and we acknowledge that, in the event of any third party claim that the App or your possession and use of the App infringes that third party’s intellectual property rights, we, not the relevant Operator, will be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim; provided such infringement was caused by us.{"\n"}
                                i)	You must comply with any applicable third party terms of agreement when using the App (e.g. you must ensure that your use of the App is not in violation of your mobile device agreement or any wireless data service agreement).{"\n"}
                                j)	You and we acknowledge and agree that the relevant Operator, and that Operator’s subsidiaries, are third party beneficiaries of these Terms of Use, and that, upon your acceptance of these Terms of Use, that Operator will have the right (and will be deemed to have accepted the right) to enforce these Terms of Use against you as a third party beneficiary thereof.{"\n"}
                            </Text>
                            <Text style={styles.spaced}>
                                16.	Limitation of Liability{"\n"}
                                a)	You hereby release Bienville Software Company, its officers, directors, agents, and employees from all claims, demands, and damages
                                (actual and consequential) of any kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of,
or in any way, connected with any disputes arising between you and any suppliers, or between you and other App users.{"\n"}
                                b)	YOU ASSUME ALL RESPONSIBILITY AND RISK WITH RESPECT TO YOUR USE OF THE APP. THE APP IS AVAILABLE “AS IS,” AND “AS AVAILABLE”. YOU UNDERSTAND
                                AND AGREE THAT, TO THE FULLEST EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, REPRESENTATIONS AND ENDORSEMENTS, EXPRESS OR IMPLIED, WITH REGARD
                                TO THE APP, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
                                WE DO NOT WARRANT USE OF THE APP WILL BE UNINTERRUPTED OR ERROR-FREE OR THAT ERRORS WILL BE DETECTED OR CORRECTED. WE DO NOT ASSUME ANY LIABILITY
                                OR RESPONSIBILITY FOR ANY COMPUTER VIRUSES, BUGS, MALICIOUS CODE OR OTHER HARMFUL COMPONENTS, DELAYS, INACCURACIES, ERRORS OR OMISSIONS, OR THE ACCURACY,
                                COMPLETENESS, RELIABILITY OR USEFULNESS OF THE INFORMATION DISCLOSED OR ACCESSED THROUGH THE APP. WE HAVE NO DUTY TO UPDATE OR MODIFY THE APP AND WE ARE NOT
                                LIABLE FOR OUR FAILURE TO DO SO. IN NO EVENT, UNDER NO LEGAL OR EQUITABLE THEORY (WHETHER TORT, CONTRACT, STRICT LIABILITY OR OTHERWISE), SHALL WE OR ANY OF OUR
                                RESPECTIVE EMPLOYEES, DIRECTORS, OFFICERS, AGENTS OR AFFILIATES, BE LIABLE HEREUNDER OR OTHERWISE FOR ANY LOSS OR DAMAGE OF ANY KIND, DIRECT OR INDIRECT,
                                IN CONNECTION WITH OR ARISING FROM THE APP, THE USE OF THE APP OR OUR AGREEMENT WITH YOU CONCERNING THE APP, INCLUDING, BUT NOT LIMITED TO, COMPENSATORY,
                                DIRECT, CONSEQUENTIAL, INCIDENTAL, INDIRECT, SPECIAL OR PUNITIVE DAMAGES, LOST ANTICIPATED PROFITS, LOSS OF GOODWILL, LOSS OF DATA, BUSINESS INTERRUPTION,
                                ACCURACY OF RESULTS, OR COMPUTER FAILURE OR MALFUNCTION, EVEN IF WE HAVE BEEN ADVISED OF OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES. IF WE ARE
                                HELD LIABLE TO YOU IN A COURT OF COMPETENT JURISDICTION FOR ANY REASON, IN NO EVENT WILL WE BE LIABLE FOR ANY DAMAGES IN EXCESS OF ONE HUNDRED FIFTY DOLLARS
                                (US$150.00). SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATION OR
                                EXCLUSION MAY NOT APPLY TO YOU. IF ANY LIMITATION ON REMEDIES, DAMAGES OR LIABILITY IS PROHIBITED OR RESTRICTED BY LAW, WE SHALL REMAIN ENTITLED TO THE MAXIMUM
DISCLAIMERS AND LIMITATIONS AVAILABLE UNDER THIS AGREEMENT, AT LAW AND/OR IN EQUITY.{"\n"}
                            </Text>
                            <Text style={styles.spaced}>
                                17.	Your Representations and Warranties.{"\n"}You represent and warrant that (a) your use of the App will be in strict accordance with these Terms of Use and with all applicable laws and regulations, including without limitation any local laws or regulations in your country, state, city, or other governmental area, regarding online conduct and acceptable content, and regarding the transmission of technical data exported from the United States or the country in which you reside and (b) your use of the App will not infringe or misappropriate the intellectual property rights of any third party.
</Text>
                            <Text style={styles.spaced}>
                                18.	Indemnification{"\n"}You agree to indemnify and hold Bienville Software Company, and each of our affiliates, successors and assigns, and their respective officers, directors, employees, agents, representatives, licensors, advertisers, suppliers, and operational service providers harmless from and against any and all losses, expenses, damages, costs and expenses (including attorneys’ fees), resulting from your use of the App and/or any violation of these Terms of Use. We reserve the right to assume the exclusive defense and control of any demand, claim or action arising hereunder or in connection with the App and all negotiations for settlement or compromise. You agree to fully cooperate with us in the defense of any such demand, claim, action, settlement or compromise negotiations, as requested by us.
</Text>
                            <Text style={styles.spaced}>
                                19.	Trade Marks.{"\n"}The “Nomad” name and logos and all related names, trademarks, service marks, design marks and slogans are the trademarks or service marks of ours or our licensors.
</Text>
                            <Text style={styles.spaced}>
                                20.	Intellectual Property Rights{"\n"}
                                a)	As between you and us, we are the sole and exclusive owner or the licensee of all intellectual property rights in the App, and in the material published on it.
Those works are protected by copyright and trademark laws and treaties around the world. All such rights are reserved.{"\n"}
                                b)	You must not use any part of the materials on the App for commercial purposes without obtaining a license to do so from us or our licensors.{"\n"}
                                c)	Information About You & Your Use of the App. We process information about you in accordance with our Privacy Policy, which is available on our website
at www.orderwithnomad.com. By using the App, you consent to such processing and you warrant that all data provided by you is accurate.{"\n"}
                            </Text>
                            <Text style={styles.spaced}>
                                21.	Third Party Websites{"\n"}
                                a)	We have no control over and accept no responsibility for the content of any website or mobile application to which a link from the App
                                exists (unless we are the provider of those linked websites or mobile applications). Such linked websites and mobile applications are provided “as is”
                                for your convenience only with no warranty, express or implied, for the information provided within them. We do not provide any endorsement or recommendation
                                of any third party website or mobile application to which the App provides a link. The terms and conditions, terms of use and privacy policies of those third
                                party websites and mobile applications will apply to your use of those websites and mobile applications and any orders you make for goods and services via
                                such websites and mobile applications. If you have any queries, concerns or complaints about such third party websites or mobile applications
                                (including, but not limited to, queries, concerns or complaints relating to products, orders for products, faulty products and refunds) you must direct them to the
operator of that third party website or mobile application.{"\n"}
                                b)	You must not without our permission:{"\n"}
                                i.	use or copy any material from the App, including, but not limited to, onto other websites or in other mobile applications; or{"\n"}
                                ii.	frame any of the App onto your own or another person’s website or mobile application.{"\n"}
                            </Text>
                            <Text style={styles.spaced}>
                                22.	Severability{"\n"}If any of these terms should be determined to be illegal, invalid or otherwise unenforceable by reason of the laws of any state or country in which these terms are intended to be effective, then to the extent and within the jurisdiction which that term is illegal, invalid or unenforceable, it shall be severed and deleted and the remaining Terms of Use shall survive, remain in full force and effect and continue to be binding and enforceable.
</Text>
                            <Text style={styles.spaced}>
                                23.	Non-assignment{"\n"}You shall not assign or transfer or purport to assign or transfer the contract between you and us to any other person.
  </Text>
                            <Text style={styles.spaced}>
                                <Text>24.	Exclusion{"\n"}
                                    Except as expressly stated in these Terms of Use, all warranties and conditions, whether express or implied by statute,
    common law or otherwise are hereby excluded to the extent permitted by law.</Text>
                            </Text>
                            <Text style={styles.spaced}>
                                <Text>25.	Miscellaneous{"\n"}</Text>
                                <Text>
                                    a)	These Terms of Use (and our Privacy Policy, and any other document referred to in these Terms of Use and any other terms and conditions
                                    specifically agreed between you and us in writing) contain all the terms agreed between us and you regarding their subject matter and supersedes
                                    and excludes any prior terms and conditions, understanding or arrangement between us and you, whether oral or in writing. No representation, undertaking or
                                    promise shall be taken to have been given or be implied from anything said or written in negotiations between us and you prior to these Terms of Use except as
                                    expressly stated in these Terms of Use. Neither us nor you shall have any remedy in respect of any untrue statement made by the other upon which that party
                                    relied in entering into these Terms of Use (unless such untrue statement was made fraudulently or was as to a matter fundamental to a party’s ability to perform
                                    these Terms of Use) and that party’s only remedies shall be for breach of contract as provided in these Terms and Conditions.
                                  
                                    b)	These Terms of Use may only be modified by a written amendment signed by an authorized executive of the Company or by the posting of a revised
                                    version by us. Except to the extent applicable law, if any, provides otherwise, this Agreement and any access to or use of the App will be governed
                                    by the laws of the state of Louisiana, excluding its conflict of law provisions. Any dispute or claim arising out of or in connection with these Terms
                                    of Use will be subject to the exclusive jurisdiction of the federal and state courts sitting in New Orleans, Louisiana. If any part of this Agreement is
                                    held invalid or unenforceable, that part will be construed to reflect the parties’ original intent, and the remaining portions will remain in full force and
                                    effect. A waiver by either party of any term or condition of this Agreement or any breach thereof, in any one instance, will not waive such term or condition
                                    or any subsequent breach thereof. You may not assign your rights under this Agreement to any party; We may assign our rights under this Agreement without condition.
                                    This Agreement will be binding upon and will inure to the benefit of the parties, their successors, and permitted assigns.
</Text>
                            </Text>
                            <Text style={styles.headline}>
                                <Text style={styles.bold}>NOMAD Privacy Policy{"\n"}</Text>
                                <Text style={styles.headline}>Last updated: September 22, 2018{"\n"}</Text>
                            </Text>
                            <Text style={styles.spaced}>
                                <Text style={styles.spaced}>
                                    <Text>
                                        This privacy policy (“Policy”) applies to the Nomad Website located at www.nomadfoodtruckapp.com (the “Site”) owned
                                        and operated by Bienville Software. Inc. (“Bienville Software” “we” or “us”) and the Nomad service accessed
                                        through our mobile application (the “service” or “App”).  The Site and App are owned and operated by Bienville
                                        Software, Inc. located at PO Box 750627, New Orleans, LA 70175, United States.
    </Text>
                                </Text>
                                <Text style={styles.spaced}>
                                    The Policy is designed to tell you how we collect and use personal information (as defined below) so you can make an
                                    informed decision about using our Site and App. Please read this statement before submitting any personal information to us.
By using our Site and service, you represent that you:{"\n"}
                                    •	are 16 years old or older,{"\n"}
                                    •	are using our App for lawful purposes,{"\n"}
                                    •	consent to the information collection, use and retention practices described in this Policy.{"\n"}{"\n"}

                                    <Text style={styles.spaced}>What is personal information?{"\n"}
                                        <Text>As used herein, the term “personal information” means information that identifies an individual,
                                          or from which an individual could be identified (such as a name, address, telephone number, mobile number,
                                          email address, credit card number or other account number), and information about that individual’s location or activities,
                                          such as information about his or her use of the Nomad service. Personal information also includes demographic information
                                          such as date of birth, gender, geographic area and preferences when such information is linked to other personal information
                                          that identifies you.
                                          Personal information does not include “aggregate” information, which is data we collect about the use of the Site or service or
                                          categories of Site and service users, from which any personal information has been removed.  We collect aggregate data for a
                                          number of purposes, including to help us understand trends and user needs so that we can better consider new publications,
                                          products and services, and tailor existing publications, products and services to user desires. This Policy in no way limits or
                                          restricts our collection of aggregate information.
                                          What personal information do we collect?
                                          Personal information may be collected in a number of ways when you use the Site and service. At several places on our Site or
                                          within our mobile application, we may collect certain information you voluntarily provide to us which may contain personal
                                          information. If you register for the mobile application we may collect your name, email address, phone number, credit card
                                          number, and any other demographic information that you provide.
                                          We will never post to any social network or contact anyone on your behalf without your permission.
                                          When turned on, the App will automatically collect location information from your mobile device and/or your mobile carrier.
                                          Your personal location information will only be associated with your personal information    by Nomad until you check in
                                          from another location. . If you no longer wish to allow us to collect and use your location information, you may discontinue
                                          the use of the service.
</Text></Text>
                                </Text>
                                <Text style={styles.spaced}>
                                    We treat the information collected in conjunction with the app as personal information if we combine it with or link it to any of the identifying information mentioned above.
                                    Tracking Technologies
                                    Bienville Software and its partners use cookies or similar technologies to analyze trends, administer the website, track users’ movements around the website, and to gather demographic information about our user base as a whole. You can control the use of cookies at the individual browser level, but if you choose to disable cookies, it may limit your use of certain features or functions on our Site or service.
                                    Behavioral Advertising / Re-Targeting
                                    We may partner with a third party to either display advertising on our Site or within our app or to manage advertising on other sites or within other apps. Our third-party partner may use technologies such as cookies to gather information about your activities within our App and within other apps in order to provide you advertising based upon your browsing activities and interests.
                                    Log Files
                                    As true of most applications, we gather certain information automatically and store it in log files.  We do not link this automatically collected data to other information we collect about you.
                                    Mobile Analytics
                                    We use mobile analytics software to allow us to better understand the functionality of our Mobile Software on your phone. This software may record information such as how often you use the App, the events that occur within the App, aggregated usage, performance data, and from where the application was downloaded. We do not link the information we store within the analytics software to any personally identifiable information you submit within the App.
                                    Push Notifications
                                    We may send you push notifications from time-to-time in order to update you about any updates, events or promotions that we may be running. If you do not wish to receive these types of communications, you may turn them off at the device level. To ensure you receive proper notifications, we will need to collect certain information about your device such as operating system and user identification information.
                                    Usage and Disclosure
                                    In general, we use personal information we may collect (such as your name, location or social network information) to facilitate your use of the mobile App and Nomad service,
                                    to provide you with information, products and services you request, to administer and assist us with the operation of the Site, our app, and for any other purpose for which
the information was provided. For example, we may use the information we collect:{"\n"}
                                    •	to respond to your emails, submissions, comments, requests or complaints;{"\n"}
                                    •	to ensure that the application is working correctly and that you are being served the most recent version;{"\n"}
                                    •	to show where you and other users are for purposes of our food truck clients. To be clear, this information shared with food truck
clients would not include your personal information.{"\n"}
                                    •	to request feedback and to enable us to develop, customize and improve the Site and our information and services; and{"\n"}
                                    •	for the specific purpose for which the information was provided.{"\n"}
                                </Text>{"\n"}
                                <Text style={styles.spaced}>
                                    In addition, if you so indicate in the event of sharing your contact information with us, we may use the information we collect to send you news and product and service updates, including contacting you about our services, products, activities, special events or offers and for other marketing, informational, product development and promotional purposes, including notifications about new features and or improvements to the Site and services.
                                    We may also receive information from third parties with whom we have business relationships, and may combine this information with the other personal information we have about you. In such cases, this Policy governs our use and disclosure of such information.
                                    What personal information do we share with third parties?
                                    We do not sell, share, or rent any personal information to third parties in any ways different from what is disclosed in this Policy. However, we may provide other non-personal information and aggregate information to third parties without your authorization.
                                    With regard to SMS functionality, when Bienville Software sends location information from your mobile device to food trucks , your name and phone number will be provided to them along with such additional identifying information as you choose.  In the event that we engage or partner with third party vendors, consultants or other service providers in connection with the operation of the Site and our services (“Service Providers”), we may share personal information with such Service Providers who need access to such information to carry out their work for us. Service Providers include network operations and service monitoring (NOC) providers, server hosting companies, customer support service providers, and analytics services.  In addition, we may offer various features through a third party service provider. When you use these services, you would be providing information directly to the provider of these services, and the provision of such information would be subject to such party’s own privacy policy. We are not responsible for any information you provide to these parties directly, and we encourage you to become familiar with their practices before disclosing information directly to such third parties with which you come into contact.
                                    We also may disclose personal information when we are required to comply with the law (e.g. a lawful subpoena, warrant or court order); to enforce or apply this privacy policy or our other policies or agreements; to initiate, render, bill, and collect for amounts owed to us; to protect our or our customers’ rights, property or safety; to protect our customers from fraudulent, abusive, or unlawful use of our Site; or if we believe that an emergency involving the danger of death or serious physical injury to any person requires disclosure of communications or justifies disclosure of personal information. In addition, information about our customers, including personal information, may be disclosed as part of any merger, acquisition, debt financing, sale of company assets, as well as in the event of an insolvency, bankruptcy or receivership in which personally identifiable information could be transferred to third parties as one of the business assets of Bienville Software.
                                    Please note: The Site and service may provide links to other sites, or other interactive forums hosted by a third party. Please be aware that although we may participate in or utilize such other sites, we are not responsible for the privacy practices of such other sites. We encourage you to be aware when you leave our Site and to read the privacy policies of any Website that collects personally identifiable information. Similarly, if you entered the Site through another Web site, we are also not responsible for the privacy practices of that site, and you should review the privacy policy of the originating site before providing any personal information to that site. This privacy policy applies solely to information collected by us.
  </Text>
                                <Text style={styles.spaced}>
                                    Where is your information stored?{"\n"}
                                    Bienville Software, Inc. is based in the United States of America, and any information which we collect about you or which you provide to us may therefore be transferred to the United States. Some of our Service Providers may also be located in the United States or in other countries.
                                    What steps do we take to protect your information online?
                                    We endeavor to secure your personal information from unauthorized access, use or disclosure by putting into place measures to safeguard the information we collect. Please be aware, however, that despite our efforts, no security measures are perfect or impenetrable, and that we cannot guarantee that the Site or services will be free of any defects or flaws that affect your use of it or the security of your information.
                                    When you enter sensitive information or we automatically collect sensitive information on our forms, we encrypt the transmission of that information using secure socket layer technology (SSL).
                                    What choices do you have regarding the retention and use of your information?
                                    If you use the features within the app that allow you to designate a particular business as a “favorite,” the app will associate the designation with the selected business(es) within the app. It is not necessary to use the “favorites” feature to use the app. You may change your designated “favorites” or otherwise delete a “favorite” within the app at your discretion.
                                    If you provide us with your contact details as part of your use of the Site or the App, we may send you news and product updates relating to Nomad.  You may “opt out” of receiving marketing or promotional email from Bienville Software by following unsubscribe instructions or using unsubscribe links within communications we send. Please note, however, that as long as you receive the Nomad service, you may not “opt out” of receiving service-related emails from Bienville Software.
</Text>{"\n"}
                                <Text style={styles.spaced}>
                                    How can you correct and update your personal information or obtain additional information?
                                    Upon request, we will inform you of the specific personal information we hold. If you have any questions or comments about this Policy or the practices relating to this Site or our service, please contact us at privacy@nomadfoodtruckapp.com. . If you wish to verify, correct or delete any of your personal information that may be collected by Nomad, please email your request to privacy@nomadfoodtruckapp.com. All requests sent to privacy@followwhatyoucrave.com will be processed within a reasonable timeframe. Note, however, that if we delete the personal information we have on file for you, you will not be able to continue to receive the Nomad service.
</Text>{"\n"}
                                <Text style={styles.spaced}>
                                    Data Retention{"\n"}
                                    Any information supplied by you in connection with a Nomad user account will be retained as long as your account is active or as needed to provide you services.
                                    If you created a user account with Nomad for your mobile device and you wish to delete it, you can click the “delete my account” button within the application.
                                    We may retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
                                    Bienville Software may be required to disclose personal information in response to lawful requests by public authorities, including to meet national security or law enforcement requirements.
</Text>{"\n"}
                                <Text style={styles.spaced}>
                                    Facebook Connect or other OpenID providers{"\n"}
                                    You can log in to our mobile application using sign-in services such as Facebook Connect or an OpenID provider. These services will authenticate your identity and provide you the option to share certain personal information with us such as your name and email address to pre-populate our sign-up form. Services like Facebook Connect give you the option to post information about your activities and locations on this Site to your profile page to share with others within your network.
</Text>

                                <Text style={styles.spaced}>{"\n"}
                                    Social Media Features{"\n"}
                                    Our Site and App may include Social Media Features, such as links to Facebook or Twitter.  These features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the Feature to function properly.  Social Media Features are hosted by third parties.  Your interactions with these Features are governed by the privacy policy of the company providing it.
    {"\n"}Changes in this Privacy Statement
    {"\n"}We may update this privacy statement to reflect changes to our information practices. If we make any material changes we will notify you by posting the revised privacy policy on our Site or, if you have established a Nomad user account, we will send a notification to the email address specified in your account. Any revisions to our privacy policy will become effective after such notification. We encourage you to periodically review this page for the latest information on our privacy practices.
    {"\n"}<Text style={styles.bold}>Contact Us:</Text>
                                    {"\n"}Bienville Software, Inc.
    {"\n"}PO Box 750627
    {"\n"}New Orleans, LA 70175
    {"\n"}privacy@nomadfoodtruckapp.com
  </Text>
                            </Text>
                        </ScrollView>
                    </View>

                </ImageBackground>
            </Container>);
    }


}

const styles = StyleSheet.create({
    headline: {
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 15,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        textAlign: 'left',
        flexDirection: 'column',
    },
    bold: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    italic: {
        fontWeight: '400',
        fontSize: 12,
    },
    spaced: {
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 15,
    },
    bullets: {
        marginLeft: 15,
        flexDirection: 'column',
    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: 'black',
        margin: 10,
    }
});
