// react modules
import React, { Component } from "react";
import { Image,  Alert,  ImageBackground, TouchableOpacity, AsyncStorage } from "react-native";

// external modules
import { Container, Content, Form, Item, Input, Label, Icon, View, Text, } from "native-base";

// internal components & modules
import keyStrings from '../../common/Localization';

// Orientation file
import Orientation from 'react-native-orientation';


import styles from "./styles";

// All images
import Images from '@assets/images';
import util from "../../common/Util";

import ApiManager from "../../common/ApiManager";
import { ProgressDialog } from 'react-native-simple-dialogs';

export default class loginScreen extends Component {

  // Component navigation options
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      loaderVisible: false,
      height: null,
      width: null,
    }
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('height_P').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width_p').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })

  }

  // Component methods
  login = () => {
    var fbauth = util.fireBase.firebaseLogin(this.username, this.password);
    fbauth.then((resp) => {


      console.log('login nomad ', resp);
      var uId = util.fireBase.firebaseUid();
      console.log('Get Logged User Firebase uId  ', uId)
      //   this.props.navigation.navigate("DrawStack")
      ApiManager._getAuthToken(this.username, uId)
        .then((response) => response.json())
        .then(res => {

          this.setState({ loaderVisible: false })

          console.log('get Auth Token signup ', res);
          if (res.status == 200) {
            console.log('AuthToken signup', res.status);
            console.log('AuthToken signup ', res.data.id_token);
            console.log('TruckId signup ', res.data.id_token);
            util.setStore('AuthToken', res.data.id_token);
            util.setStore('TruckId', res.data.unique_token);
            this.props.navigation.navigate("DrawStack")
          }
          else {
            alert("There is somthing wrong");
          }



        }).catch((error) => {
          console.log('Token Error ', error)
        });
    })
      .catch((error) => {
        this.setState({ loaderVisible: false });
        Alert.alert('Please enter valid credentials!');
      });
  }

  _validations = () => {
    if (!this.username) {
      Alert.alert('Please enter email address');
    } else if (!util.emailPatternMatcher(this.username)) {
      Alert.alert('Please enter valid email address');
    } else if (!this.password) {
      Alert.alert('Please enter password');
    } else {
      this.setState({ loaderVisible: true })
      this.login();
    }
  }

  render() {
    return (<Container >
      <Content>
        <ImageBackground source={Images.bg_p} style={{ flex: 1, width: "100%", height: this.state.height }}>

          <ProgressDialog
            visible={this.state.loaderVisible}
            message={'Please wait...'}
          />

          <View style={styles.logoView}>
            <Image source={Images.fullLogo} style={styles.logo} />
          </View>


          <Form style={{ marginHorizontal: '8%', marginBottom: '5%' }}>

            <Item floatingLabel >
              <Icon active name='person' style={styles.appBlueColor} />
              <Label style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14 }]}>   </Label>
              <Input
                style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14, }]}
                placeholderTextColor='#1a5177'
                placeholder={keyStrings.userNlable}
                onChangeText={(t) => this.username = t}
                returnKeyType={"next"} />
            </Item>

            <Item floatingLabel >
              <Icon active name='lock' style={styles.appBlueColor} />
              <Label style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14 }]}>  </Label>
              <Input
                secureTextEntry
                placeholder={keyStrings.passWlable}
                style={[styles.appBlueColor, { marginLeft: 5, fontSize: 14, }]}
                placeholderTextColor='#1a5177'
                onChangeText={(t) => this.password = t}
                returnKeyType={"done"} />
            </Item>
          </Form>
          <TouchableOpacity style={styles.btn}
            onPress={() => this._validations()}>
            <Text style={styles.logInBtnStyle}> {keyStrings.loginBtnText} </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:'90%',marginTop:10,alignItems:'center',justifyContent:'center'}}
          onPress={() => {
            this.props.navigation.navigate("ForgetPassword")}}>
            <Text style={{color:'#1a5177',alignSelf:'flex-end'}}>Forget password ? </Text>
          </TouchableOpacity>

          {/* <View style={styles.bg}>
         
        </View> */}

          <View style={{ flexDirection: 'row', marginTop: '15%', alignItems: 'center', width: '100%', justifyContent: 'center' }} onPress={() => {
            this.props.navigation.navigate("Register")
          }} >
            <Text style={[styles.appBlueColor, { textAlign: 'center', fontSize: 18, }]}
            > {keyStrings.noaccountText}</Text>
            <TouchableOpacity onPress={() => {
              this.props.navigation.navigate("Register")
            }}>
              <Text style={[styles.appBlueColor, { fontSize: 18, }]}
              > Sign up here</Text>
            </TouchableOpacity>
          </View>

        </ImageBackground>
      </Content>
    </Container>);
  }


}

