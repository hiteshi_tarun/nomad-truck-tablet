// react modules
import React, { Component } from "react";
import { TextInput, Alert, ImageBackground, Image, Platform, TouchableOpacity, AsyncStorage } from "react-native";
// external modules
import { Container, Content, Button, View, Text, Icon ,CheckBox} from "native-base";

// global and local styles
import gbStyle from "@assets/css/global";
import styles from "./styles";
import util from "../../common/Util"
import database from "../../common/Database";
// All images
import Images from '@assets/images';
// Orientation fix file
import Orientation from 'react-native-orientation';
// Localization content
import keyStrings from '../../common/Localization';
import ApiManager from "../../common/ApiManager";
import { ProgressDialog } from 'react-native-simple-dialogs';

import { TextInputMask } from "react-native-masked-text";
blueColor = '#1a5177';

export default class RegisterScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loaderVisible: false,
      height: null,
      width: null,
      view: 0,
      truckName: '',
      email: '',
      phone: '',
      firstName: '',
      lastName: '',
      BusinessName: '',
      street: '',
      addStreet: '',
      city: '',
      zipCode: '',
      Country: 'US',
      state: 'LA',
      password: '',
      rePassword: '',
      ein: '',
      date_of_birth: '',
      ssnNumber: '',
      BusinessOwnerFirstName: '',
      BusinessOwnerLastName: '',
      checked:false
    }
  }

  static navigationOptions = {
    header: null,
  };

  // static navigationOptions = {
  //   hearder:'none',
  //     title:  '         '+keyStrings.titleText,
  //     headerTitleStyle: { justifyContent:'center', textAlign:'center', color: blueColor, fontSize:16 }
  // };

  componentDidMount = () => {
    Orientation.lockToPortrait();
    AsyncStorage.getItem('height_P').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width_p').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })

  }

  // _getAuthToken = () => {
  //   ApiManager.getAuthentication(this.useremail, this.pword)
  //     .then(response => {
  //       console.log('Registration Auth Token ', response)
  //     }).catch((error) => {
  //       console.log('Registration Token Error ', error)
  //     });
  // }

  // Register with firebase and get Authorization token to call other api
  registration = () => {
    const { truckName, email, phone, firstName, lastName, BusinessName, street, addStreet, city,
      zipCode, Country, state, BusinessOwnerFirstName, BusinessOwnerLastName, password, rePassword, ein, date_of_birth, ssnNumber } = this.state;

    console.log( truckName, email, phone, firstName, lastName, BusinessName, street, addStreet, city,
      zipCode, Country, state, BusinessOwnerFirstName, BusinessOwnerLastName, password, rePassword, ein, date_of_birth, ssnNumber)

    this.setState({ loaderVisible: true });
    var fbAuth = util.fireBase.firebaseRegister(email, password);
    fbAuth.then((resp) => {
      console.log('firebaseRegister SignUp truck ', resp)
      var uId = util.fireBase.firebaseUid();
      console.log('Get User Id  ', uId)
      ApiManager._getAuthToken(email, uId)
        .then((response) => response.json())
        .then(res => {
          // var token = res.id_token;
          console.log('get Auth Token signUp ', res);
          if(res.status == 200){
            console.log('AuthToken signUp', res.status);
            console.log('AuthToken signUp ', res.data.id_token);
            console.log('TruckId signUp ', res.data.id_token);
            util.setStore('AuthToken', res.data.id_token);
            util.setStore('TruckId', res.data.unique_token);
            util.getValueFromStore("DeviceToken")
             .then(token => {
              ApiManager._registerUser(token,res.data.id_token,res.data.unique_token,truckName, email, phone, firstName, lastName, BusinessName, street, addStreet, city,
                zipCode, Country, state, BusinessOwnerFirstName, BusinessOwnerLastName, password, ein, date_of_birth, ssnNumber)
             
                .then(res => {
                  if(res.status == 200){
                        console.log('registration successful', res);
                        this.props.navigation.navigate("DrawStack")
                            // Update data in db register table
                  //  database.insertRegisterRecords(token, truckName, email, phone, firstName, lastName, BusinessName, street, addStreet, city,
                  //   zipCode, Country, state,BusinessOwnerFirstName,BusinessOwnerLastName, password, ein, date_of_birth, ssnNumber);
                  }
                  else{
                    console.log('registration error', res);
                  }
                  this.setState({ loaderVisible: false });
    
                }).catch((error) => {
                  alert('_registerUser Error' + error);
                  console.log('_registerUser Error ', error)
                  this.setState({ loaderVisible: false });
                });
             }).catch((error) => {
              alert('DeviceToken Error' + error);
              console.log('DeviceToken Error ', error)
              this.setState({ loaderVisible: false });
            });
           
          }
       
          // alert('TruckId signUp'+ res.data.unique_token);
       
        }).catch(error => {
          alert(error);
          this.setState({ loaderVisible: false });
        })
    })
      .catch((error) => {
        this.setState({ loaderVisible: false });
        alert('Error' + error);
      });
  }

  _signUpValidations = () => {
    console.log("ein", this.state.ein);
    if (this.state.password == "") {
      Alert.alert('Please enter password');
    } else if (this.state.password.length <= 5) {
      Alert.alert('Password length atleast 6 character');
    } else if (this.state.ein == "") {
      Alert.alert('Please enter truck TaxId');
    } else if (this.state.password != this.state.rePassword) {
      Alert.alert('Password does not match');
    } else if (this.state.date_of_birth == "") {
      Alert.alert('Please enter date_of_birth');
    } else if (this.state.ssnNumber == "") {
      Alert.alert('Please enter SSN number');
    }else if (this.state.checked == false) {
      Alert.alert('Please accept terms and condition');
    } else {
      this.setState({ loaderVisible: true });
      this.registration();
    }
  }

  _signUpValidations0 = () => {
    if (this.state.truckName == "") {
      Alert.alert('Please enter truckName');
    } else if (!util.emailPatternMatcher(this.state.email)) {
      Alert.alert('Please enter valid email address');
    } else if (this.state.phone == "") {
      Alert.alert('Please enter phone');
    } else if (this.state.firstName == "") {
      Alert.alert('Please enter truck TaxId');
    } else if (this.state.lastName == "") {
      Alert.alert('Password does not match');
    } else {
      this._nextBtn(1)
    }
  }

  _signUpValidations1 = () => {
    if (this.state.BusinessName == "") {
      Alert.alert('Please enter BusinessName');
    } else if (this.state.street == "") {
      Alert.alert('Please enter street');
    }
    // } else if (this.state.addStreet == "") {
    //   Alert.alert('Please enter addStreet');
    // } 
    else if (this.state.city == "") {
      Alert.alert('Please enter city');
    } else if (this.state.zipCode == "") {
      Alert.alert('Please enter zipCode');
    } else if (this.state.Country == "") {
      Alert.alert('Please enter Country');
    } else if (this.state.state == "") {
      Alert.alert('Please enter state');
    }  else {
      this._nextBtn(2)
    }
  }
  _nextBtn = (value) => {

    this.setState({ view: value });
  }
  _firstForm() {
    return (
      <View>
        <Text style={styles.titleText}> Tell us yours...</Text>

        <View style={{ width: util.getWidth(85) }}>

          <TextInput
            style={styles.textInput}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.truckName}
            placeholderTextColor={blueColor}
            value={this.state.truckName}
            onChangeText={truckName => this.setState({ truckName: truckName })}
            returnKeyType={'next'}
          />

          <TextInput
            style={styles.textInput}
            // underlineColorAndroid='transparent'
            keyboardType="email-address"
            placeholder={keyStrings.email}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.email}
            onChangeText={email => this.setState({ email: email })}
          />

          <TextInput
            style={styles.textInput}
            keyboardType="phone-pad"
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.phone}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.phone}
            onChangeText={phone => this.setState({ phone: phone })}
          />

          <TextInput

            style={styles.textInput}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.firstName}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.firstName}
            onChangeText={firstName => this.setState({ firstName: firstName })}
          />

          <TextInput
            style={styles.textInput}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.lastName}
            placeholderTextColor={blueColor}
            returnKeyType={'done'}
            onChangeText={lastName => this.setState({ lastName: lastName })}
          />

          <View style={styles.btnBg}>
            <Button style={styles.btnStyle}
              onPress={() => this._signUpValidations0()}>
              <Text style={{ textAlign: 'center', color: 'white' }}> {keyStrings.nextBtn} </Text></Button>
          </View>
        </View>
      </View>
    );
  }
  _secondForm() {
    console.log(this.state.BusinessName)
    return (
      <View>
        <Text style={styles.titleText}>We'd love to hear about...</Text>

        <View style={{ width: util.getWidth(85) }}>

          <TextInput
            style={[styles.textInput]}
            placeholder={keyStrings.BusinessName}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.BusinessName}
            onChangeText={BusinessName => this.setState({ BusinessName: BusinessName })}
          />

          <TextInput
            style={styles.textInput}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.street}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.street}
            onChangeText={street => this.setState({ street: street })}
          />

          <TextInput
            style={styles.textInput}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.addStreet}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            value={this.state.addStreet}
            onChangeText={addStreet => this.setState({ addStreet })}
          />
          <View style={{ flexDirection: 'row' }}>
            <TextInput

              style={[styles.textInput, { width: util.getWidth(50) }]}
              // underlineColorAndroid='transparent'
              placeholder={keyStrings.city}
              placeholderTextColor={blueColor}
              returnKeyType={'next'}
              value={this.state.city}
              onChangeText={city => this.setState({ city: city })}
            />
            <TextInput

              style={[styles.textInput, { width: util.getWidth(35) }]}
              // underlineColorAndroid='transparent'
              placeholder={keyStrings.zipCode}
              placeholderTextColor={blueColor}
              returnKeyType={'next'}
              keyboardType="phone-pad"
              onChangeText={zipCode => this.setState({ zipCode })}
            />
          </View>
          <View style={{ flexDirection: 'row' }}>
            <TextInput

              style={[styles.textInput, { width: util.getWidth(50) }]}
              // underlineColorAndroid='transparent'
              placeholder={keyStrings.Country}
              value={this.state.Country}
              editable={false} selectTextOnFocus={false}
              placeholderTextColor={blueColor}
              returnKeyType={'done'}
              onChangeText={Country => this.setState({ Country })}
            />
            <TextInput

              style={[styles.textInput, { width: util.getWidth(35) }]}
              // underlineColorAndroid='transparent'
              value={this.state.state}
              placeholder={keyStrings.state}
              editable={false} selectTextOnFocus={false}
              placeholderTextColor={blueColor}
              returnKeyType={'next'}
              onChangeText={state => this.setState({ state })}
            />
          </View>

          <View style={styles.btnBg}>
            <Button style={styles.btnStyle}
              onPress={() => this._signUpValidations1()}>
              <Text style={{ textAlign: 'center', color: 'white' }}> {keyStrings.nextBtn} </Text></Button>
          </View>
        </View>
      </View>
    );
  }
  _thiredForm() {
    return (
      <View>
        <Text style={styles.titleText}>And a little bit about you</Text>

        <View style={{ width: util.getWidth(85) }}>

          <TextInput
            style={styles.textInput}
            value={this.state.BusinessOwnerFirstName}
            // underlineColorAndroid='transparent'
            placeholder={keyStrings.BusinessOwnerFirstName}
            placeholderTextColor={blueColor}
            onChangeText={BusinessOwnerFirstName => this.setState({ BusinessOwnerFirstName })}
            returnKeyType={'next'}
          />

          <TextInput
            style={styles.textInput}
            value={this.state.BusinessOwnerLastName}
            placeholder={keyStrings.BusinessOwnerLastName}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            onChangeText={BusinessOwnerLastName => this.setState({ BusinessOwnerLastName })}
          />

          <TextInput
            style={styles.textInput}
            value={this.state.ein}
            placeholder={keyStrings.ein}
            placeholderTextColor={blueColor}
            returnKeyType={'next'}
            onChangeText={ein => this.setState({ ein: ein })}
          />
          <View style={{ flexDirection: 'row' }}>
            <TextInput

              style={[styles.textInput, { width: util.getWidth(40) }]}
              value={this.state.password}
              placeholder={keyStrings.password}
              placeholderTextColor={blueColor}
              secureTextEntry={true}
              returnKeyType={'next'}
              onChangeText={password => this.setState({ password })}
            />
            <TextInput

              style={[styles.textInput, { width: util.getWidth(45) }]}
              value={this.state.rePassword}
              placeholder={keyStrings.rePassword}
              placeholderTextColor={blueColor}
              secureTextEntry={true}
              returnKeyType={'next'}
              onChangeText={rePassword => this.setState({ rePassword })}
            />

          </View>
          <View style={{ flexDirection: 'row' }}>
            <TextInputMask
              style={[styles.textInput, { width: util.getWidth(40) }]}
              // underlineColorAndroid='transparent'
              placeholder={"YYYY-MM-DD"}
              value={this.state.date_of_birth}
              placeholder={keyStrings.date_of_birth}
              placeholderTextColor={blueColor}
              returnKeyType={'next'}
              type={"datetime"}
              options={{
                format: "YYYY-MM-DD"
              }}
              value={this.state.date_of_birth}
              onChangeText={date_of_birth => this.setState({ date_of_birth })} />

            <TextInput

              style={[styles.textInput, { width: util.getWidth(45) }]}
              value={this.state.ssnNumber}
              placeholder={keyStrings.ssnNumber}
              placeholderTextColor={blueColor}
              returnKeyType={'next'}
              keyboardType="phone-pad"
              maxLength={4}
              onChangeText={ssnNumber => this.setState({ ssnNumber })}
            />

          </View>
          <View style={{ width: '100%', alignItems: 'center', marginTop: "2%" }}>
            <View style={{ flexDirection: 'row', marginTop: "5%" }}>
              <CheckBox  checked={this.state.checked}
                onPress={()=>this.setState({checked:!this.state.checked})} color={blueColor} style={{justifyContent: 'center', alignItems: 'center'}} />
              <Text style={{ color: blueColor, fontWeight: "500", marginLeft:10 }}>  I agree with the </Text>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate("Terms")
              }}>
                <Text style={{ color: blueColor, textDecorationLine: 'underline', fontWeight: "500" }}>Terms of Services</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnBg}>
              <Button style={styles.btnStyle}
                onPress={() => this._signUpValidations()}>
                <Text style={{ textAlign: 'center', color : 'white', }}> {keyStrings.signUpBtn} </Text></Button>
            </View>
          </View>
        </View>
      </View>
    );
  }
  nextHeaderBtn = () => {
    switch (this.state.view) {
      case 0:
        this._signUpValidations0()
       //  this.setState({ view: 1 });
        break;
      case 1:
        this._signUpValidations1()
        //  this.setState({ view: 2 });
        break;

    }
  }
  backBtn() {
    switch (this.state.view) {
      case 0:
        this.props.navigation.navigate('Login');
        break;
      case 1:
        this.setState({ view: 0 });
        break;
      case 2:
        this.setState({ view: 1 });
        break;
    }
  }
  inputForm() {
    switch (this.state.view) {
      case 0:
        return (<View style={{ marginTop: '2%' }}>
          {this._firstForm()}
        </View>
        );
        break;
      case 1:
        return (<View style={{ marginTop: '3%' }}>
          {this._secondForm()}
        </View>
        );
        break;
      case 2:
        return (<View style={{ marginTop: '10%' }}>
          {this._thiredForm()}
        </View>
        );
        break;
    }

  }

  render() {
    return (
      <Container>
        <Content>
          <ImageBackground source={Images.bg_p} style={{ flex: 1, height: this.state.height, width: '100%' }}>

            <ProgressDialog
              visible={this.state.loaderVisible}
              message={'Please wait...'}
            />
            <View style={{ width: util.getWidth(100), height: util.getHeight(8), flexDirection: 'row' }}>
              <View style={{ width: util.getWidth(10), alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => this.backBtn()}>
                  <Icon name="md-arrow-back" size={25}  style={{color : '#1a5177'} }/>

                </TouchableOpacity>
              </View>
              <View style={{ width: "70%", alignItems: 'center', justifyContent: 'center' }}>

              </View>
              {this.state.view == 2 ?
                null
                : <View style={{ width: util.getWidth(20), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.nextHeaderBtn()}>
                    <Text style={{ color: '#1a5177', fontSize: 20, fontWeight: 'bold' }}>{keyStrings.nextBtn}</Text>
                  </TouchableOpacity>
                </View>}
            </View>

            <View style={{ alignItems: 'center', height: util.getHeight(92), width: util.getWidth(100) }}>

              <Image source={Images.makeImg_p} style={{ height: util.getHeight(8), width: "35%", marginBottom: util.getHeight(0.5) }}></Image>

              <View style={{
                width: util.getWidth(100) + 30, height: util.getWidth(100)
                , alignItems: 'center', justifyContent: 'center', borderRadius: util.getWidth(100) / 2, marginHorizontal: -20, borderWidth: 2, borderColor: '#84827b', backgroundColor: '#fbe48d'
              }}>
                {this.inputForm()}
              </View>
            </View>

          </ImageBackground>
        </Content>
      </Container>

    )
  }
}