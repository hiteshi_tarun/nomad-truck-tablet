const React = require("react-native");

const { StyleSheet, Dimensions } = React;
import theme from "src/themes/base-theme";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import util from '../../common/Util'

export default {
  titleText: {
    color: theme.appDefaultColor,
    textAlign: "center",
    fontSize: 22,
    marginTop: 15,
    fontWeight: "500",
    marginBottom:util.getHeight(1),
  },
  lableStyle: {
    marginTop: 20,
    fontSize: 12,
    color: theme.appDefaultColor
  },
  textInput: {
    marginTop: 10,
    padding: 12,
    color: theme.appDefaultColor,
    fontSize: 14
  },
  innerView: {
    marginTop: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  btnStyle: {
    backgroundColor: theme.appDefaultColor,
    width: util.getWidth(30),
    justifyContent: "center",
    alignSelf: "center"
  },
  btnBg: {
    height: deviceHeight * 0.15,
    justifyContent: "center"
  }
};
