// Native module
import React, { Component } from "react";
import { connect } from "react-redux";

// Actions
import { setIndex } from "actions/list";
import { openDrawer } from "actions/drawer";

// Component
import homeScreen from "./homeScreen";

// Store props to map with current state
const mapStateToProps = (state) => {
	return {
	  name: state.user.name,
	  list: state.list.list
	}
}

// Actions prop to dispatch
const mapDispatchToProps = (dispatch) => {
	return {
		setIndex: index => dispatch(setIndex(index)),
    openDrawer: () => dispatch(openDrawer())
  }
}

// connect states and dispatchers with components
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(homeScreen);
