import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Alert, Image, ImageBackground, AsyncStorage, Dimensions } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  FooterTab,
  Button,
  Footer,
} from "native-base";
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Entypo';
import Orientation from 'react-native-orientation';
import styles from "./styles";
import database from "../../common/Database";
import images from "@assets/images";
import ItemCheckoutScreen from '../homeTabs/itemCheckOutTab/itemCheckoutScreen';
import OrderTabScreen from '../homeTabs/orderTab/orderTabScreen';

export default class homeScreen extends Component {
  constructor(props) {
    super(props)
    console.log("props", this.props)
    this.state = {
      index: 0,
      comeFrom: "Nomad app",
      Height_Layout: '',
      Width_Layout: '',
      OrientationStatus: ''
    } 
  }

  switchScreen(index) {
    this.setState({
      index: index,
    })
  }

  _navigateToAddItem = status => {
    if (status == "AddItem") {
      database.fetchDataFromTable();
      this.props.navigation.navigate("AddItemScreen", {
        data: null
      });
    } else {
      this.props.navigation.navigate("AddItemScreen", {
        data: "burger"
      });
    }
  };

  componentDidMount = () => {
    Orientation.lockToLandscape();

    this.DetectOrientation();
    console.log("data_height", Dimensions.get('window').height);
    console.log("data_width", Dimensions.get('window').width);
  }
  DetectOrientation() {

    if (this.state.Width_Layout > this.state.Height_Layout) {
      console.log("Height_Layout", this.state.Height_Layout);
      // Write Your own code here, which you want to execute on Landscape Mode.
      try {
        console.log("Width_Layout", this.state.Width_Layout);
        AsyncStorage.setItem('height', JSON.stringify(this.state.Height_Layout));
        AsyncStorage.setItem('width', JSON.stringify(this.state.Width_Layout));
      } catch (error) {
        // Error saving data
      }
      this.setState({
        OrientationStatus: 'Landscape Mode'
      });
    }
    else {

      // Write Your own code here, which you want to execute on Portrait Mode.

      this.setState({
        OrientationStatus: 'Portrait Mode'
      });
    }
  }
  _viewShow = () => {

    if (this.state.OrientationStatus == 'Landscape Mode') {
      let AppComponent = null;

      switch (this.state.index) {
        case 0:
          AppComponent = ItemCheckoutScreen
          break;
        case 1:
          AppComponent = OrderTabScreen

          break;

      }
      return (
        <View style={{ width: this.state.Width_Layout, height: this.state.Height_Layout, flexDirection: 'row', }}>
          <View style={{ width: "100%", height: '100%', flexDirection: 'row' }} >
            <View style={{ width: '7%', backgroundColor: 'white', height: '100%', alignItems: 'center' }}>
              <TouchableOpacity style={{ marginTop: 3 }}
                onPress={() => this.props.navigation.openDrawer()} >
                <Icon1 active name="menu" size={35} color="#2A409A" />
              </TouchableOpacity>

              <View style={{ height: '17%', width: "70%", marginBottom: '8%', position: "absolute", bottom: 0, }}>
                <Image style={{ height: '100%', width: '80%' }} source={images.makeImg} />
              </View>

            </View>
            <View style={{ width: '93%', height: '100%', }}>
              <View style={{ width: '100%', height: '90%', backgroundColor: 'blue' }}>
                <ImageBackground source={images.bg_l} style={{ width: '100%', height: '100%' }}>

                  <View style={{ width: '100%', height: '100%' }}>
                    <AppComponent />
                  </View>

                </ImageBackground>
              </View>
              {/* <View style={{ width: '100%', height: '10%', backgroundColor: 'yellow' ,position: "absolute", bottom: 0,}}> */}
              <Footer style={{ width: '101%', height: '10%', backgroundColor: 'yellow' }}>
                <FooterTab >
                  <Button style={{ height: '100%', backgroundColor: this.state.index == 0 ? "#ffffff" : '#1a5177' }} onPress={() => this.switchScreen(0)} vertical active={this.state.index == 0}>
                    <Icon name="md-mail-open" size={25} color={this.state.index != 0 ? "#ffffff" : '#1a5177'} />

                  </Button>
                  <Button style={{ height: '100%', backgroundColor: this.state.index == 1 ? "#ffffff" : '#1a5177' }} onPress={() => this.switchScreen(1)} vertical active={this.state.index == 1}>
                    <Icon1 name="tv" size={25} color={this.state.index != 1 ? "#ffffff" : '#1a5177'} />
                  </Button>
                </FooterTab>
              </Footer>
              {/* </View> */}
            </View>
          </View>
        </View>
      )
    }
  }

  // Component render method
  render() {

    return (
      <Container style={styles.container} onLayout={(event) => this.setState({
        Width_Layout: event.nativeEvent.layout.width,
        Height_Layout: event.nativeEvent.layout.height
      }, () => this.DetectOrientation())}>

        {this._viewShow()}

      </Container>
    );
  };
}
// Component prop types
homeScreen.propTypes = {
  name: PropTypes.string,
  setIndex: PropTypes.func,
  list: PropTypes.arrayOf(PropTypes.string),
  openDrawer: PropTypes.func
};

// Component navigation options
homeScreen.navigationOptions = ({ navigation }) => ({
  title: "home",
  headerRight: (
    <Button transparent onPress={() => this.props.navigation.openDrawer()} >
      <Icon active name="menu" color={"red"} />
    </Button>
  )
});
