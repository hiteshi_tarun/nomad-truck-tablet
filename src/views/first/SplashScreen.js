// react modules
import React, { Component } from "react";
import { Image, Dimensions, ImageBackground,AsyncStorage } from "react-native";

// external modules
import { Container, View } from "native-base";

// internal components & modules
import { resetToLogin, resetToDrawNav } from 'src/navReset';
// Orientation fix file
import Orientation from 'react-native-orientation';
// global and local styles
import gbStyle from "@assets/css/global";
import styles from "./styles";

// All images
import Images from '@assets/images';
import DBHelper from '../../common/Database';
const deviceWidth = Dimensions.get('window').width;
import util from "../../common/Util";
import firebase from 'react-native-firebase';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    DBHelper.createSchema();
    this.state = {
      Height_Layout: '',
      Width_Layout: '',
      OrientationStatus:''
    } 
  }

  componentDidMount() {

    Orientation.lockToPortrait();

     var authFlag = true;
     firebase.auth().onAuthStateChanged( user => {
    //  console.log("cureent user",user);
    if(authFlag) {
      authFlag = false;
      if (user) {
        setTimeout(() => {
          console.log("already login user",user.email);
                 this.props.navigation.dispatch(resetToDrawNav)
                 // this.props.navigation.navigate('Login');
               }, 2000)
      }
      else {
        setTimeout(() => {
          console.log("no user found",);
                this.props.navigation.dispatch(resetToLogin)
              }, 2000)
      }
    }
  });
   

    // util.firbaseGetCurrentUser((user) => {
    //   console.log("user", user)
    //   if (user == null) {
    //     setTimeout(() => {
    //       this.props.navigation.dispatch(resetToLogin)
    //       // this.props.navigation.navigate('Login');
    //     }, 2000)
    //   } else {
    //     setTimeout(() => {
    //       util.getValueFromStore('AuthToken').then(res => {
    //         console.log('Auth Value at splash', res)
    //       });
    //        this.props.navigation.dispatch(resetToDrawNav)
       
    //     }, 2000)
    //   }
    // })

  }
  DetectOrientation() {

    if (this.state.Width_Layout > this.state.Height_Layout) {
    
      this.setState({
        OrientationStatus: 'Landscape Mode'
      });
    }
    else {

      // Write Your own code here, which you want to execute on Portrait Mode.
      console.log("Height_Layout",this.state.Height_Layout);
      // Write Your own code here, which you want to execute on Landscape Mode.
      try {
        console.log("Width_Layout",this.state.Width_Layout);
          AsyncStorage.setItem('height_P', JSON.stringify(this.state.Height_Layout));
          AsyncStorage.setItem('width_p',JSON.stringify(this.state.Width_Layout));
      } catch (error) {
        // Error saving data
      }
      this.setState({
        OrientationStatus: 'Portrait Mode'
      });
    }

  }
  viewshow = () => {

    if (this.state.OrientationStatus == 'Portrait Mode') {
      return(
        <ImageBackground source={Images.bg_l} style={{
          alignItems: 'center', flex: 1,
          width: this.state.Width_Layout, height: this.state.Height_Layout, justifyContent: 'center'
        }}>
          <Image source={Images.splashIcon}
            style={{
              width: "100%",
              height: "50%"
            }}></Image>

        </ImageBackground>
      )
    }}
  
  render() {
    console.log("this.props", this.props);
    return (
      <Container onLayout={(event) => this.setState({
        Width_Layout: event.nativeEvent.layout.width,
        Height_Layout: event.nativeEvent.layout.height
      }, () => this.DetectOrientation())}>
         {this.viewshow()}
      </Container>
    )
  }
}

// define component
// const SplashScreen = (props) => {
//   const {navigation} = props;

//   chekUserIsLogged = () =>{
//     setTimeout(()=>{
//     var token = util.firbaseAuthToken();
//     console.log("Token in Splash", token);
//     token.onAuthStateChanged(function(user) {
//       if (user) {
//         console.log("YES");
//       } else {
//         console.log("NO");
//       }
//     });
//     }, 5000)



// react-native@^0.55.4:
//   version "0.55.4"
//   resolved "https://registry.yarnpkg.com/react-native/-/react-native-0.55.4.tgz#fe25934b3030fd323f3ca1a70f034133465955ed"

//token = JSON.stringify(util.firbaseAuthToken()); 
//console.log("Token in Splash stringified", token)
// token.then((resp)=>{

//   console.log("resp", resp)
// }).catch((err)=>{
//   console.log("error in auth at splash", err)
// })
//Alert.alert(''+token);
// if(token._55.currentUser == null){
//    return false
// }else{
//    return true
// }
// token
// .then(()=>{
//   console.log('loggedAuth ',JSON.stringify(token));
//   Alert.alert(JSON.stringify(token));})
// .catch( (error)=>{Alert.alert(error);});
//}
// setTimeout(() => {
//   this.chekUserIsLogged()
//   console.log('chekUserIsLogged ',this.chekUserIsLogged())
//   Alert.alert(''+this.chekUserIsLogged())
//   if(this.chekUserIsLogged()){
//     navigation.navigate('DrawNav');
//   }else{
//     navigation.dispatch(resetToAuthStack)
//   }
//   var token = util.firbaseAuthToken();
//   token
//   .then(()=>{
//     Alert.alert(JSON.stringify(token));
//     console.log("Token", token)
//   })
//   .catch( (error)=>{Alert.alert(error);});

//       },2000);
// Component Variable & props

// Component methods

// Component render method
// return (
//   <Container>
//     <View style={gbStyle.container}>
//       {/* <Content> */}
//       <View style={{alignItems:'center',  flex:1,
//             alignSelf:'center' , width:"100%", height:"100%",justifyContent:'center', backgroundColor:'white'}}>
//               <Image source={Images.fullLogo} 
//               style={{alignSelf:'center', width:  deviceWidth*.8,
//               height: deviceWidth*.32,justifyContent:'center'}}></Image>
//           </View>
{/* <View style={styles.logoView}>
          <Image source={Images.logoImg}  style={styles.logo} />
          </View>
          <View style={styles.bg}>
          <Text>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec sollicitudin molestie malesuada. </Text>
            <Button
              style={styles.btn}
              onPress={() => navigation.dispatch(resetToAuthStack)}
            >
              <Text style={styles.btnText}>Get Started</Text>
            </Button>

            <Button
              style={styles.btn}
              onPress={() => navigation.dispatch(resetToDemoStack)}
            >
              <Text>Demo Pages</Text>
            </Button>

            <Button
              style={styles.btn}
              onPress={() => navigation.dispatch(resetToInfoStack)}
            >
              <Text>About App</Text>
            </Button>
          </View> */}
{/* </Content> */ }
//       </View>
//     </Container>
//   )
// }

// Component prop types
SplashScreen.propTypes = {

};

// Component navigation options
SplashScreen.navigationOptions = {
  header: null
};

export default SplashScreen;
