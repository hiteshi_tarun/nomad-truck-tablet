
const React = require('react-native');

const { StyleSheet, Dimensions } = React;
import theme from "src/themes/base-theme";
const deviceHeight = Dimensions.get('window').height;

export default {
  logoView: {
    flex: 1,
    marginTop: 50,
    alignItems: 'center'
  },
  
  logo: {
    width: 150,
    height: 150
  },
  bg: {
    flex: 1,
    padding: 20,
    marginTop: deviceHeight / 4,
    bottom: 0,
  },
  btn: {
    marginTop: 20,
    backgroundColor: theme.btnPrimaryBg,
    alignSelf: 'center',
  },
  btnText: {
    fontSize: theme.btnTextSize,
  }
};
