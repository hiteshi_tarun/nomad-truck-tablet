import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Footer,
  Icon,
  Left,
  Right,
  Body,
  CheckBox
} from "native-base";

import { View, TextInput, Image, Alert, TouchableOpacity, ImageBackground, ScrollView, Modal, AsyncStorage } from "react-native";
import global from '@assets/css/global';
import { resetToMain } from 'src/navReset';
import styles from "./styles";
import util from "../../common/Util"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import IconIonic from "react-native-vector-icons/Ionicons.js";
import Icon1 from 'react-native-vector-icons/Entypo';
comeFromStatus = ''
import images from "@assets/images";
import ApiManager from "../../common/ApiManager";
class AddItemScreen extends Component {
  constructor() {
    super();
    this.state = {
      height: null,
      deleteitems: false,
      Stock_no: '',
      prices: '',
      Description: '',
      Item_Name: '',
      Spinnervisible: false,


    }
  }
  componentDidMount() {
    AsyncStorage.getItem('height').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
  }

  hidedeletealert = () => {
    this.setState({ deleteitems: false })
  }
  onItemsPress = () => {
    this.setState({ deleteitems: true })
  }

  additem = () => {
    const { Description, Item_Name, prices, Stock_no } = this.state;

    console.log("Add item", Description, Item_Name, prices, Stock_no)
    this.setState({ Spinnervisible: true })
    var Uth='';
    util.getValueFromStore("AuthToken")
    .then(authtoken => {
      Uth=authtoken;
    util.getValueFromStore("TruckId")
      .then(token => {
        //  truckID = token;
        console.log('get All Food truckID', token);
        console.log('get All Food authtoken', authtoken);
        ApiManager._addFoodMenuItems(token,authtoken, Description, Item_Name, prices, Stock_no)
        .then(res => {
          this.setState({ Spinnervisible: false })
            console.log('Item add sucessfully', res);
            alert('Item add sucessfully');
            this.props.navigation.navigate("ItemsScreen")
           
        }).catch((error) => {
          this.setState({ Spinnervisible: false })
            console.log('Item add error', error)
            alert('Item add error' + error);
        });
     
      })
      .catch(err => {
        this.setState({ Spinnervisible: false })
        alert("TruckId token error " + err);
      })
    })
    .catch(err => {
      this.setState({ Spinnervisible: false })
      alert("AuthId token error " + err);
    });

  }

  render() {
    const { props: { name, index, list, navigation } } = this;
    return (
      <Container style={{ height: this.state.height }}>
        <Content >

          <Spinner visible={this.state.Spinnervisible} textContent={"Loading..."} color="#1a5177" overlayColor="rgba(256, 256, 256, 0.5)" textStyle={{ color: '#1a5177' }} />
          <ImageBackground source={images.bg_l} style={{ width: "100%", height: this.state.height }}>
            <Modal

              animationType="slide"
              transparent={true}
              visible={this.state.deleteitems}
              onRequestClose={() => {
                alert('Modal has been closed.');
              }}>
              <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' }}>

                <View style={{ height: util.getHeight(40), width: util.getWidth(40), backgroundColor: 'rgba(255,255,255, 0.95)', borderRadius: 10, borderWidth: 0.3, borderColor: '#7F7F7F', justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ height: '80%', width: '70%', justifyContent: 'center', alignItems: 'center' }}>

                    <Text style={{ color: '#1a5177', marginBottom: '20%', textAlign: 'center', fontSize: 18, width: "80%" }}>Are you sure you want to delete this itme?</Text>

                    <TouchableOpacity onPress={this.deleteItems} style={{ backgroundColor: '#f96467', justifyContent: 'center', alignItems: 'center', width: '90%', height: '20%', borderRadius: 10, justifyContent: 'center', alignContent: 'center', marginBottom: '2%' }}>
                      <Text style={{ color: 'white', alignContent: 'center' }}>YES</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.hidedeletealert} style={{ backgroundColor: '#c8c8c8', height: '20%', justifyContent: 'center', alignItems: 'center', width: '90%', borderRadius: 10, justifyContent: 'center', alignContent: 'center' }}>
                      <Text style={{ color: '#1a5177' }}>NO</Text>
                    </TouchableOpacity>

                  </View>

                </View>
              </View>
            </Modal>
            <View style={{ flexDirection: 'row', height: "100%" }}>
              <View style={{ width: "6%", alignItems: 'center', borderColor: "gray", borderWidth: 0.3, height: "100%", backgroundColor: 'white' }}>
                <TouchableOpacity style={{ marginTop: 3 }}
                  onPress={() => this.props.navigation.openDrawer()} >
                  <Icon1 active name="menu" size={35} color="#1a5177" />
                </TouchableOpacity>
                <View style={{ height: '13%', width: "70%", marginBottom: '8%', position: "absolute", bottom: 0, }}>
                  <Image style={{ height: '100%', width: '80%' }} source={images.makeImg} />
                </View>
              </View>
              <View style={{ width: "94%", height: "100%", }}>

                <View style={{ height: "10%", width: '100%', flexDirection: 'row' }}>
                  <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate("ItemsScreen") }} style={{ marginTop: 3 }} >
                      <IconIonic active name="md-arrow-back" size={25} color="#1a5177" />
                    </TouchableOpacity>
                  </View>
                  <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ color: '#1a5177', fontWei0ht: 'bold' }}>Add Item</Text>
                  </View>
                  <TouchableOpacity onPress={() => { this.additem() }} style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Save</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{ height: "78%", flexDirection: 'row', width: "94%", }}>
                  <Content contentContainerStyle={{}} >
                    <View
                      style={{ width: '80%', marginLeft: '20%', }}>

                      <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Item Name</Text>
                      <TextInput
                        style={[styles.textInput, {
                          marginTop: 15,

                        }]}
                        underlineColorAndroid='transparent'
                        placeholderTextColor='#2A409A'
                        onChangeText={Item_Name => this.setState({ Item_Name: Item_Name })}
                        returnKeyType={'next'}
                      />

                      <Text style={{ color: '#1a5177', fontWeight: 'bold', marginTop: '2%' }}>Description</Text>
                      <TextInput
                        style={[styles.textInput, {
                          width: "90%", height: util.getHeight(17),
                          marginTop: 15, textAlignVertical: "top", paddingVertical: "3%"
                        }]}
                        multiline={true}
                        underlineColorAndroid='transparent'
                        onChangeText={Description => this.setState({ Description: Description })}
                        returnKeyType={'next'}
                      />
                      <View style={{ flexDirection: 'row', marginTop: '2%', width: "90%", }}>
                        <View style={{ flex: 1, marginRight: '2%', }}>
                          <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Stock</Text>
                          <TextInput
                            style={[styles.textInput, {
                              width: "100%",
                              marginTop: 15
                            }]}
                            onChangeText={Stock_no => this.setState({ Stock_no: Stock_no })}
                            underlineColorAndroid='transparent'
                            placeholder=''
                            returnKeyType={'next'}
                          />
                        </View>
                        <View style={{ flex: 1, marginLeft: '2%' }}>
                          <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Price</Text>
                          <TextInput
                            style={[styles.textInput, {
                              width: "100%",
                              marginTop: 15
                            }]}
                            underlineColorAndroid='transparent'
                            onChangeText={prices => this.setState({ prices: prices })}
                            placeholder='$0.00'
                            returnKeyType={'next'}
                          />
                        </View>
                      </View>

                      <View style={{ flexDirection: 'row', marginVertical: '4%', }} >
                        <CheckBox checked={false} color='#1a5177' style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }} />
                        <Text style={{ textAlign: 'center', marginLeft: 25, color: "#1a5177" }}>Enabled</Text></View>

                      {/* <Text style={{ color: '#1a5177', fontWeight: 'bold', marginVertical: '2%' }}>Addons</Text>
                    <TextInput
                      style={[styles.textInput, {
                        width: "90%", height: util.getHeight(17), borderRadius: 5,
                        textAlignVertical: "top",
                        marginBottom: util.getHeight(15)

                      }]}
                      underlineColorAndroid='transparent'

                      returnKeyType={'next'}

                    /> */}
                    </View>
                  </Content>

                  <View
                    style={{ width: '40%', alignItems: 'center' }}>
                    <View style={styles.cameraIconBg} >
                      <Icon name="camera" Size={35} style={{ color: "#FFF" }}></Icon>
                    </View>
                  </View>

                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', height: util.getHeight(10), width: '100%', position: 'absolute', bottom: 0 }}>
                  {/* <TouchableOpacity onPress={this.onItemsPress} style={{ borderColor: "gray", borderWidth: 0.3, borderRadius: 5, justifyContent: 'center', alignItems: 'center', width: '25%', height: '60%', backgroundColor: '#f96467' }}>
                                    <Text style={{ color: 'white' }}>Delete</Text>
                                </TouchableOpacity> */}
                </View>
              </View>
            </View>
          </ImageBackground>
        </Content>
      </Container>
    );
  }
}

export default AddItemScreen;

