const React = require("react-native");

const { StyleSheet, Dimensions } = React;
import theme from "src/themes/base-theme";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import util from "../../common/Util"

export default {
 
  textInput: {
    borderColor: "gray",
    borderWidth: 0.3,
    marginTop: 5,
    borderRadius:5,
    padding: 12,
    backgroundColor:'white',
    width:'90%',
    paddingVertical:0,
    height: util.getHeight(7),
    color: theme.appDefaultColor,
    fontSize: 14
  },
  topViewBg:{
    flexDirection:'row', marginLeft:20, marginRight:15, marginTop:20, justifyContent:'center'
  },
  cameraIconBg:{
    backgroundColor:'#1a5177' , width: 150, height: 150,
    marginTop:5, marginLeft:10,
    borderRadius: 150/2,
    justifyContent:'center', alignItems:'center'
  },
  servingBg:{
    width:util.getWidth(50), 
    marginTop:5, marginLeft:10,
    justifyContent:'center', alignItems:'center'
  },
  
  innerView: {
  
  },
  titleText:{
    fontSize:10,
    marginTop:15
  },
  btnStyle: {
    backgroundColor: theme.buttonGrayBg,
    width: deviceWidth * 0.75,
    justifyContent: "center",
    alignSelf: "center"
  },
  btnBg: {
    flex: 1,
    marginTop: 10,
    height: deviceHeight * 0.15,
    justifyContent: "center"
  }
};
