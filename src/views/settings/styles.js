
const React = require('react-native');

import theme from "src/themes/base-theme";
import util from "../../common/Util";

export default {
 
    borderBgView:{
            padding:10,
            width:util.getWidth(90),
            height:util.getHeight(15),
            borderWidth: .5,
            borderRadius: 2,
            borderColor: 'gray',
            borderBottomWidth: 0,
            shadowColor: 'gray',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,
            shadowRadius: 5,
            elevation: 2,
            marginTop: 20,
            justifyContent:'center',
            alignItems:'center',
            flexDirection:'row'
    },
    btnStyle: {
        backgroundColor: theme.appDefaultColor,
        width: '100%',
        justifyContent: "center",
        alignSelf: "center",
        padding: '5%',
      },
      btnBg: {
     marginTop:10,
        width: '70%',
        height: util.getHeight(15),
        justifyContent: "center"
      },
      textInput: {
        borderColor: "gray",
        borderWidth: 0.3,
        marginTop: 5,
        padding: 12,
        width:util.getWidth(50)-15,
        height: util.getHeight(7),
        color: theme.appDefaultColor,
        fontSize: 14
      },
      shadowBg:{
        backgroundColor:'white',
        borderRadius:3,
        borderWidth:0.5,
        shadowColor: 'rgba(0, 174 , 239, 0.1)',
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 1,
        shadowOpacity: 1,
        elevation: 10,
      },
      settingCellStyle:{
        marginTop: 30,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        padding:15,
      }

};
