import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
} from "native-base";

import { View, TextInput, Image, Alert } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import util from "../../common/Util";
const blueColor= '#2A409A';
class ProfileScreen extends Component {
    constructor(){
        super();
    }
    render(){
        const { props: { name, index, list, navigation } } = this;
        return( <Container style= {global.container} >
            <Content>
            <Header style={{backgroundColor:'white'}}>
            <Left>
            <Button
            transparent
            onPress={() => navigation.navigate("SettingScreen")} >
            <Icon active name="arrow-back" style={{ color:blueColor}} />
            </Button>
            </Left>
            <Body >
            <Title style={global.headerTitleText}>      PROFILE</Title>
            </Body>
            </Header>
             <View style= {{justifyContent:'center', alignItems:'center',
                   marginLeft:util.getWidth(5), marginRight:util.getWidth(5)}} >

             <View style={{borderRadius:90, backgroundColor:blueColor, 
                     justifyContent:'center', alignItems:'center', marginTop:util.getHeight(10),
                     width:util.getWidth(30), height:util.getWidth(30)}} >
                   <Icon name="camera" Size={50} style={{ color:"#FFF"}}></Icon>
             </View>
             <Text style={{ textAlign:'center',color:blueColor,
                  fontWeight:'500' , fontSize:18}} > Hamburger World</Text>
             <Text style={{ textAlign:'center',color:blueColor, marginTop:10,
                   fontSize:9 }} >info@hamburgerworld.com</Text>

            <TextInput 
              style={[styles.textInput, {width:util.getWidth(88.5), height:util.getHeight(30), 
                      marginTop:15, textAlignVertical: "top",}]}
              underlineColorAndroid='transparent'
              placeholder='TRUCK DESCRIPTION'
              returnKeyType={'next'}
            />     

             <View style={styles.btnBg}>
                         <Button style={styles.btnStyle}>
                              <Text style={{textAlign:'center', color:'white'}}> SAVE </Text></Button>
                        </View>
             </View>

            </Content>
        </Container>
        )
    }

}

export default ProfileScreen;