import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage, Dimensions } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import images from "@assets/images";
import util from "../../common/Util";
import Icons from "react-native-vector-icons/MaterialIcons.js";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import IconTruck from "react-native-vector-icons/MaterialCommunityIcons.js";
import Iconfork from "react-native-vector-icons/MaterialCommunityIcons.js";
import Icon1 from 'react-native-vector-icons/Entypo';

// tabs pages
import TruckSettings from './settingsTabs/trucksettings';
import SquareAccount from './settingsTabs/squareLoginScreen';
import Payments from './settingsTabs/payments';
import LocationScreen from './settingsTabs/location';
import SupportScreen from './settingsTabs/support';
const blueColor = '#1a5177';

class SettingScreen extends Component {
  constructor() {
    super();

    this.state = {
      SwitchOnValueHolder: false,
      height: null,
      width: null,
      index: 0
    }
  }
  componentDidMount() {
    AsyncStorage.getItem('height').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })
  }

  render() {

    let AppComponent = null;


    switch (this.state.index) {
      case 0:
        AppComponent = TruckSettings

        break;
      case 1:
        AppComponent = SquareAccount
        break;
      case 2:
        AppComponent = Payments
        break;
      case 3:
        AppComponent = LocationScreen
        break;
      case 4:
        AppComponent = SupportScreen
        break;

    }
    const { props: { name, index, list, navigation } } = this;
    return (<Container style={global.container} >

      <Content>
      <ImageBackground source={images.bg_p} style={{ width: "100%", height: this.state.height }}>

        <View style={{ width: '100%', height: '100%', flexDirection: 'row' }}>
          <View style={{ width: '30%', backgroundColor: 'white', height: '100%', borderWidth: 0.5, borderColor: '#7F7F7F' }}>
            <View style={{ width: '100%', height: '6%', flexDirection: 'row', marginTop: '2%', marginBottom: '2%' }}>
              <View style={{ width: '20%', alignItems: 'center' }}>
                <TouchableOpacity style={{ marginTop: 3 }}

                  onPress={() => this.props.navigation.openDrawer()} >

                  <Icon1 active name="menu" size={35} color="#1a5177" />
                </TouchableOpacity>
              </View>
              <View style={{ width: '60%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Settings</Text>
              </View>
              <View style={{ width: '20%' }}></View>
            </View>
            <View>


              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '7%', marginBottom: '2%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 0 })} style={{ width: '100%', height: '7%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconTruck
                    name="truck"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Food Truck settings
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>

              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '3%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 1 })} style={{ width: '100%', height: '7%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconTruck
                    name="directions-fork"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Link your Square Account
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>

              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '3%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 2 })} style={{ width: '100%', height: '7%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <Icons
                    name="payment"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Payments & Subscription
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>



              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '3%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 3 })} style={{ width: '100%', height: '7%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <Icons
                    name="my-location"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Location
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>


              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '30%' }}></View>
              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '3%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 4 })} style={{ width: '100%', height: '7%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <Icons
                    name="help"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Support
                      </Text>
                </View>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>

              </TouchableOpacity>
              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%' }}></View>


            </View>
            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={images.makeImg_p}
                style={{ width: '60%', height: '60%' }} />
            </View>
          </View>

          <View style={{ width: '70%' }}>
            <AppComponent />
          </View>


        </View>
      </ImageBackground>

      </Content>
    </Container>)
  }

}
export default SettingScreen;
