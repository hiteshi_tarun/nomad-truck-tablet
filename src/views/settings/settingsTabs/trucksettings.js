import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    List,
    ListItem,
    Left,
    Right,
    Body,
    CheckBox,

} from "native-base";

import Spinner from 'react-native-loading-spinner-overlay';
import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage, } from "react-native";
import global from "@assets/css/global";
import images from "@assets/images";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import IconTruck from "react-native-vector-icons/MaterialCommunityIcons.js";
import util from '../../../common/Util'
import ApiManager from "../../../common/ApiManager";
const blueColor = '#2A409A';

class TruckSettingsScreens extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: null,
            truckName: '',
            truckId: '',
            Description: '',
            BusinessName: '',
            street: '',
            addStreet: '',
            city: '',
            zipCode: '',
            Country: '',
            state: '',
            ImageUrl: '',
            ImageType: '',
            Spinnervisible: false,
        }
    }

    componentDidMount() {
        this.setState({ Spinnervisible: true })
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
        util.getValueFromStore("AuthToken")
            .then(authtoken => {

                util.getValueFromStore("TruckId")
                    .then(token => {
                        //  truckID = token;
                        console.log('get All Food truckID ', token);
                        ApiManager._getTruckDescription(token, authtoken)
                            .then((response) => response.json())
                            .then(res => {
                                this.setState({ Spinnervisible: false })
                                if (res.status == 200) {
                                    console.log('Get _getTruckDescription ', res.data);
                                    this.setState({
                                        truckId: res.data[0].truckId, truckName: res.data[0].truckName, Description: res.data[0].truckDescription,
                                        ImageUrl: res.data[0].profilePicture.contents, ImageType: res.data[0].profilePicture.contentsContentType
                                    })
                                }
                                else {
                                  //  alert("some thing is wrong- status" + res.status)
                                }
                            }).catch((error) => {
                                console.log('Get _getTruckDescription error ', error)
                            });

                        ApiManager._getTruckInfo(token, authtoken)
                            .then((response) => response.json())
                            .then(res => {
                                this.setState({ Spinnervisible: false })
                                if (res.status == 200) {
                                    console.log('Get _getTruckInfo ', res.data);
                                    this.setState({
                                        BusinessName: res.data.name, street: res.data.contact.street1, addStreet: res.data.contact.street2,
                                        city: res.data.contact.city, zipCode: res.data.contact.zipCode, Country: res.data.contact.country, state: res.data.contact.state
                                    })
                                }
                                else {
                                 //   alert("some thing is wrong- status" + res.status)
                                }

                            }).catch((error) => {
                                console.log('Get _getTruckInfo error ', error)
                            });
                    })
                    .catch(err => {
                        console.log("TruckId token error ", err);
                    });
            })
            .catch(err => {
                console.log("AuthId token error ", err);
            });
    }
    _updateDesription = () => {
        this.setState({ Spinnervisible: true })
        util.getValueFromStore("AuthToken")
            .then(authtoken => {
                util.getValueFromStore("TruckId")
                    .then(token => {
                        //  truckID = token;
                        console.log('get All Food truckID ', token);
                        const { truckId, truckName, Description } = this.state;
                        ApiManager._UpdateTruckDescription(Description, truckId, truckName, authtoken)
                            .then(res => {
                                this.setState({ Spinnervisible: false })
                                if (res.status == 200) {

                                  //  alert('Get _UpdateTruckDescription ' + res.status);
                                }
                                else {

                                 //   alert("some thing is wrong- status" + res.status)
                                }
                            }).catch((error) => {
                                console.log('Get _getTruckDescription error ', error)
                            });
                    })
                    .catch(err => {
                        console.log("TruckId token error ", err);
                    })
            })
            .catch(err => {
                console.log("AuthId token error ", err);
            });

    }
    render() {
        return (
            <Container style={{ height: this.state.height, width: '100%' }} >
                <Spinner visible={this.state.Spinnervisible} textContent={"Loading..."} color="#1a5177" overlayColor="rgba(256, 256, 256, 0.5)" textStyle={{ color: '#1a5177' }} />
                <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ marginTop: 3 }} >
                            <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Food Truck Settings</Text>
                    </View>
                    <TouchableOpacity onPress={() => this._updateDesription()} style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#1a5177', fontWeight: 'bold', fontSize: 20 }}>Save</Text>
                    </TouchableOpacity>
                </View>


                <View style={{ width: '100%', flexDirection: 'row', height: '90%' }}>

                    <View style={{ width: '70%', alignItems: 'center', height: '100%' }}>
                        <Content contentContainerStyle={{}} >
                            <View style={{}}>
                                <View style={{ width: '90%', height: util.getHeight(7), backgroundColor: 'white', marginTop: '5%', borderWidth: 1, borderColor: '#7f7f7f' }}>
                                    <View style={{ width: '100%', height: util.getHeight(7), flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                        <View style={{ width: '7%', marginLeft: '3%' }}>
                                            <IconTruck
                                                name="truck"
                                                size={25}
                                                style={{ color: "#1a5177", justifyContent: 'center' }}
                                            />
                                        </View>
                                        <View style={{ width: '89%', marginLeft: '1%' }}>
                                            <TextInput
                                                style={{ width: '100%', color: '#1a5177' }}
                                                // editable={false} selectTextOnFocus={false}
                                                // underlineColorAndroid='transparent'
                                                placeholder="Food Truck Name"
                                                placeholderTextColor='#1a5177'
                                                returnKeyType={'next'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.truckName}
                                                onChangeText={truckName => this.setState({ truckName: truckName })}
                                            />
                                        </View>
                                    </View>
                                </View>
                                {/* <View style={{ width: '90%', height: util.getHeight(7), backgroundColor: 'white', marginTop: '2%', borderWidth: 1, borderColor: '#7f7f7f' }}>
                                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                        <View style={{ width: '7%', marginLeft: '3%' }}>
                                            <IconTruck
                                                name="truck"
                                                size={25}
                                                style={{ color: "#1a5177", justifyContent: 'center' }}
                                            />
                                        </View>
                                        <View style={{ width: '89%', marginLeft: '1%', }}>
                                            <TextInput
                                                style={{ color: '#1a5177' }}
                                                // underlineColorAndroid='transparent'
                                                placeholder="Food Truck Name"
                                                placeholderTextColor='#1a5177'
                                                returnKeyType={'next'}
                                                editable={false}
                                                underlineColorAndroid='transparent'
                                                value={this.state.truckName}
                                            // onChangeText={street => this.setState({ street: street })}
                                            />
                                        </View>
                                    </View>
                                </View> */}
                                <View style={{ width: '90%', paddingHorizontal: '5%', height: util.getHeight(25), backgroundColor: 'white', marginTop: '3%', borderWidth: 1, borderColor: '#7f7f7f' }}>
                                    <TextInput
                                        style={{ color: '#1a5177', }}
                                        // underlineColorAndroid='transparent'
                                        placeholder="Food Truck Description"
                                        placeholderTextColor='#1a5177'
                                        multiline={true}
                                        returnKeyType={'next'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.Description}
                                        onChangeText={Description => this.setState({ Description: Description })}
                                    />
                                </View>
                                <View style={{ width: '90%', height: util.getHeight(7), backgroundColor: 'white', marginTop: '5%', borderWidth: 1, borderColor: '#7f7f7f' }}>
                                    <View style={{ width: '100%', height: util.getHeight(7), flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                        <View style={{ width: '7%', marginLeft: '3%' }}>
                                            <IconTruck
                                                name="truck"
                                                size={25}
                                                style={{ color: "#1a5177", justifyContent: 'center' }}
                                            />
                                        </View>
                                        <View style={{ width: '89%', marginLeft: '1%' }}>
                                            <TextInput
                                                style={{ width: '100%', color: '#1a5177' }}
                                                editable={false} selectTextOnFocus={false}
                                                // underlineColorAndroid='transparent'
                                                placeholder="Business Name"
                                                placeholderTextColor='#1a5177'
                                                returnKeyType={'next'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.BusinessName}
                                            // onChangeText={street => this.setState({ street: street })}
                                            />
                                        </View>
                                    </View>
                                </View>

                                <View style={{ width: '90%', paddingVertical: '5%', paddingHorizontal: '5%', marginBottom: util.getHeight(7), backgroundColor: 'white', marginTop: '5%', borderWidth: 1, borderColor: '#7f7f7f' }}>

                                    <TextInput
                                        style={{ width: '100%', color: '#1a5177', height:util.getHeight(7) }}
                                        // underlineColorAndroid='transparent'
                                        editable={false} selectTextOnFocus={false}
                                        placeholder="Street"
                                        placeholderTextColor='#1a5177'
                                        returnKeyType={'next'}

                                        value={this.state.street}
                                    // onChangeText={street => this.setState({ street: street })}
                                    />
                                    <TextInput
                                        style={{ color: '#1a5177', height:util.getHeight(7) }}
                                        // underlineColorAndroid='transparent'
                                        editable={false} selectTextOnFocus={false}
                                        placeholder="Additional Street"
                                        placeholderTextColor='#1a5177'
                                        returnKeyType={'next'}

                                    // value={this.state.street}
                                    // onChangeText={street => this.setState({ street: street })}
                                    />
                                    <View style={{ width: '100%', flexDirection: 'row' }}>
                                        <TextInput
                                            style={{ width: '60%', color: '#1a5177' , height:util.getHeight(7)}}
                                            // underlineColorAndroid='transparent'
                                            editable={false} selectTextOnFocus={false}
                                            placeholder="City"
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}

                                            value={this.state.city}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                        <TextInput
                                            style={{ width: '40%', color: '#1a5177', height:util.getHeight(7) }}
                                            // underlineColorAndroid='transparent'
                                            editable={false} selectTextOnFocus={false}
                                            placeholder="Zip Code"
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}
                                            value={this.state.zipCode}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                    </View>

                                    <View style={{ width: '100%', flexDirection: 'row' }}>

                                        <TextInput
                                            style={{ width: '60%', color: '#1a5177', height:util.getHeight(7) }}
                                            editable={false} selectTextOnFocus={false}
                                            // underlineColorAndroid='transparent'
                                            placeholder="Country"
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}

                                            value={this.state.Country}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                        <TextInput
                                            style={{ width: '40%', color: '#1a5177', height:util.getHeight(7) }}
                                            editable={false} selectTextOnFocus={false}
                                            // underlineColorAndroid='transparent'
                                            placeholder="State"
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}

                                            value={this.state.state}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                    </View>
                                </View>
                            </View>

                        </Content>

                    </View>

                    <View style={{ width: '30%', height: '100%', alignItems: 'center', marginTop: '2%' }}>

                        {/* <View style={{ backgroundColor: 'white', borderRadius: util.getWidth(17) / 2, borderWidth: 1, borderColor: '#7F7F7F', height: util.getWidth(17), width: util.getWidth(17), justifyContent: 'center', alignItems: 'center' }}>
                        
                            <Image
                                style={{ height: util.getWidth(13), width: util.getWidth(13) }}
                                resizeMode={"cover"}
                                source={{ uri: `data:${this.state.ImageType};base64,${this.state.ImageUrl}` }}
                            />

                        </View> */}


                    </View>
                </View>
            </Container >)
    }
}
export default TruckSettingsScreens;
