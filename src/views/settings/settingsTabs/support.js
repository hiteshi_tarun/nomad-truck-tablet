import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Footer,
    Icon,
    Left,
    Right,
    Body,
    CheckBox,
    Form, Item, Input, Label
} from "native-base";

import { View, TextInput, Image, Alert, TouchableOpacity, AsyncStorage, WebView,Linking  } from "react-native";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import global from "@assets/css/global";
import styles from "./styles";
import util from "../../../common/Util";
import keyStrings from '../../../common/Localization';
const blueColor = '#1a5177';
class SquareLoginScreen extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
    }
    render() {
        const { props: { name, index, list, navigation } } = this;
        return (<Container style={{ height: this.state.height }}>

            <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ marginTop: 3 }} >
                        <IconIonic active name="md-arrow-back" size={25} color="#1a5177" />
                    </TouchableOpacity>
                </View>
                <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Support</Text>
                </View>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>

                </View>
            </View>
            <View style={{ height: '40%', margin: '4%', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f', backgroundColor: 'white' }}>

                <View style={{ height: '50%', justifyContent: 'center', }}>
                    <Text style={{ color: blueColor, alignSelf: 'center', fontWeight: '500' }}>For support please feel </Text>
                    <Text style={{ color: blueColor, alignSelf: 'center', fontWeight: '500' }}>free to start a ticket at:</Text>
                    <Text style={{ color: blueColor,alignSelf: 'center', textDecorationLine: 'underline', marginTop: '1%' }}
                        onPress={() => Linking.openURL('https://nomadfoodtruckapp.zendesk.com')}>
                        https://nomadfoodtruckapp.zendesk.com</Text>
                </View>
                <View style={{ height: '50%', justifyContent: 'center' }}>
                    <Text style={{ color: blueColor, alignSelf: 'center', marginBottom: '3%' }}>You can use your NOMAD email address to login.</Text>
                    <Text style={{ color: blueColor, alignSelf: 'center', fontSize: 18 }}>truck@nomadfoodtruckapp.com</Text>
                </View>

            </View>


        </Container>
        )
    }

}

export default SquareLoginScreen;