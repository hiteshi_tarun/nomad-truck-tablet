import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Footer,
    Icon,
    Left,
    Right,
    Body,
    CheckBox,
    Form, Item, Input, Label
} from "native-base";

import { View, TextInput, Image, Alert, TouchableOpacity, Switch,AsyncStorage} from "react-native";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import global from "@assets/css/global";
import styles from "./styles";
import util from "../../../common/Util";
import keyStrings from '../../../common/Localization';
const blueColor = '#1a5177';
import  BackgroundTimer from "react-native-background-timer";
import { ProgressDialog } from 'react-native-simple-dialogs';
import ApiManager from "../../../common/ApiManager";
var intervalId;
class SquareLoginScreen extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
            switchStatus: false,
            loader: false
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
        util.getValueFromStore("LocationActiveStatus").then(
            status =>{ 
                if (status =='Yes') {
                    console.log('locatin status ', status)
                    this.setState({switchStatus: true});
                } else {
                    console.log('locatin status ', status)
                    this.setState({switchStatus: false});
                }
            }
        )
    }

    _callApiLastLoc = (lat, long) => {
        console.log('lat long value ', lat, long);
        // this.setState({loader: true})
        util
          .getValueFromStore("AuthToken")
          .then(AuthToken => {  
        util
          .getValueFromStore("TruckId")
          .then(truckId => {
            console.log("Truck Id API call ", truckId);
            ApiManager._updateCurrentLocation(AuthToken, lat, long, truckId)
              .then(data => {
                if (data.status == 200) {
                  console.log("_updateCurrentLocation success ", data);
                //   this.setState({loader: false})
                } else {
                  console.log("_updateCurrentLocation fail ", data);
                //   this.setState({loader: false})
                }
              })
              .catch(error => {
                console.log("_updateCurrentLocation error in catch block ", error);
              });
          })
        })
          .catch(err => {
            console.log("error", err);
          });
      };

    _updateCurrentLocation = ()=>{
        this.setState({switchStatus:!this.state.switchStatus})


        console.log('in _updateCurrentLocation')
        
        if (!this.state.switchStatus) {
            alert("Tracking on");
            util.setStore('LocationActiveStatus',"Yes");
            console.log('in switchStatus')
          
             intervalId = BackgroundTimer.setInterval(() => {
                console.log('in BackgroundTimer')
                navigator.geolocation.getCurrentPosition(position => {
                    console.log('in geolocation')
                  lat = position.coords.latitude;
                  long = position.coords.longitude;
                  console.log("Lattitude long ", lat, long);
                //   this.setState({
                //     Lattitude: lat,
                //     Longitude: long
                //   });
                  this._callApiLastLoc(lat, long);
                });
              }, 180000);
        } else {
            alert("Tracking off");
            // this.setState({loader: false})
            console.log('in switchStatus')
            util.setStore('LocationActiveStatus',"No");
            clearInterval(intervalId);
        }
    }

    render() {
        console.log("switch value",this.state.switchStatus)
        const { props: { name, index, list, navigation } } = this;
        return (<Container style={{ height: this.state.height }}>
                <ProgressDialog
                    visible={this.state.loader}
                    message={'Please wait...'}
                    />

            <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ marginTop: 3 }} >
                        <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                    </TouchableOpacity>
                </View>
                <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Location</Text>
                </View>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    {/* <Text style={{ color: '#1a5177', fontWeight: 'bold', fontSize: 20 }}>Save</Text> */}
                </View>
            </View>
            <View style={{ height: "9%", margin: '3%',flexDirection:'row', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f', backgroundColor: 'white' }}>
                <View style={{ width: '85%', justifyContent: 'center',marginLeft:'5%',justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Tracking</Text>
                </View>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <Switch
                    onValueChange={()=> this._updateCurrentLocation() }
                    value={this.state.switchStatus} thumbTintColor={blueColor} style={{ marginTop: 3 }} onTintColor='#7f7f7f'  >
                       
                    </Switch>
                </View>
            </View>

        
        </Container>
        )
    }

}

export default SquareLoginScreen;