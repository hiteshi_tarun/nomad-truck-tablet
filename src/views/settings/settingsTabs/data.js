import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Form,
    Item,
    Input,
    List,
    ListItem,
    Left,
    Right,
    Body,
    CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import global from "@assets/css/global";
import images from "@assets/images";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import Icon from "react-native-vector-icons/MaterialIcons.js";
import util from '../../../common/Util'
import ApiManager from "../../../common/ApiManager";

const blueColor = '#1a5177';

class PaymentsScreens extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
            nameOnCard: '',
            ccNumber: '',
            expDate: '',
            cCv: '',
            defaultStatus: false,
            Subscriptionstaus: true,
            Subscriptionamount: '24.99',
            view: false
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
    }
    async handleSubmit() {
        if (!this.state.nameOnCard) {
            alert('Please enter card holder name');
        } else if (!this.state.ccNumber) {
            alert('Please enter card number');
        } else if (this.state.ccNumber.length < 16) {
            alert('Please enter valid card number');
        } else if (!this.state.expDate) {
            alert('Please enter card expiry date');
        } else if (!this.state.cCv) {
            alert('Please enter CCV number');
        } else {
            const month = this.state.expDate.split("/")[0];
            const year = this.state.expDate.split("/")[1];
            console.log("Card Detail ", this.state.ccNumber, "month/year", month, year, this.state.cCv);
            util.getValueFromStore("AuthToken")
                .then(authtoken => {
                    util.getValueFromStore("TruckId").then(TruckId => {

                        console.log("TruckId Id Data ", TruckId);
                        ApiManager._getStripeToken(this.state.nameOnCard, this.state.ccNumber, month, year, this.state.cCv)
                            .then((response) => response.json())
                            .then(response => {
                                console.log('Stripe Token Response: ', response);  // , response._bodyText
                                //   var res = JSON.parse(response._bodyText);
                                console.log('Stripe Token Response data: ', response.id);

                                ApiManager._addCard(authtoken, TruckId, response.id)
                                    .then(
                                        data => {
                                            console.log(" data add card ", data)
                                        }
                                    );
                            })
                    }).catch(err => {
                        console.log("TruckId token error ", err);
                    })
                })
                .catch(err => {
                    console.log("AuthToken token error ", err);
                });
        }
    }
    render() {

        return (
            <Container style={{ height: this.state.height }}>

                <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ marginTop: 3 }} >
                            <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Payments</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    </View>
                </View>


                <View style={{ width: '94%', height: '90%', margin: '3%' }}>

                    {this.state.view ? <View style={{ width: '100%', height: '40%', }}>
                        <View style={{ width: '100%', height: '100%', }}>
                            <View style={{ width: '100%', height: '40%', flexDirection: 'row' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: '78%', flexDirection: 'row', height: '65%', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Text style={{ color: blueColor, flex: 1, fontSize: 13, marginLeft: 10 }}>Routing Number</Text>
                                    <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>1234567890</Text>
                                    <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>Account Number</Text>
                                    <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>************4444</Text>
                                </View>

                                <TouchableOpacity style={{ width: '18%', marginLeft: '2%', height: '65%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                                    <Text style={{ color: '#ffffff' }}>Pay now</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ width: '100%', marginVertical: '2%', height: '15%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Subscription</Text>
                            </View>
                            <View style={{ width: '100%', height: '40%', flexDirection: 'row' }}>
                                <View style={{ width: '78%', alignItems: 'center', height: '65%', flexDirection: 'row', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Icon
                                        name="payment"
                                        size={25}
                                        style={{ color: "#1a5177", justifyContent: 'center', marginHorizontal: '2%' }}
                                    />
                                    <Text style={{ color: blueColor }}>**** **** **** 4444</Text>

                                </View>

                                <TouchableOpacity style={{ width: '18%', marginLeft: '2%', height: '65%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: "#f96467" }}>
                                    <Text style={{ color: '#ffffff' }}>DELETE</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: '15%', flexDirection: 'row', marginTop: '5%', justifyContent: 'center', alignItems: 'center', }}>

                            <View style={{ width: '78%', flexDirection: 'row', height: '85%', margin: '1%', }}>
                                <TouchableOpacity onPress={() => this.setState({ Subscriptionstaus: true, Subscriptionamount: '24.99' })} style={{ flex: 1, backgroundColor: this.state.Subscriptionstaus ? blueColor : "white", width: '45%', justifyContent: 'center', alignItems: 'center', marginRight: '5%', height: '100%', alignItems: 'center', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Text style={{ color: this.state.Subscriptionstaus ? "white" : blueColor, }}>Monthly</Text>
                                    <Text style={{ color: this.state.Subscriptionstaus ? "white" : blueColor, }}>$ 24.99</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ Subscriptionstaus: false, Subscriptionamount: '624.99' })} style={{ flex: 1, backgroundColor: this.state.Subscriptionstaus ? "white" : blueColor, width: '45%', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', height: '100%', alignItems: 'center', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Text style={{ color: this.state.Subscriptionstaus ? blueColor : "white", }}>Yearly</Text>
                                    <Text style={{ color: this.state.Subscriptionstaus ? blueColor : "white", }}>$ 624.99</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '18%', height: '85%' }}>

                            </View>

                        </View>
                        {/* <View style={{ width: '100%', height: '50%', flexDirection: 'row' }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: '78%', flexDirection: 'row', height: '85%', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                            <Text style={{ color: blueColor, flex: 1, fontSize: 13, marginLeft: 10 }}>Routing Number</Text>
                            <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>1234567890</Text>
                            <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>Account Number</Text>
                            <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>************4444</Text>
                        </View>

                        <TouchableOpacity style={{ width: '18%', marginLeft: '2%', height: '80%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                            <Text style={{ color: '#ffffff' }}>Pay now</Text>
                        </TouchableOpacity>
                    </View>


                        <View style={{ width: '100%', marginTop: '2%', height: '50%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Subscription</Text>
                        </View>
                        <View style={{ width: '100%', height: '10%', flexDirection: 'row' }}>
                            <View style={{ width: '78%', alignItems: 'center', height: '85%', flexDirection: 'row', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                <Icon
                                    name="payment"
                                    size={25}
                                    style={{ color: "#1a5177", justifyContent: 'center', marginHorizontal: '2%' }}
                                />
                                <Text style={{ color: blueColor }}>**** **** **** 4444</Text>

                            </View>

                            <TouchableOpacity style={{ width: '18%', marginLeft: '2%', height: '80%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: "#f96467" }}>
                                <Text style={{ color: '#ffffff' }}>DELETE</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                        : <View style={{ width: '100%', height: '30%', flexDirection: 'row', marginTop: '5%' }}>
                            <View style={{ width: '78%', paddingHorizontal: '3%', backgroundColor: 'white', marginRight: '2%', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>

                                <View style={{ paddingVertical: '1%', }}>
                                    <TextInput
                                        style={{ width: '100%' }}
                                        // underlineColorAndroid='transparent'
                                        placeholder="NAME ON CARD"
                                        placeholderTextColor='#1a5177'

                                        value={this.state.nameOnCard}
                                        onChangeText={t => this.setState({ nameOnCard: t })}
                                        returnKeyType={"next"}
                                    // value={this.state.street}
                                    // onChangeText={street => this.setState({ street: street })}
                                    />
                                    <TextInput
                                        style={{ width: '100%' }}
                                        // underlineColorAndroid='transparent'
                                        maxLength={16}
                                        placeholder="CC NUMBER"
                                        placeholderTextColor='#1a5177'
                                        keyboardType={"numeric"}
                                        value={this.state.ccNumber}
                                        onChangeText={t => this.setState({ ccNumber: t })}
                                        returnKeyType={"next"}

                                    // value={this.state.street}
                                    // onChangeText={street => this.setState({ street: street })}
                                    />
                                    <View style={{ width: '100%', flexDirection: 'row' }}>
                                        <TextInputMask
                                            style={{ width: '60%' }}
                                            // underlineColorAndroid='transparent'
                                            placeholder={"MM/YYYY"}
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}
                                            maxLength={7}
                                            type={"datetime"}
                                            options={{
                                                format: "MM/YYYY"
                                            }}
                                            value={this.state.expDate}
                                            onChangeText={t => this.setState({ expDate: t })}


                                        // value={this.state.street}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                        <TextInput
                                            style={{ width: '40%' }}
                                            // underlineColorAndroid='transparent'
                                            placeholder="CCV"
                                            maxLength={3}
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}
                                            keyboardType={"numeric"}
                                            value={this.state.cCv}
                                            onChangeText={t => this.setState({ cCv: t })}
                                            returnKeyType={"done"}
                                        />
                                    </View>

                                </View>


                            </View>
                            <View style={{ width: '18%', height: '100%', }}>
                                <View style={{ width: '100%', height: '50%', alignItems: 'center', }}>
                                    <TouchableOpacity onPress={() => this.handleSubmit()} style={{ width: '98%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', height: '60%', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                                        <Text style={{ color: '#ffffff' }}>ADD</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '100%', height: '50%', alignItems: 'center', }}>
                                    <TouchableOpacity style={{ width: '98%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', height: '60%', bottom: 0, position: "absolute", justifyContent: 'center', alignItems: 'center', backgroundColor: "#d0d0d8" }}>
                                        <Text style={{ color: '#ffffff' }}>CLEAR</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>


                        </View>}





                </View>
            </Container>)
    }
}
export default PaymentsScreens;
