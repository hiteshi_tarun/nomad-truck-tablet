import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Right,
    Body,
    CheckBox,
    Text,
    Button,
    Footer,
    Icon,
    Left,
    Form, Item, Input, Label
} from "native-base";

import { View, TextInput, Image, Alert, TouchableOpacity, AsyncStorage, WebView, ActivityIndicator } from "react-native";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import util from "../../../common/Util";
import keyStrings from '../../../common/Localization';
import global from "@assets/css/global";
import styles from "./styles";
import ApiManager from "../../../common/ApiManager";

const blueColor = '#1a5177';
class SquareLoginScreen extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
            showWebview: false,
            ShowMessage:"Optional: please log in to your SQUARE account to allow NOMAD sync orders inventory."
        
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
        AsyncStorage.getItem('accessTokenSquare').then((value) => {
            console.log("token  final ", value)
            this.setState({ showWebview: true })
        })
    }

    _storeSquareToken = (accestoken) =>{
        util.getValueFromStore("AuthToken")
        .then(authtoken => {
            util.getValueFromStore("TruckId")
            .then(TruckId => {
                ApiManager._sendCardNonce(AuthToken ,TruckId, accestoken)
                .then( res =>{
                    console.log('Update square token at backend ', res)
                }) .catch(err => {
                     console.log("Update square token error ", err);
                });
            })
        })
     }

    onNavigationStateChange(navState) {
        var event = navState.url.split('access_token=')[1]
        if (event != null) {
            console.log("token tokenValue ",event)
       //  this.setState({ ShowMessage: "Already Login in SQUARE account and allow NOMAD sync orders inventory." })
         var mySplitResult2 = event.split("&")[0];
         console.log("token tokenValue final ", mySplitResult2);
        
         AsyncStorage.setItem("accessTokenSquare", mySplitResult2);
         this._storeSquareToken(mySplitResult2);
     }
    //     if (navState.url != "https://connect.squareup.com/oauth2/authorize?client_id=sq0idp-kGtIoTz2DnCmJQjfeZTDDQ&response_type=token&scope=ITEMS_READ ITEMS_WRITE PAYMENTS_READ PAYMENTS_WRITE") {
    //         this.setState({ showWebview: false })
    // // }
    // //         else{
    // //             this.webview.stopLoading();
    // //         }
           
    //     }
    //     else {
          
    //     }
        //  console.log("dfgdsfg",data)
    }

    ActivityIndicatorLoadingView() {

        return (

            <ActivityIndicator
                color='#009688'
                size='large'
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            />
        );
    }
    render() {
        const { props: { name, index, list, navigation } } = this;
        return (<Container style={{ height: this.state.height }}>

            <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ marginTop: 3 }} >
                        <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                    </TouchableOpacity>
                </View>
                <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>SQUARE Login</Text>
                </View>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: '#1a5177', fontWeight: 'bold', fontSize: 20 }}>Save</Text>
                </View>
            </View>
            <View style={{ height: '70%', margin: '3%' }}>
                <View style={{ flex: 1 }}>

                    {this.state.showWebview == true ? <WebView
                       ref={(ref) => { this.webview = ref; }}
                        source={{ uri: 'https://connect.squareup.com/oauth2/authorize?client_id=sq0idp-kGtIoTz2DnCmJQjfeZTDDQ&response_type=token&scope=ITEMS_READ ITEMS_WRITE PAYMENTS_READ PAYMENTS_WRITE' }}
                        style={{ flex: 1 }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        renderLoading={this.ActivityIndicatorLoadingView}
                        startInLoadingState={true}
                        automaticallyAdjustContentInsets={false}
                        // onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest} //for iOS
                        onNavigationStateChange={this.onNavigationStateChange.bind(this)}  //for Android
                    />
                        :
                        <View style={{ height: "25%", margin: '3%', flexDirection: 'row', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f', backgroundColor: 'white' }}>
                            <View style={{ width: '90%', justifyContent: 'center', marginLeft: '5%', marginRight: '5%', justifyContent: 'center' }}>
                                <Text style={{ color: blueColor, alignSelf: 'center', }}>{this.state.ShowMessage}</Text>
                                {/* <Text style={{ color: blueColor, alignSelf: 'center', }}></Text> */}
                            </View>
                        </View>
                    }
                </View>
            </View>
            <Footer style={{ position: 'absolute', bottom: 0, height: '10%', width: '100%', backgroundColor: blueColor, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => this.setState({ visible: true, showWebview: true, })} style={{ height: '100%', width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontWeight: "500" }}>Link SQUARE Account</Text>
                </TouchableOpacity>
            </Footer>
           
        </Container>
        )
    }

}

export default SquareLoginScreen;