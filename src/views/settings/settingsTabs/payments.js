import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Form,
    Item,
    Input,
    List,
    ListItem,
    Left,
    Right,
    Body,
    CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage, TouchableHighlight, Modal } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import global from "@assets/css/global";
import images from "@assets/images";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import Icon from "react-native-vector-icons/MaterialIcons.js";
import util from '../../../common/Util'
import ApiManager from "../../../common/ApiManager";
import Spinner from 'react-native-loading-spinner-overlay';

const blueColor = '#1a5177';

class PaymentsScreens extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
            nameOnCard: '',
            ccNumber: '',
            expDate: '',
            cCv: '',
            defaultStatus: false,
            SubscriptionStatus: true,
            SubscriptionName: "MONTHLY",
            SubscriptionAmount: '24.99',
            view: false,
            SpinnerVisible: false,
            lastDigitCard: '',
            showModal: false,
            token: "",
            paymentView: false,
            Routing_number: '',
            Account_number: ''

        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
        this.setState({ SpinnerVisible: true })

        util.getValueFromStore("AuthToken")
            .then(authToken => {
                util.getValueFromStore("TruckId").then(TruckId => {
                    console.log("TruckId Id Data ", TruckId);

                    ApiManager._getBankAccount(authToken, TruckId)
                    .then((response) => response.json())
                    .then(response => {
                        console.log("_getBankAccount" + response.status);
                        
                        this.setState({ SpinnerVisible: false })
                        if (response.status == 200) {
                            this.setState({ paymentView: true })
                          //  alert("_getBankAccount" + response.status);
                        } else if (response.status == 500) {
                            this.setState({ paymentView: false })
                          //  alert("_getBankAccount error" + response.status);
                        } else {
                          //  alert("_getBankAccount" + response.status);
                        }
                        // alert("" + response.status);
                    }).catch(err => {
                        console.log("card details error", err);
                    })

                    ApiManager._getCardDetails(authToken, TruckId)
                        .then((response) => response.json())
                        .then(response => {
                            this.setState({ SpinnerVisible: false })
                            if (response.status == 200) {
                                this.setState({ view: true })
                                this.setState({ lastDigitCard: response.data.lastFourDigits })
                                console.log("last digit card", response);

                            } else if (response.status == 500) {
                                this.setState({ view: false })
                              //  alert("card error" + response.status);

                            } else {
                              //  alert("card error" + response.status);
                            }
                            // alert("" + response.status);
                        }).catch(err => {
                            console.log("card details error", err);
                        })

                }).catch(err => {
                    console.log("TruckId token error ", err);
                })
            })
            .catch(err => {
                console.log("AuthToken token error ", err);
            });

    }
    async handleSubmit() {
        if (!this.state.nameOnCard) {
            alert('Please enter card holder name');
        } else if (!this.state.ccNumber) {
            alert('Please enter card number');
        } else if (this.state.ccNumber.split(' ').join('').length < 16) {
            alert('Please enter valid card number');
        } else if (!this.state.expDate) {
            alert('Please enter card expiry date');
        } else if (!this.state.cCv) {
            alert('Please enter CCV number');
        } else {

            this.setState({ SpinnerVisible: true })

            const month = this.state.expDate.split("/")[0];
            const year = this.state.expDate.split("/")[1];
            console.log("Card Detail ", this.state.ccNumber, "month/year", month, year, this.state.cCv);

            ApiManager._getStripeToken(this.state.nameOnCard, this.state.ccNumber.split(' ').join(''), month, year, this.state.cCv)
                .then((response) => response.json())
                .then(response => {
                    console.log('Stripe Token Response: ', response);  // , response._bodyText
                    //   var res = JSON.parse(response._bodyText);
                    console.log('Stripe Token Response data: ', response.id);
                    this.setState({ SpinnerVisible: false, showModal: true, token: response.id })
    
                })

        }
    }

    _paymentAccountAdd = () => {
        const { Account_number, Routing_number } = this.state
        if (!Routing_number) {
            alert('Please enter Routing number ');
        } else if (!Account_number) {
            alert('Please enter Account number');
        } else {
            this.setState({ SpinnerVisible: true })
            ApiManager._getAccountStripeToken(Routing_number, Account_number)
                .then((response) => response.json())
                .then(response => {
                    console.log('Stripe account Token Response: ', response);
                    console.log('Stripe account Token Response data: ', response.id);
                    this.setState({ SpinnerVisible: false, })

                    util.getValueFromStore("AuthToken")
                        .then(authToken => {
                            util.getValueFromStore("TruckId").then(TruckId => {

                                ApiManager._passAccountToken(authToken, TruckId, response.id)
                                    .then((response) => response.json())
                                    .then(response => {

                                        if (response.status == 200) {
                                            console.log("_passAccountToken ", response);
                                            this.setState({ paymentView: true })
                                        }
                                        else {
                                            alert("there is something wrong status" + response.status)
                                        }
                                    }).catch(err => {
                                        console.log("_passAccountToken error ", err);
                                    })
                                    .catch(err => {
                                        console.log("_passAccountToken error ", err);
                                    })

                            }).catch(err => {
                                console.log("TruckId token error ", err);
                            })
                        })
                        .catch(err => {
                            console.log("AuthToken token error ", err);
                        });

                })

        }
    }
    _subscriptionAmount = () => {
        util.getValueFromStore("AuthToken")
            .then(authToken => {
                util.getValueFromStore("TruckId"
                ).then(TruckId => {

                    ApiManager._subCard(authToken, TruckId, this.state.token, this.state.SubscriptionName)
                        .then((response) => response.json())
                        .then(
                            data => {
                                console.log("SubscriptionAmount", data.status)

                                if (data.status == 200) {
                                    this.setState({ showModal: false,view: true  })
                                
                                 //   alert("Subscription done successfully");
                                } else {
                                    this.setState({ showModal: false })
                                    // this.setState({view:true})
                                   // alert("Subscription error with status " + data.status);
                                }

                                // alert("card add");
                            }
                        ).catch(err => {

                            console.log("SubscriptionAmount error", err);
                          //  alert("SubscriptionAmount error");
                        })
                })

            }).catch(err => {
                console.log("TruckId token error ", err);
            })
            .catch(err => {
                console.log("AuthToken token error ", err);
            });

    }

    payNowMethod=()=>{
        this.setState({ SpinnerVisible: true })

        util.getValueFromStore("AuthToken")
        .then(authToken => {
            util.getValueFromStore("TruckId"
            ).then(TruckId => {

                ApiManager._payNowTrigger(authToken, TruckId)
                    .then(data => {
                        this.setState({ SpinnerVisible: false })

                            console.log("_payNowTrigger", data.status)
                            if (data.status == 200) {
                                alert("Successfully done");
                            } else {
                                alert("Error with status " + data.status);
                            }
                        }
                    ).catch(err => {
                        this.setState({ SpinnerVisible: false })
                        console.log("SubscriptionAmount error", err);
                        alert("SubscriptionAmount error");
                    })
            })

        }).catch(err => {
            console.log("TruckId token error ", err);
        })
        .catch(err => {
            console.log("AuthToken token error ", err);
        });
    }
    render() {

        return (
            <Container style={{ height: this.state.height }}>

                <Spinner visible={this.state.SpinnerVisible} textContent={"Loading..."} color="#1a5177" overlayColor="rgba(256, 256, 256, 0.5)" textStyle={{ color: '#1a5177' }} />

                <Modal

                    animationType="slide"
                    transparent={true}
                    visible={this.state.showModal}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' }}>

                        <View style={{ height: "60%", width: "50%", backgroundColor: 'rgba(255,255,255, 0.95)', borderRadius: 10, borderWidth: 0.3, borderColor: '#7F7F7F', justifyContent: 'center', alignItems: 'center' }} >
                            <View style={{ height: '80%', width: '70%', justifyContent: 'center', alignItems: 'center' }}>

                                <Text style={{ color: '#1a5177', marginBottom: '15%', textAlign: 'center', fontSize: 20 }}>Are you start {this.state.SubscriptionName} Subscription with amount ${this.state.SubscriptionAmount} ?</Text>

                                <TouchableOpacity onPress={() => this._subscriptionAmount()} style={{ backgroundColor: '#f96467', justifyContent: 'center', alignItems: 'center', width: '90%', height: '20%', borderRadius: 10, justifyContent: 'center', alignContent: 'center', marginBottom: '2%' }}>
                                    <Text style={{ color: 'white', alignContent: 'center' }}>YES</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({ showModal: false })} style={{ backgroundColor: '#c8c8c8', height: '20%', justifyContent: 'center', alignItems: 'center', width: '90%', borderRadius: 10, justifyContent: 'center', alignContent: 'center' }}>
                                    <Text style={{ color: '#1a5177' }}>NO</Text>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>
                </Modal>

                <Content contentContainerStyle={{ height: '100%' }} >

                    <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                        <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={{ marginTop: 3 }} >
                                <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Payments</Text>
                        </View>
                        <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        </View>
                    </View>

                    <View style={{ width: '94%', height: '90%', marginLeft: '3%', marginRight: '3%', marginTop: '1%' }}>

                        {this.state.paymentView ?
                            <View style={{ width: '100%', height: '10%', flexDirection: 'row',}}>
                            
                                    <View style={{ justifyContent: 'center', alignItems: 'center', width: '78%', flexDirection: 'row', height: '85%', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                        <Text style={{ color: blueColor, flex: 1, fontSize: 13, marginLeft: 10 }}>Routing Number</Text>
                                        <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>*********</Text>
                                        <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>Account Number</Text>
                                        <Text style={{ color: blueColor, flex: 1, fontSize: 13, }}>************</Text>
                                    </View>

                                    <TouchableOpacity onPress={()=>this.payNowMethod()} style={{ width: '18%', marginLeft: '2%', height: '80%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                                        <Text style={{ color: '#ffffff' }}>Pay now</Text>
                                    </TouchableOpacity>
                             
                            </View>
                            :
                            <View style={{ width: '100%', height: '20%', flexDirection: 'row' }}>

                                <View style={{ width: '78%', alignItems: 'center', height: '100%', flexDirection: 'row', }}>
                                    <View style={{ width: '100%', height: '100%', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>

                                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}>
                                                <Text style={{ color: '#1a5177', justifyContent: 'center', alignSelf: 'center', }}>Routing Number</Text>
                                            </View>
                                            <View style={{ width: '50%', }}>
                                                <TextInput
                                                    style={{ width: '100%', }}
                                                    //  underlineColorAndroid='transparent'
                                                    placeholder=""
                                                    placeholderTextColor='#1a5177'
                                                    value={this.state.Routing_number}
                                                    onChangeText={Routing_number => this.setState({ Routing_number: Routing_number })}
                                                    returnKeyType={"next"}

                                                />
                                            </View>
                                        </View>

                                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                            <View style={{ width: '40%', justifyContent: 'center' }}>
                                                <Text style={{ color: '#1a5177', justifyContent: 'center', alignSelf: 'center', }}>Account Number</Text>
                                            </View>
                                            <View style={{ width: '50%', }}>
                                                <TextInput
                                                    style={{ width: '100%', }}
                                                    //  underlineColorAndroid='transparent'
                                                    placeholder=""
                                                    placeholderTextColor='#1a5177'
                                                    value={this.state.Account_number}
                                                    onChangeText={Account_number => this.setState({ Account_number: Account_number })}
                                                    returnKeyType={"next"}

                                                />
                                            </View>
                                        </View>

                                    </View>
                                </View>

                                <View style={{ width: '22%', height: '100%', justifyContent: "center", alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => this._paymentAccountAdd()} style={{ width: '80%', marginLeft: '2%', height: '50%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                                        <Text style={{ color: '#ffffff' }}>Add</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>}

                        <View style={{ width: '100%', marginTop: '1%', height: '10%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Subscription</Text>
                        </View>
                        <View style={{ width: '100%', height: '12%', flexDirection: 'row' }}>
                            <View style={{ width: '78%', alignItems: 'center', height: '85%', flexDirection: 'row', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                <Icon
                                    name="payment"
                                    size={25}
                                    style={{ color: "#1a5177", justifyContent: 'center', marginHorizontal: '2%' }}
                                />
                                <Text style={{ color: blueColor }}>{this.state.lastDigitCard == '' ? "No card added" : "**** **** ****" + this.state.lastDigitCard}  </Text>

                            </View>

                            {/* <TouchableOpacity style={{ width: '18%', marginLeft: '2%', height: '80%', borderRadius: 5, borderWidth: 1, borderColor: '#7f7f7f', justifyContent: 'center', alignItems: 'center', backgroundColor: "#f96467" }}>
                        <Text style={{ color: '#ffffff' }}>DELETE</Text>
                    </TouchableOpacity> */}
                        </View>

                        <View style={{ width: '100%', height: '29%', flexDirection: 'row', marginTop: '5%' }}>
                            <View style={{ width: '78%', paddingHorizontal: '3%', backgroundColor: 'white', marginRight: '2%', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>

                                <View style={{ paddingVertical: '1%', }}>
                                    <TextInput
                                        style={{ width: '100%', height:util.getHeight(7) }}
                                        // underlineColorAndroid='transparent'
                                        placeholder="NAME ON CARD"
                                        placeholderTextColor='#1a5177'

                                        value={this.state.nameOnCard}
                                        onChangeText={t => this.setState({ nameOnCard: t })}
                                        returnKeyType={"next"}
                                    // value={this.state.street}
                                    // onChangeText={street => this.setState({ street: street })}
                                    />

                                    <TextInputMask
                                        style={[
                                            global.defaultBlueColor,
                                            { width: '100%', fontStyle: "italic", height:util.getHeight(7) }
                                        ]}
                                        // underlineColorAndroid='#1a5177'
                                        placeholderTextColor={"#BBBEC0"}
                                        placeholder={"0000 0000 0000 0000"}
                                        placeholderTextColor='#1a5177'
                                        type={'credit-card'}
                                        options={{
                                            format: '0000 0000 0000 0000'
                                        }}
                                        value={this.state.ccNumber}
                                        onChangeText={t => this.setState({ ccNumber: t })}
                                        returnKeyType={"next"}
                                    />

                                    {/* <TextInput
                                    style={{ width: '100%' }}
                                    // underlineColorAndroid='transparent'
                                    maxLength={16}
                                    placeholder="CC NUMBER"
                                    placeholderTextColor='#1a5177'
                                    keyboardType={"numeric"}
                                    value={this.state.ccNumber}
                                    onChangeText={t => this.setState({ ccNumber: t })}
                                    returnKeyType={"next"}

                                // value={this.state.street}
                                // onChangeText={street => this.setState({ street: street })}
                                /> */}
                                    <View style={{ width: '100%', flexDirection: 'row' }}>
                                        <TextInputMask
                                            style={{ width: '60%', height:util.getHeight(7) }}
                                            // underlineColorAndroid='transparent'
                                            placeholder={"MM/YYYY"}
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}
                                            maxLength={7}
                                            type={"datetime"}
                                            options={{
                                                format: "MM/YYYY"
                                            }}
                                            value={this.state.expDate}
                                            onChangeText={t => this.setState({ expDate: t })}

                                        // value={this.state.street}
                                        // onChangeText={street => this.setState({ street: street })}
                                        />
                                        <TextInput
                                            style={{ width: '40%' , height:util.getHeight(7)}}
                                            // underlineColorAndroid='transparent'
                                            placeholder="CCV"
                                            maxLength={3}
                                            placeholderTextColor='#1a5177'
                                            returnKeyType={'next'}
                                            keyboardType={"numeric"}
                                            value={this.state.cCv}
                                            onChangeText={t => this.setState({ cCv: t })}
                                            returnKeyType={"done"}
                                        />
                                    </View>

                                </View>
                            </View>
                            <View style={{ width: '18%', height: '100%' }}>
                                <View style={{ width: '100%', height: '50%', alignItems: 'center', }}>
                                    <TouchableOpacity onPress={() => this.handleSubmit()} style={{ width: '98%', borderRadius: 5, borderWidth: 1, height: '60%', justifyContent: 'center', alignItems: 'center', backgroundColor: blueColor }}>
                                        <Text style={{ color: '#ffffff' }}>ADD</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '100%', height: '50%', alignItems: 'center', }}>
                                    <TouchableOpacity style={{ width: '98%', borderRadius: 5, borderWidth: 1, height: '60%', bottom: 0, position: "absolute", justifyContent: 'center', alignItems: 'center', backgroundColor: "#d0d0d8" }}>
                                        <Text style={{ color: '#1a5177' }}>CLEAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.view ? <TouchableOpacity onPress={() => alert("Card already added")} style={{ width: '100%', height: '100%', backgroundColor: 'rgba(256,256,256,0.6)', position: 'absolute', }}>
                            </TouchableOpacity> : <View></View>
                            }
                        </View>
                        <View style={{ width: '100%', height: '10%', flexDirection: 'row', marginTop: '3%', justifyContent: 'center', alignItems: 'center', }}>

                            <View style={{ width: '78%', flexDirection: 'row', height: '85%', margin: '1%', }}>
                                <TouchableOpacity onPress={() => this.setState({ SubscriptionStatus: true, SubscriptionAmount: '24.99', SubscriptionName: 'MONTHLY' })} style={{ flex: 1, backgroundColor: this.state.SubscriptionStatus ? blueColor : "white", width: '45%', justifyContent: 'center', alignItems: 'center', marginRight: '5%', height: '100%', alignItems: 'center', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Text style={{ color: this.state.SubscriptionStatus ? "white" : blueColor, }}>Monthly</Text>
                                    <Text style={{ color: this.state.SubscriptionStatus ? "white" : blueColor, }}>$ 24.99</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ SubscriptionStatus: false, SubscriptionAmount: '239.88', SubscriptionName: 'YEARLY' })} style={{ flex: 1, backgroundColor: this.state.SubscriptionStatus ? "white" : blueColor, width: '45%', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', height: '100%', alignItems: 'center', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                                    <Text style={{ color: this.state.SubscriptionStatus ? blueColor : "white", }}>Yearly</Text>
                                    <Text style={{ color: this.state.SubscriptionStatus ? blueColor : "white", }}>$ 239.88</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '18%', height: '85%' }}>

                            </View>

                        </View>
                    </View>

                </Content>
            </Container>)
    }
}
export default PaymentsScreens;
