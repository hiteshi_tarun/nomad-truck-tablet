import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Alert, Image, ImageBackground } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  FooterTab,
  Item,
  Left,
  Right,
  Button,
  Body,
  Card,
  Label,
  Footer,
  Form,
  CardItem,

} from "native-base";
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import { Grid, Row, Col } from "react-native-easy-grid";
import Orientation from 'react-native-orientation';
import { resetToMain } from "src/navReset";
import styles from "./styles";
import gbStyle from "@assets/css/global";
import util from "../../common/Util";
import database from "../../common/Database";
import ApiManager from "../../common/ApiManager";
import images from "@assets/images";

import NewScreen from './remotetabs/new';
import FinishScreen from './remotetabs/finished';

import {createMaterialTopTabNavigator} from 'react-navigation'

const Tab = createMaterialTopTabNavigator({

  NewScreen:{
      screen:NewScreen,
       navigationOptions : {
        tabBarIcon: ({ tintColor }) => (
          <Icon1 name="inbox" size={25} color='#1a5177'/>
        ),
      }
  },
  FinishScreen:{
      screen:FinishScreen,
      navigationOptions : {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-checkmark" size={25} color='#1a5177'/>
        ),
      }
  }
},

{
  tabBarPosition:'top',
  swipeEnabled:false,
  scrollEnabled: true,
  lazyLoad: true,
  animationEnabled: false,
  backgroundColor:'yellow',
  initialRouteName:'NewScreen'
,    tabBarOptions:{
      upperCaseLabel: false,
      activeTintColor: '#fff',
      inactiveTintColor: '#bbb',
      showIcon : true,
      showLabel: false,
      labelStyle:{
          fontSize:19,
          
      },
      style: {
    backgroundColor: "white",
    
      },
      indicatorStyle: {
          backgroundColor: 'red',
        
      },
      navigationOptions: ({ navigation }) => ({
       
        }),
  }
});

export default Tab


