const React = require("react-native");
const { StyleSheet } = React;
import util from "../../common/Util";

export default {
  container: {
    height: "90%" ,
  },
  row: {
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  mt: {
    marginTop: 18
  },
  itemTitel: {
    fontWeight: "500",
    marginLeft: 15,
    fontSize: 16,
    color: "gray",
    marginTop: 5
  },
  priceText: {
    marginLeft: 15,
    color: "gray",
    fontSize: 12,
    marginBottom: 5
  },
  gridBg: {
    backgroundColor: "white",
    justifyContent: "center",
    width: util.getWidth(25),
    height: util.getWidth(25),
    margin: 15,
    borderWidth: 0.5,
    borderColor: "black"
  },
  gridIcon: {
    width: util.getWidth(23),
    height: util.getWidth(15),
    marginLeft: 5,
    marginRight: 5
  },
  addItemTopView: {
    alignItems: "center",
    justifyContent: "center"
  },
  plusBtnBg: {
    borderRadius: 40,
    backgroundColor: "#2A409A",
    width: 60,
    height: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  plusTextStyle: {
    textAlign: "center",
    color: "white",
    paddingBottom: 5,
    fontSize: 30,
    color: "white"
  },
  flatListStyle: {
    marginLeft:'4%',marginTop:'2.5%' ,marginBottom:'3%' 
  }
};
