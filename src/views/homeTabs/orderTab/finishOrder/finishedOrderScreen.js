
import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, } from "react-native";
import {
  Container,
  Text,

} from "native-base";

import util from "../../../../common/Util";
// import database from "../../common/Database";
import theme from "../../../../themes/base-theme";
import ApiManager from "../../../../common/ApiManager";
import styles from "./styles";
import Icon from 'react-native-vector-icons/Ionicons';
import KeyString from '../../../../common/Localization'
export default class FinishItemsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      additems: [],
      Data: [],
      declineOrder: false,
      prepareOrder: false,
      spinnerVisible: true,
      DetailView: false,
      object: '',
    }
    this.keyExtractor = this.keyExtractor.bind(this)
  }
  keyExtractor = (item, index) => item.id

  componentDidMount = () => {

    this._getLastFoodFunction();

    //     try {
    //       myInterval = setInterval(async () => this._getLastFoodFunction(), 300000);
    //     } catch(e) {
    //       console.log(e);

    //  }

  }
  _getLastFoodFunction = () => {
    console.log("interval _getLastFoodFunction");
    util.getValueFromStore("AuthToken")
      .then(authToken => {
        util.getValueFromStore("TruckId")
          .then(token => {
            //  truckID = token;
            console.log('get All Food truckID ', token);
            ApiManager._getLastFoodOrders(token, authToken)
              .then((response) => response.json())
              .then(res => {
                this.setState({ spinnerVisible: false })
                // for (let index = 0; index < res.length; index++) {
                //   const element = array[index];
                // }

                if (res.status == 200) {

                  console.log("_getCurrentFoodOrders", res.data.length)
                  var element = [];
                  for (let index = 0; index < res.data.length; index++) {
                    element.push(res.data[index])

                  }
                  console.log("order last", element)
                  this.setState({
                    Data: element
                  }, () => {
                    console.log(this.state)
                  });
                  //  console.log('Get current food  ', res[0].orderItems);
                  console.log('Get Last food orders ', this.state.Data);
                } else {
                  console.log("_getLastFoodOrders error status", res.status)
                }
              }).catch((error) => {
                console.log('Get last food orders error ', error)
              });
          })
          .catch(err => {
            console.log("TruckId token error ", err);
          })
      })
      .catch(err => {
        console.log("AuthId token error ", err);
      });
  }

  // _AddonsData=(item)=>{

  // }
  // _cellDetailView = item => {
  //  console.log("_cellDetailView item:",item)
  //   return (
  //     <View style={{ margin: '2%' }} >
  //       <View style={{
  //         backgroundColor: "white",
  //         flexDirection: 'row',
  //         width: '100%',
  //         height: util.getHeight(8),

  //       }}>
  //         <View style={{ flexDirection: 'row', width: '100%', justifyContent: "center", alignItems: 'center' }}>
  //           <View style={{ width: '60%', marginVertical: '2.5%', marginHorizontal: '3%' }}>
  //             <View style={{ height: '55%'}} >
  //               <Text style={{ color: "#1a5177", }}>{item.foodMenuItem.name}</Text>
  //             </View>
  //           </View>
  //           <Text style={{color:'#1a5177', width: '30%' }}>$ {item.foodMenuItem.price}</Text>
  //         </View>

  //           <FlatList
  //           style={{ marginLeft: '1%', marginTop: '2.5%', marginBottom: '3%' }}
  //           data={item.foodMenuItem.foodAddons == null ? [] : item.foodMenuItem.foodAddons}
  //           renderItem={({ item }) => this._AddonsData(item)}
  //           keyExtractor={this.keyExtractor}
  //         />
  //       </View>
  //     </View>
  //   );
  // };

  _assigndetail = (item) => {
    console.log("click item:", item)
    this.setState({ DetailView: true, object: item })
  }
  // data=(item)=>{
  //   console.log("_AddonsData item1:",item)
  // }
  _dataAddons = (item) => {
    console.log("_AddonsData item:", item)
    var element = [];
    for (let index = 0; index < item.length; index++) {
      element.push(item[index].name)
      console.log(element)
    }
    return (
      <View style={{}} >
        {element.map((element, key) => (
          <Text style={styles.addonsText}> _{element} </Text>)
        )}
      </View>
    );
  }
  _cellDetailView = item => {
    console.log("_cellDetailView item:", item.foodMenuItem.foodAddons)
    return (
      <View style={styles.shippingItemView}>
        <View style={styles.shippingItemBg}>
          <View style={styles.shippingItemBg2}>
            <View style={styles.shippingItemBg }>
              <Text style={{ color: "#1a5177", fontWeight: 'bold' }}>{item.foodMenuItem.name}</Text>
              <Text style={{ color: "#7F7F7F", }}> * {item.amount}</Text>
            </View>
          </View>
          <View style={{ width: '30%', alignItems: 'center' }}>
            <Text style={{ color: '#1a5177' }}>$ {item.foodMenuItem.price == null ? "00.00" : item.foodMenuItem.price}</Text>
          </View>
        </View>
        {this._dataAddons(item.foodMenuItem.foodAddons)}
      </View>
    );
  };

  _navigateToShowData = (item) => {
    console.log("click item priceTotal:", item)
    //   this.setState({DetailView:true})
    return (
      <View style={styles.detailsViewBg1}>
        <View style={styles.statusViewBg}>
          <View style={[styles.statusViewBg2, { backgroundColor: item.orderStatus.status == KeyString.deliveredText ? theme.appDefaultColor : theme.buttonPinkBg }]} >
            <Text style={{ color: item.orderStatus.status == KeyString.deliveredText ? theme.yellowBtnBg : theme.white, fontSize: 12 }}>{item.orderStatus.status}</Text>
          </View>

        </View>
        <Text style={styles.customerText}>{item.customer.firstName == null ? KeyString.customerText : item.customer.firstName}</Text>
        <Text style={styles.customerIdText}>{item.id}</Text>
        <FlatList
          style={styles.detailFlatListBg}
          data={item.orderItems == null ? [] : item.orderItems}
          renderItem={({ item }) => this._cellDetailView(item)}
          keyExtractor={this.keyExtractor}
        />
        <Text style={styles.totalText}>Total  {item.currency}   {item.totalAmountWithTax}</Text>
      </View>
    )
  }

  _CellListView = item => {
    return (
      <View style={styles.listOrderBg} onPress={() => this._assigndetail(item)}>
        <View style={styles.listOrderNextBg}>
          <View style={styles.listOrderNextBg2}>
            <View style={styles.orderStatusBtnView}>
              <TouchableOpacity
                onPress={() => this._assigndetail(item)}
                style={[styles.orderStatusBtnBg, { backgroundColor: item.orderStatus.status == KeyString.deliveredText ? theme.appDefaultColor : theme.buttonPinkBg , borderColor: item.orderStatus.status == KeyString.deliveredText ? theme.appDefaultColor : theme.buttonPinkBg }]} >
                <Text style={{ color: item.orderStatus.status == KeyString.deliveredText ? theme.yellowBtnBg : theme.white , fontSize: 12 }}>{item.orderStatus.status}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.orderIdBg}>
              <Text>{item.orderId == null ? "No content" : item.orderId}</Text>
            </View>
            <Text style={styles.orderAmount}>{item.currency} {item.totalAmountWithTax}</Text>
            <View style={styles.listArrowBg}>
              <Icon
                name="ios-arrow-forward"
                size={20}
                style={{ color: theme.appDefaultColor }}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  _clickmessage = () => {
        return (
          <View style={styles.clickMessageBg}>
            <View style={styles.clickMessageBg2}>
              <Text style={styles.clickText}>{KeyString.detailsMessage}</Text>
            </View>
          </View>
        )
      }
  render() {
    return (
      <Container style={styles.container}>
               <View style={styles.flatListBg}>
                <FlatList
                  style={styles.flatListStyle}
                  data={this.state.Data == null ? [] : this.state.Data}
                   renderItem={({ item }) => this._CellListView(item)}
                   keyExtractor={this.keyExtractor}
                />
               </View>
      
               <View style={styles.detailsViewBg}>
                {
                   this.state.DetailView ? this._navigateToShowData(this.state.object) : this._clickmessage()
                 }
      
               </View>
             </Container>

    );
  };
}

// import React, { Component } from "react";
// import { TouchableOpacity, FlatList, View, } from "react-native";
// import {
//   Container,
//   Text,

// } from "native-base";

// import util from "../../../../common/Util";
// // import database from "../../common/Database";
// import theme from "../../../../themes/base-theme";
// import ApiManager from "../../../../common/ApiManager";
// import styles from "./styles";
// import Icon from 'react-native-vector-icons/Ionicons';
// import KeyString from '../../../../common/Localization'
// export default class FinishItemsScreen extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       additems: [],
//       Data: [],
//       declineOrder: false,
//       prepareOrder: false,
//       spinnerVisible: true,
//       DetailView: false,
//       object: '',
//     }
//     this.keyExtractor = this.keyExtractor.bind(this)
//   }
//   keyExtractor = (item, index) => item.id

//   componentDidMount = () => {

//     this._getLastFoodFunction();

//     //     try {
//     //       myInterval = setInterval(async () => this._getLastFoodFunction(), 300000);
//     //     } catch(e) {
//     //       console.log(e);

//     //  }

//   }
//   _getLastFoodFunction = () => {
//     console.log("interval _getLastFoodFunction");
//     util.getValueFromStore("AuthToken")
//       .then(authToken => {
//         util.getValueFromStore("TruckId")
//           .then(token => {
//             //  truckID = token;
//             console.log('get All Food truckID ', token);
//             ApiManager._getLastFoodOrders(token, authToken)
//               .then((response) => response.json())
//               .then(res => {
//                 this.setState({ spinnerVisible: false })
//                 // for (let index = 0; index < res.length; index++) {
//                 //   const element = array[index];
//                 // }

//                 if (res.status == 200) {

//                   console.log("_getCurrentFoodOrders", res.data.length)
//                   var element = [];
//                   for (let index = 0; index < res.data.length; index++) {
//                     element.push(res.data[index])

//                   }
//                   console.log("order last", element)
//                   this.setState({
//                     Data: element
//                   }, () => {
//                     console.log(this.state)
//                   });
//                   //  console.log('Get current food  ', res[0].orderItems);
//                   console.log('Get Last food orders ', this.state.Data);
//                 } else {
//                   console.log("_getLastFoodOrders error status", res.status)
//                 }
//               }).catch((error) => {
//                 console.log('Get last food orders error ', error)
//               });
//           })
//           .catch(err => {
//             console.log("TruckId token error ", err);
//           })
//       })
//       .catch(err => {
//         console.log("AuthId token error ", err);
//       });
//   }

//   // _AddonsData=(item)=>{

//   // }
//   // _cellDetailView = item => {
//   //  console.log("_cellDetailView item:",item)
//   //   return (
//   //     <View style={{ margin: '2%' }} >
//   //       <View style={{
//   //         backgroundColor: "white",
//   //         flexDirection: 'row',
//   //         width: '100%',
//   //         height: util.getHeight(8),

//   //       }}>
//   //         <View style={{ flexDirection: 'row', width: '100%', justifyContent: "center", alignItems: 'center' }}>
//   //           <View style={{ width: '60%', marginVertical: '2.5%', marginHorizontal: '3%' }}>
//   //             <View style={{ height: '55%'}} >
//   //               <Text style={{ color: "#1a5177", }}>{item.foodMenuItem.name}</Text>
//   //             </View>
//   //           </View>
//   //           <Text style={{color:'#1a5177', width: '30%' }}>$ {item.foodMenuItem.price}</Text>
//   //         </View>

//   //           <FlatList
//   //           style={{ marginLeft: '1%', marginTop: '2.5%', marginBottom: '3%' }}
//   //           data={item.foodMenuItem.foodAddons == null ? [] : item.foodMenuItem.foodAddons}
//   //           renderItem={({ item }) => this._AddonsData(item)}
//   //           keyExtractor={this.keyExtractor}
//   //         />
//   //       </View>
//   //     </View>
//   //   );
//   // };

//   _assigndetail = (item) => {
//     console.log("click item:", item)
//     this.setState({ DetailView: true, object: item })
//   }
//   // data=(item)=>{
//   //   console.log("_AddonsData item1:",item)
//   // }
//   _dataAddons = (item) => {
//     console.log("_AddonsData item:", item)
//     var element = [];
//     for (let index = 0; index < item.length; index++) {
//       element.push(item[index].name)
//       console.log(element)
//     }
//     return (
//       <View style={{}} >
//         {element.map((element, key) => (
//           <Text style={styles.addonsText}> _{element} </Text>)
//         )}
//       </View>
//     );
//   }
//   _cellDetailView = item => {
//     console.log("_cellDetailView item:", item.foodMenuItem.foodAddons)
//     return (
//       <View style={styles.shippingItemView}>
//         <View style={styles.shippingItemBg}>
//           <View style={styles.shippingItemBg2}>
//             <View style={styles.shippingItemBg }>
//               <Text style={{ color: "#1a5177", fontWeight: 'bold' }}>{item.foodMenuItem.name}</Text>
//               <Text style={{ color: "#7F7F7F", }}> * {item.amount}</Text>
//             </View>
//           </View>
//           <View style={{ width: '30%', alignItems: 'center' }}>
//             <Text style={{ color: '#1a5177' }}>$ {item.foodMenuItem.price == null ? "00.00" : item.foodMenuItem.price}</Text>
//           </View>
//         </View>
//         {this._dataAddons(item.foodMenuItem.foodAddons)}
//       </View>
//     );
//   };

//   _navigateToShowData = (item) => {
//     console.log("click item priceTotal:", item)
//     //   this.setState({DetailView:true})
//     return (
//       <View style={styles.detailsViewBg1}>
//         <View style={styles.statusViewBg}>
//           <View style={[styles.statusViewBg2, { backgroundColor: item.orderStatus.status == KeyString.deliveredText ? theme.appDefaultColor : theme.buttonPinkBg }]} >
//             <Text style={{ color: item.orderStatus.status == KeyString.deliveredText ? theme.yellowBtnBg : theme.white, fontSize: 12 }}>{item.orderStatus.status}</Text>
//           </View>

//         </View>
//         <Text style={styles.customerText}>{item.customer.firstName == null ? KeyString.customerText : item.customer.firstName}</Text>
//         <Text style={styles.customerIdText}>{item.id}</Text>
//         <FlatList
//           style={styles.detailFlatListBg}
//           data={item.orderItems == null ? [] : item.orderItems}
//           renderItem={({ item }) => this._cellDetailView(item)}
//           keyExtractor={this.keyExtractor}
//         />
//         <Text style={styles.totalText}>Total  {item.currency}   {item.totalAmountWithTax}</Text>
//       </View>
//     )
//   }

//   _CellListView = item => {
//     return (
//       <View style={styles.listOrderBg} onPress={() => this._assigndetail(item)}>
//         <View style={styles.listOrderNextBg}>
//           <View style={styles.listOrderNextBg2}>
//             <View style={styles.orderStatusBtnView}>
//               <TouchableOpacity
//                 onPress={() => this._assigndetail(item)}
//                 style={[styles.orderStatusBtnBg, { backgroundColor: item.orderStatus.status == KeyString.deliveredText ? theme.yellowBtnBg : theme.buttonAcceptBg, borderColor: item.orderStatus.status == KeyString.deliveredText ? theme.yellowBtnBg : theme.buttonAcceptBg, }]} >
//                 <Text style={{ color: item.orderStatus.status == KeyString.deliveredText ? theme.appDefaultColor : theme.yellowBtnBg, fontSize: 12 }}>{item.orderStatus.status}</Text>
//               </TouchableOpacity>
//             </View>
//             <View style={styles.orderIdBg}>
//               <Text>{item.orderId == null ? "No content" : item.orderId}</Text>
//             </View>
//             <Text style={styles.orderAmount}>{item.currency} {item.totalAmountWithTax}</Text>
//             <View style={styles.listArrowBg}>
//               <Icon
//                 name="ios-arrow-forward"
//                 size={20}
//                 style={{ color: theme.appDefaultColor }}
//               />
//             </View>
//           </View>
//         </View>
//       </View>
//     );
//   };

//   _clickmessage = () => {
//     return (
//       <View style={styles.clickMessageBg}>
//         <View style={styles.clickMessageBg2}>
//           <Text style={styles.clickText}>{KeyString.detailsMessage}</Text>
//         </View>
//       </View>
//     )
//   }
//   render() {
//     return (

//       <Container style={styles.container}>
//         <View style={styles.flatListBg}>
//           <FlatList
//             style={styles.flatListStyle}
//             data={this.state.Data == null ? [] : this.state.Data}
//             renderItem={({ item }) => this._CellListView(item)}
//             keyExtractor={this.keyExtractor}
//           />
//         </View>

//         <View style={styles.detailsViewBg}>>
//           {
//             this.state.DetailView ? this._navigateToShowData(this.state.object) : this._clickmessage()
//           }

//         </View>
//       </Container>

//     );
//   };
// }
