import theme from "../../../../themes/base-theme";
import util from "../../../../common/Util";
import globalStyle from "../../../../assets/css/global"
export default {
    container: [globalStyle.viewFull, {
        flexDirection: 'row'
    }],
    detailsViewBg: {
        width: '35%',
        height: "100%"
    },
    flatListStyle: {
        marginLeft: '1%',
        marginTop: '2.5%',
        marginBottom: '3%'
    },
    flatListBg: {
        width: '65%',
        height: "100%"
    },
    clickMessageBg: {
        height: '90%',
        marginBottom: "5%",
        marginLeft: "3%",
        marginRight: "8%",
        marginTop: "8%",
        backgroundColor: theme.white,
        paddingHorizontal: '5%'
    },
    clickMessageBg2: {
        height: '100%',
        alignItems: 'center'
    },
    clickText: {
        color: theme.appDefaultColor,
        marginTop: '10%'
    },
    addonsText: {
        color: theme.appDefaultColor,
        fontSize: 15,
        marginLeft: '2%'
    },

    // list of orders design
    listOrderBg: {
        margin: '2%'
    },
    listOrderNextBg: {
        backgroundColor: theme.white,
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: theme.buttonGrayBg,
        borderRadius: 5,
        width: '100%',
        height: util.getHeight(8),
    },
    listOrderNextBg2: [globalStyle.viewCenter, {
        flexDirection: 'row',
        width: '100%',
    }],
    orderStatusBtnView: {
        width: '18%',
        marginVertical: '2.5%',
        marginHorizontal: '3%'
    },
    orderStatusBtnBg: [globalStyle.viewCenter, {
        height: '55%',
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    orderIdBg: [globalStyle.viewCenter, {
        width: '54%'
    }],
    orderAmount: {
        width: '14%',
    },
    listArrowBg: [globalStyle.viewCenter, {
        width: '10%',
        height: '100%'
    }],

    //details view design
    detailsViewBg1: {
        height: '90%',
        marginBottom: "5%",
        marginLeft: "3%",
        marginRight: "8%",
        marginTop: "8%",
        backgroundColor: theme.white
    },
    statusViewBg: {
        height: '10%',
        marginTop: '10%',
        alignItems: 'center'
    },
    statusViewBg2: [globalStyle.viewCenter, {
        height: '80%',
        width: '30%',
        borderColor: "gray",
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    customerText: {
        color: theme.appDefaultColor,
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: '10%',
    },
    customerIdText: {
        color: theme.appDefaultColor,
        fontSize: 12,
        marginLeft: '10%'
    },
    detailFlatListBg: {
        marginLeft: '1%',
        marginTop: '2.5%',
        marginBottom: '1%',
    },
    totalText: {
        color: theme.appDefaultColor,
        marginLeft: '10%',
        alignSelf: "flex-end",
        width: '40%'
    },
    //shipping cart design
    shippingItemView: {
        width: '100%'
    },
    shippingItemBg: {
        flexDirection: 'row',
        width: '100%',
        marginVertical: '2.5%',
        alignItems: 'center'
    },
    shippingItemBg2: {
        width: '60%',
        marginHorizontal: '3%',
    },
    shippingItemBg: {
        flexDirection: 'row'
    },
    shippingItemText: {
        color: theme.appDefaultColor,
        fontWeight: 'bold'
    },
    shippingItemPrizeBg: {
        width: '30%',
        alignItems: 'flex-start'
    },
}