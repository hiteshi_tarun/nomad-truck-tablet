import theme from "../../../../themes/base-theme";
import util from "../../../../common/Util";
import globalStyle from "../../../../assets/css/global"
export default {
    container: [globalStyle.viewFull, {
        flexDirection: 'row'
    }],
    spinnerText: {
        color: theme.appDefaultColor
    },
    listOrderBg: {
        margin: '2%'
    },
    listOrderNextBg: {
        backgroundColor: theme.white,
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: theme.buttonGrayBg,
        borderRadius: 5,
        width: '100%',
        height: util.getHeight(8),
    },
    listOrderNextBg2: [globalStyle.viewCenter, {
        flexDirection: 'row',
        width: '100%',
    }],
    orderStatusBtnView: {
        width: '18%',
        marginVertical: '2.5%',
        marginHorizontal: '3%'
    },
    orderStatusBtnBg: [globalStyle.viewCenter, {
        height: '55%',
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    orderIdBg: [globalStyle.viewCenter, {
        width: '54%'
    }],
    orderAmount: {
        width: '14%',
    },
    listArrowBg: [globalStyle.viewCenter, {
        width: '10%',
        height: '100%'
    }],
    flatListBg: {
        width: '65%',
        height: "100%"
    },
    flatListStyle: {
        marginLeft: '1%',
        marginTop: '2.5%',
        marginBottom: '3%'
    },
    detailsViewBg: {
        width: '35%',
        height: "100%"
    },
    statusViewBg: {
        height: '10%',
        marginTop: '10%',
        alignItems: 'center'
    },
    statusViewBg2: [globalStyle.viewCenter, {
        height: '80%',
        width: '30%',
        borderColor: "gray",
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    detailsViewBg1: {
        height: '90%',
        marginBottom: "5%",
        marginLeft: "3%",
        marginRight: "8%",
        marginTop: "8%",
        backgroundColor: theme.white
    },
    detailFlatListBg: {
        marginLeft: '1%',
        marginTop: '2.5%',
        marginBottom: '1%',
    },
    customerText: {
        color: theme.appDefaultColor,
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: '10%',
    },
    customerIdText: {
        color: theme.appDefaultColor,
        fontSize: 12,
        marginLeft: '10%'
    },
    totalAmountBg: {
        width: '100%',
        flexDirection: 'row'
    },
    totalAmountBgText: {
        width: '55%',
        marginLeft: '10%'
    },
    totalAmountBgResult: {
        width: '35%'
    },
    totalText: {
        color: theme.appDefaultColor,
    },
    readyBtnBg: [globalStyle.viewCenter, {
        height: '11%',
        width: '94%',
        marginBottom: '2%',
        marginHorizontal: '3%',
        position: 'absolute',
        bottom: 0,
    }],
    readyBtnBg2: [globalStyle.viewCenter, {
        backgroundColor: theme.appDefaultColor,
        height: '80%',
        borderColor: theme.yellowBtnBg,
        borderWidth: 0.3,
        borderRadius: 5,
        width: '100%',
    }],
    readyText: {
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold'
    },

    preparingBtnBg: [globalStyle.viewCenter, {
        height: '30%',
        width: '94%',
        marginHorizontal: '3%',
        marginVertical: '2%',
    }],
    preparingBtnBg2: [globalStyle.viewCenter, globalStyle.viewFull, {
        backgroundColor: theme.yellowBtnBg,
        borderColor: theme.buttonGrayBg,
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    yesAndNoBtnBg: [globalStyle.viewCenter, {
        alignContent: 'center',
        width: '90%',
        height: '20%',
        borderRadius: 10,
    }],
    noBtnBg: {
        backgroundColor: theme.buttonGrayBg,
    },
    yesBtnBg: {
        backgroundColor: theme.appDefaultColor,
        marginBottom: '2%'
    },
    yesBtnBg2: {
        backgroundColor: theme.yellowBtnBg,
        marginBottom: '2%'
    },
    yesBtnText: {
        color: theme.white,
    },
    noBtnText: {
        color: theme.appDefaultColor
    },
    yesBtnText2: {
        color: theme.appDefaultColor,
    },
    noBtnText2: {
        color: theme.white,
    },
    pickUpMessageText: {
        color: theme.appDefaultColor,
        marginBottom: '20%',
        textAlign: 'center',
        fontSize: 18,
        width: "80%"
    },

    modalBg: [globalStyle.viewCenter, {
        backgroundColor: theme.transparentWhite,
        borderRadius: 10,
        borderWidth: 0.3,
        borderColor: theme.buttonGrayBg,
    }],
    modal1View2Style: {
        height: util.getHeight(50),
        width: util.getWidth(50),
    },
    allModalView1Style: [
        globalStyle.viewCenter,
        globalStyle.viewFull
    ],
    modal1View3Style: [globalStyle.viewCenter, {
        height: '90%',
        width: '90%',
    }],
    modal2View2Style: {
        height: util.getHeight(40),
        width: util.getWidth(40),
    },
    modal2View3Style: [globalStyle.viewCenter, {
        height: '80%',
        width: '90%',
    }],
    // shipping cart design
    shippingItemView: {
        width: '100%'
    },
    shippingItemBg: {
        flexDirection: 'row',
        width: '100%',
        marginVertical: '2.5%',
        alignItems: 'center'
    },
    shippingItemBg2: {
        width: '60%',
        marginHorizontal: '3%',
    },
    shippingItemBg: {
        flexDirection: 'row'
    },
    shippingItemText: {
        color: theme.appDefaultColor,
        fontWeight: 'bold'
    },
    shippingItemPrizeBg: {
        width: '30%',
        alignItems: 'flex-start'
    },
    preparingTextBg: [globalStyle.viewCenter,globalStyle.viewFull, {
        backgroundColor: theme.yellowBtnBg,
        borderColor: theme.buttonGrayBg,
        borderWidth: 0.3,
        borderRadius: 5,
    }],
    preparingText: {
        color: theme.appDefaultColor,
        fontSize: 17,
        fontWeight: 'bold'
    },
    twoBtnBg: {
        height: '22%',
    },
    detailsViewBg: {
        width: '35%',
        height: "100%"
    },
    declineBtnBg2: [globalStyle.viewCenter, {
        height: '30%',
        width: '94%',
        marginHorizontal: '3%',
        marginTop: '4%',
    }],
    declineBtnBg: [globalStyle.viewCenter, {
        backgroundColor: theme.buttonPinkBg,
        borderColor: "gray",
        borderWidth: 0.3,
        borderRadius: 5,
        height: '100%',
        width: '100%',
    }],
    declineBtnText2: {
        color: theme.white,
        fontSize: 17,
        fontWeight: 'bold'
    },
    declineText: {
        color: theme.appDefaultColor,
        marginBottom: '5%',
        textAlign: 'center',
        fontSize: 18,
        width: "90%"
    },
    reasonInputText: {
        borderColor: theme.buttonGrayBg,
        borderWidth: 0.3,
        borderRadius: 5,
        padding: 12,
        backgroundColor: theme.white,
        paddingVertical: 0,
        color: theme.badgeBg,
        fontSize: 14,
        width: "90%",
        height: util.getHeight(17),
        textAlignVertical: "top",
        marginBottom: '7%',
    },
    declineYesBtn: {
        backgroundColor: theme.buttonPinkBg,
        height: '15%',
        marginBottom: '2%'
    },
    clickMessageBg: {
        height: '90%',
        marginBottom: "5%",
        marginLeft: "3%",
        marginRight: "8%",
        marginTop: "8%",
        backgroundColor: theme.white,
        paddingHorizontal: '5%'
    },
    clickMessageBg2: {
        height: '100%',
        alignItems: 'center'
    },
    clickText: {
        color: theme.appDefaultColor,
        marginTop: '10%'
    },
    addonsText: {
        color: theme.appDefaultColor,
        fontSize: 15,
        marginLeft: '2%'
    }
}
