import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Modal, TextInput } from "react-native";
import {
  Container,
  Text,
} from "native-base";
import theme from "../../../../themes/base-theme";
import util from "../../../../common/Util";
import ApiManager from "../../../../common/ApiManager";
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from "./styles";
import KeyString from '../../../../common/Localization'

export default class NewItemsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      additems: [],
      Data: [],
      declineOrder: false,
      prepareOrder: false,
      readyorder: false,
      SpinnerVisible: true,
      DetailView: false,
      object: '',

    }
    this.keyExtractor = this.keyExtractor.bind(this)
  }

  keyExtractor = (item, index) => item.id
  _navigateToAddItem = status => {

    // //additems.push(status);
    // this.setState({
    //   additems: [...this.state.additems, {
    //     name: status.name,
    //     rs: status.rs,
    //   }]
    // });
    // console.log('Api Call ', this.state.additems)

    // this.totalRupees(status.rs);
  };
  componentDidMount = () => {

    this._getCurrentFoodFunction();
    // try {
    //     myInterval = setInterval(async () => this._getCurrentFoodFunction(), 300000);
    // } catch(e) {
    //   console.log(e);
    // }
  }

  _getCurrentFoodFunction = () => {

    console.log("interval _getCurrentFoodFunction");
    util.getValueFromStore("AuthToken")
      .then(authToken => {

        util.getValueFromStore("TruckId")
          .then(token => {
            //  truckID = token;
            console.log('get All Food truckID ', token, authToken);
            ApiManager._getCurrentFoodOrders(token, authToken)
              .then((response) => response.json())
              .then(res => {

                if (res.status == 200) {
                  this.setState({ SpinnerVisible: false })

                  console.log("_getCurrentFoodOrders", res.data.length)
                  var element = [];
                  for (let index = 0; index < res.data.length; index++) {
                    element.push(res.data[index])
                  }
                  console.log("order current", element)
                  this.setState({
                    Data: element
                  }, () => {
                    console.log(this.state)
                  });
                  //  console.log('Get current food  ', res[0].orderItems);
                  console.log('Get current food orders ', this.state.Data);
                } else {
                  console.log("_getCurrentFoodOrders error status", res.status)
                }

              }).catch((error) => {
                this.setState({ SpinnerVisible: false })
                console.log('Get current food orders error ', error)
              });
          })
          .catch(err => {
            console.log("TruckId token error ", err);
          });
      })
      .catch(err => {
        console.log("AuthToken token error ", err);
      });
  }

  _clickmessage = () => {
    return (
      <View style={styles.clickMessageBg}>
        <View style={styles.clickMessageBg2}>
          <Text style={styles.clickText}>{KeyString.detailsMessage}</Text>
        </View>
      </View>
    )
  }
  dataAddons = (item) => {
    console.log("_AddonsData item:", item)
    var element = [];
    for (let index = 0; index < item.length; index++) {
      element.push(item[index].name)
      console.log(element)
    }
    return (
      <View style={{}} >
        {element.map((element, key) => (
          <Text style={styles.addonsText}> _{element} </Text>)
        )}
      </View>
    );
  }

  _cellDetailView = item => {

    return (
      <View style={styles.shippingItemView}>
        <View style={styles.shippingItemBg}>
          <View style={styles.shippingItemBg2}>
            <View style={styles.shippingItemBg }>
              <Text style={styles.shippingItemText}>{item.foodMenuItem.name}</Text>
              {/* <Text style={{ color: theme.appDefaultColor, }}> * {item.amount}</Text> */}
            </View>
          </View>
          <View style={styles.shippingItemPrizeBg}>
            <Text style={{ color: theme.appDefaultColor }}>$ {item.foodMenuItem.price == null ? "00.00" : item.foodMenuItem.price}</Text>
          </View>
        </View>
        {this.dataAddons(item.foodMenuItem.foodAddons)}
      </View>
    );
  };

  _detailsBtnView = () => {
    return (
      <View style={styles.twoBtnBg}>
        <View style={styles.preparingBtnBg}>
          <TouchableOpacity onPress={() => this.setState({ prepareOrder: true })} style={styles.preparingTextBg} >
            <Text style={styles.preparingText}>{KeyString.preparingBtn}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.declineBtnBg2}>
          <TouchableOpacity onPress={() => this.setState({ declineOrder: true })} style={styles.declineBtnBg} >
            <Text style={styles.declineBtnText2}>{KeyString.declineBtn}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _readyBtnView = () => {
    return (

      <View style={styles.readyBtnBg}>
        <TouchableOpacity onPress={() => this.setState({ readyorder: true })}
          style={styles.readyBtnBg2} >
          <Text style={styles.readyText}>{KeyString.readyBtn}</Text>
        </TouchableOpacity>
      </View>

    )
  }
  _navigateToShowData = (item) => {
    return (
      <View style={styles.detailsViewBg1}>
        <View style={styles.statusViewBg}>
          <View style={[styles.statusViewBg2,{ backgroundColor: item.orderStatus.status == KeyString.receivedText ? theme.yellowBtnBg : theme.buttonAcceptBg }]} >
            <Text style={{ color: item.orderStatus.status == KeyString.receivedText ? theme.appDefaultColor : theme.yellowBtnBg, fontSize: 12 }}>{item.orderStatus.status}</Text>
          </View>
        </View>
        <Text style={styles.customerText}>{item.customer.firstName == null ? KeyString.customerText : item.customer.firstName}</Text>
        <Text style={styles.customerIdText}>{item.id}</Text>

        <FlatList
          style={styles.detailFlatListBg}
          data={item.orderItems == null ? [] : item.orderItems}
          renderItem={({ item }) => this._cellDetailView(item)}
          keyExtractor={this.keyExtractor}
        />

        <View style={styles.totalAmountBg}>
          <View style={styles.totalAmountBgText}>
            <Text style={styles.totalText}>Total</Text>
          </View>
          <View style={styles.totalAmountBgResult}>
            <Text style={styles.totalText}>{item.currency}  {item.totalAmountWithTax}</Text>
          </View>
        </View>

        {item.orderStatus.status == KeyString.receivedText ? this._detailsBtnView() : this._readyBtnView()}

      </View>
    )
  }
  _AddonsData = (item) => {

  }
  _assigndetail = (item) => {
    console.log("click item:", item)
    this.setState({ DetailView: true })
    this.setState({ object: item })
  }

  preparingItem = () => {
    this.setState({ SpinnerVisible: true })
    util.getValueFromStore("AuthToken")
      .then(authToken => {
        ApiManager._sendAcceptOrderState(authToken, this.state.object.orderId, this.state.object.id)
          .then(res => {
            if (res.status == 200) {
              alert("order accepted" + res.status)
              this.setState({ SpinnerVisible: false, prepareOrder: false })
              console.log('preparing food orders ', res);
              this._getCurrentFoodFunction();
            } else {
              this.setState({ SpinnerVisible: false, prepareOrder: false })
              alert("order accepted error" + res.status)
            }

          }).catch((error) => {
            this.setState({ SpinnerVisible: false })
            alert("Order preparing error");
            console.log('preparing food orders error ', error)
          })
      }).catch((error) => {
        this.setState({ SpinnerVisible: false })
        alert("Authid error");

      });
  }

  declineItem = () => {
    this.setState({ SpinnerVisible: true })
    util.getValueFromStore("AuthToken")
      .then(authToken => {
        ApiManager._sendDeclineOrderState(authToken, this.state.object.orderId, this.state.object.id)
          .then(res => {
            this.setState({ SpinnerVisible: false, declineOrder: false })
            if (res.status == 200) {
              alert("Order  decline" + res.status);
              this._getCurrentFoodFunction();
              console.log(' decline food orders ', res);
            }
            else {
              alert("Order  decline error " + res.status);
            }
          }).catch((error) => {
            alert("Order  decline error");
            console.log('decline food orders error ', error)
          })
      }).catch((error) => {
        this.setState({ SpinnerVisible: false })
        alert("Authid error");

      });
  }

  readyItem = () => {
    this.setState({ SpinnerVisible: true })
    //alert("orderId"+this.state.object.orderId);
    util.getValueFromStore("AuthToken")
      .then(authToken => {
        ApiManager._sendFinishOrderState(authToken, this.state.object.orderId, this.state.object.id)
          .then((response) => response.json())
          .then(res => {
            this.setState({ SpinnerVisible: false, readyorder: false })
            alert("Order  Finish");
            this._getCurrentFoodFunction();
            console.log(' Finish food orders ', this.state.Data);
          }).catch((error) => {
            this.setState({ SpinnerVisible: false, readyorder: false })
            alert("Order Finish error");
            console.log('Finish food orders error ', error)
          })
      }).catch((error) => {
        this.setState({ SpinnerVisible: false })
        alert("Authid error");

      });
  }

  _CellListView = item => {
    return (
      <View style={styles.listOrderBg}>
        <View style={styles.listOrderNextBg}>
          <View style={styles.listOrderNextBg2}>
            <View style={styles.orderStatusBtnView}>
              <TouchableOpacity onPress={() => this._assigndetail(item)} style={[styles.orderStatusBtnBg,{backgroundColor: item.orderStatus.status == KeyString.receivedText ? theme.yellowBtnBg : theme.buttonAcceptBg, borderColor: item.orderStatus.status == KeyString.receivedText ? theme.yellowBtnBg : theme.buttonAcceptBg,}]} >
                <Text style={{ color: item.orderStatus.status == KeyString.receivedText ? theme.appDefaultColor : theme.yellowBtnBg, fontSize: 12 }}>{item.orderStatus.status}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.orderIdBg}>
              <Text>{item.orderId == null ? "No content" : item.orderId}</Text>
            </View>
            <Text style={styles.orderAmount}>{item.currency} {item.totalAmountWithTax}</Text>
            <View style={styles.listArrowBg}>
              <Icon
                name="ios-arrow-forward"
                size={20}
                style={{ color: theme.appDefaultColor }}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };
  render() {
    return (

      <Container style={styles.container}>

        <Spinner visible={this.state.SpinnerVisible} textContent={KeyString.loadingText} color={theme.appDefaultColor} overlayColor={theme.spinnerOverlay} textStyle={styles.spinnerText} />
      {/* The pickUp decline modal start form here*/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.declineOrder}>
          <View style={styles.allModalView1Style}>
            <View style={[styles.modalBg, styles.modal1View2Style]} >
              <View style={styles.modal1View3Style}>
                <Text style={styles.declineText}>{KeyString.declineMessage}</Text>
                <TextInput
                  style={styles.declineText}
                  placeholder={KeyString.reasonInputPlaceholder}
                  underlineColorAndroid='transparent'
                  returnKeyType={'next'}
                />
                <TouchableOpacity onPress={() => this.declineItem()} style={[styles.yesAndNoBtnBg, styles.declineYesBtn]}>
                  <Text style={styles.yesBtnText}>{KeyString.yesBtnText}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.setState({ declineOrder: false })} style={[styles.yesAndNoBtnBg, styles.noBtnBg, { height: '15%' }]}>
                  <Text style={styles.noBtnText}>{KeyString.noBtnText}</Text>
                </TouchableOpacity>

              </View>
            </View>
          </View>
        </Modal>
        {/* The pickUp preparing modal start form here*/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.prepareOrder}>
          <View style={styles.allModalView1Style}>
            <View style={[styles.modalBg, styles.modal2View2Style]}>
              <View style={styles.modal2View3Style}>
                <Text style={styles.preparingText}>{KeyString.preparingMessage}</Text>
                <TouchableOpacity onPress={() => this.preparingItem()} style={[styles.yesBtnBg2, styles.yesAndNoBtnBg]}>
                  <Text style={styles.yesBtnText2}>{KeyString.yesBtnText}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.setState({ prepareOrder: false })} style={[styles.noBtnBg, styles.yesAndNoBtnBg]}>
                  <Text style={styles.noBtnText2}>{KeyString.noBtnText}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        {/* The pickUp permission modal start form here*/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.readyorder}>

          <View style={styles.allModalView1Style}>
            <View style={[styles.modalBg, styles.modal2View2Style]}>
              <View style={styles.modal2View3Style}>
                <Text style={styles.preparingText}>{KeyString.pickUpMessage}</Text>
                <TouchableOpacity onPress={() => this.readyItem()} style={[styles.yesBtnBg, styles.yesAndNoBtnBg]}>
                  <Text style={styles.yesBtnText}>{String.yesBtnText}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ readyorder: false })} style={[styles.noBtnBg, styles.yesAndNoBtnBg]}>
                  <Text style={styles.noBtnText}>{KeyString.noBtnText}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.flatListBg}>
          <FlatList
            style={styles.flatListStyle}
            data={this.state.Data == null ? [] : this.state.Data}
            renderItem={({ item }) => this._CellListView(item)}
            keyExtractor={this.keyExtractor}
          />
        </View>
        <View style={styles.detailsViewBg}>
          {
            this.state.DetailView ? this._navigateToShowData(this.state.object) : this._clickmessage()
          }
        </View>
      </Container>

    );
  };
}
