import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Alert, Image, ImageBackground } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  FooterTab,
  Item,
  Left,
  Right,
  Button,
  Body,
  Card,
  Label,
  Footer,
  Form,
  CardItem,

} from "native-base";
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import NewOrderScreen from './newOrder/newOrderScreen';
import FinishedOrderScreen from './finishOrder/finishedOrderScreen';
import { createMaterialTopTabNavigator } from 'react-navigation';
import theme from "../../../themes/base-theme";
const Tab = createMaterialTopTabNavigator({

  NewOrderScreen: {
    screen: NewOrderScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon1 name="inbox" size={25} color={theme.appDefaultColor} />
      ),
    }
  },
  FinishedOrderScreen: {
    screen: FinishedOrderScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-checkmark" size={25} color={theme.appDefaultColor} />
      ),
    }
  }
},

  {
    tabBarPosition: 'top',
    swipeEnabled: false,
    scrollEnabled: true,
    lazyLoad: true,
    animationEnabled: false,
    initialRouteName: 'NewOrderScreen'
    , tabBarOptions: {
      upperCaseLabel: false,
      activeTintColor: theme.white,
      inactiveTintColor: theme.buttonGrayBg,
      showIcon: true,
      showLabel: false,
      labelStyle: {
        fontSize: 19,
      },
      style: {
        backgroundColor: theme.white,
      },
      indicatorStyle: {
        backgroundColor: theme.badgeBg,
      },
      navigationOptions: ({ navigation }) => ({

      }),
    }
  });

export default Tab


