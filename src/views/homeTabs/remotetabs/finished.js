import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Alert, Image, ImageBackground, Modal, TextInput } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  FooterTab,
  Item,
  Left,
  Right,
  Button,
  Body,
  Card,
  Label,
  Footer,
  Form,
  CardItem,

} from "native-base";


import { resetToMain } from "src/navReset";

import gbStyle from "@assets/css/global";
import util from "../../../common/Util";
// import database from "../../common/Database";
import ApiManager from "../../../common/ApiManager";
import Spinner from 'react-native-loading-spinner-overlay';
import images from "@assets/images";
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Entypo';
import index from "react-native-uuid-generator";



var myInterval;
export default class FinishItemsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      additems: [],
      Data: [],
      declineOrder: false,
      prepaireOrder: false,
      Spinnervisible: true,
      DetailView: false,
      object: '',
    }
    this.keyExtractor = this.keyExtractor.bind(this)// default screen index
  }
  // static navigationOptions = {
  //   tabBarLabel: '',
  //   tabBarIcon: ({ tintColor }) => (
  //     <Icon name="md-mail-open" size={25} color='#1a5177'/>
  //   ),
  // };
  keyExtractor = (item, index) => item.id

  componentDidMount = () => {

    this.getlastfoodfunction();

  //     try {
  //       myInterval = setInterval(async () => this.getlastfoodfunction(), 300000);
  //     } catch(e) {
  //       console.log(e);
    
  //  }
  
  }
  getlastfoodfunction=()=>{
    console.log("interval getlastfoodfunction");
    util.getValueFromStore("AuthToken")
    .then(authtoken => {
    util.getValueFromStore("TruckId")
      .then(token => {
        //  truckID = token;
        console.log('get All Food truckID ', token);
        ApiManager._getLastFoodOrders(token,authtoken)
        .then((response) => response.json())
          .then(res => {
            this.setState({ Spinnervisible: false })
            // for (let index = 0; index < res.length; index++) {
            //   const element = array[index];
            // }

            if(res.status == 200){
        
              console.log("_getCurrentFoodOrders",res.data.length)
              var element = [];
              for (let index = 0; index < res.data.length; index++) {
                element.push(res.data[index])
              
              }
              console.log("order last",element)
              this.setState({
                Data: element
              }, () => {
                console.log(this.state)
              });
            //  console.log('Get current food  ', res[0].orderItems);
              console.log('Get Last food orders ', this.state.Data);
            }else{
              console.log("_getLastFoodOrders error status",res.status)
            }
          }).catch((error) => {
            console.log('Get last food orders error ', error)
          });
      })
      .catch(err => {
        console.log("TruckId token error ", err);
      })
    })
    .catch(err => {
      console.log("AuthId token error ", err);
    });
  }

  // _AddonsData=(item)=>{

  // }
  // _CellDetailview = item => {
  //  console.log("_CellDetailview item:",item)
  //   return (
  //     <View style={{ margin: '2%' }} >
  //       <View style={{
  //         backgroundColor: "white",
  //         flexDirection: 'row',
  //         width: '100%',
  //         height: util.getHeight(8),

  //       }}>
  //         <View style={{ flexDirection: 'row', width: '100%', justifyContent: "center", alignItems: 'center' }}>
  //           <View style={{ width: '60%', marginVertical: '2.5%', marginHorizontal: '3%' }}>
  //             <View style={{ height: '55%'}} >
  //               <Text style={{ color: "#1a5177", }}>{item.foodMenuItem.name}</Text>
  //             </View>
  //           </View>
  //           <Text style={{color:'#1a5177', width: '30%' }}>$ {item.foodMenuItem.price}</Text>
  //         </View>

  //           <FlatList
  //           style={{ marginLeft: '1%', marginTop: '2.5%', marginBottom: '3%' }}
  //           data={item.foodMenuItem.foodAddons == null ? [] : item.foodMenuItem.foodAddons}
  //           renderItem={({ item }) => this._AddonsData(item)}
  //           keyExtractor={this.keyExtractor}
  //         />
  //       </View>
  //     </View>
  //   );
  // };
  dataAddons = (item) => {
    console.log("_AddonsData item:", item)
    var element=[];
    for (let index = 0; index < item.length; index++) {
      element.push(item[index].name)
      console.log(element)
    }
    return (
      <View style={{}} >
       { element.map((element, key)=>(
          <Text style={{color: "#1a5177",fontSize:15,marginLeft:'2%' }} > _{element} </Text>)
         )}
      </View>
    );
  }
  // data=(item)=>{
  //   console.log("_AddonsData item1:",item)
  // }
  _CellDetailview = item => {
    console.log("_CellDetailview item:", item.foodMenuItem.foodAddons)
    return (
        <View style={{
          width: '100%',
        }}>

          <View style={{ flexDirection: 'row', width: '100%',marginVertical: '2.5%', alignItems: 'center' }}>
            <View style={{ width: '60%',marginHorizontal: '3%', }}>
              <View style={{flexDirection:'row' }} >
                <Text style={{ color: "#1a5177",fontWeight:'bold' }}>{item.foodMenuItem.name}</Text>
                <Text style={{ color: "#7F7F7F", }}> * {item.amount}</Text>
              </View>
            </View>
            <View style={{width: '30%',alignItems:'center'}}>
            <Text style={{ color: '#1a5177'}}>$ {item.foodMenuItem.price == null?"00.00":item.foodMenuItem.price}</Text>
            </View>
          </View>
          {this.dataAddons(item.foodMenuItem.foodAddons)}
        </View>
    );
  };

  _navigateToShowData = (item) => {
     console.log("click item priceTotal:",item)
    //   this.setState({DetailView:true})
    return (
      <View style={{ height: '90%', marginBottom: "5%", marginLeft: "3%", marginRight: "8%", marginTop: "8%",backgroundColor:'white'}}>
        <View style={{ height: '10%',marginTop: '10%',alignItems:'center'}}>
          <View style={{ height: '80%',  width: '30%', backgroundColor: item.orderStatus.status == "DELIVERED" ? '#1a5177' : "#FD6767", borderColor: "gray", borderWidth: 0.3, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }} >
            <Text style={{ color: item.orderStatus.status == "DELIVERED" ? '#FCE68F' : "white", fontSize: 12 }}>{item.orderStatus.status}</Text>
          </View>
        
        </View>
        <Text style={{ color: '#1a5177', fontSize: 18, fontWeight: 'bold', marginLeft: '10%', }}>{item.foodTruck.firstName == null ? "Customer name" : item.foodTruck.firstName+" "}{item.foodTruck.lastName == null ? "" : item.foodTruck.lastName}</Text>
        <Text style={{ color: '#1a5177', fontSize: 12,marginLeft:'10%'  }}>{item.id}</Text>
        <FlatList
            style={{ marginLeft: '1%', marginTop: '2.5%', marginBottom: '3%'}}
            data={item.orderItems == null ? [] : item.orderItems}
            renderItem={({ item }) => this._CellDetailview(item)}
            keyExtractor={this.keyExtractor}
          />
             <Text style={{ color: '#1a5177',marginLeft:'10%' ,alignSelf:"flex-end",width:'40%' }}>Total  {item.currency}   {item.totalAmountWithTax}</Text>
      </View>
    )
  }
  _assigndetail = (item) => {
    console.log("click item:", item)
    this.setState({ DetailView: true })
    this.setState({ object: item })
  }
  _CellListView = item => {
    // console.log("_CellListView item:",item)
    return (
      <View style={{ margin: '2%' }} onPress={() => this._assigndetail(item)}>
        <View style={{
          backgroundColor: "white",
          flexDirection: 'row',
          borderWidth: 0.5,
          borderColor: "gray", borderRadius: 5,
          width: '100%',
          height: util.getHeight(8),

        }}>
          <View style={{ flexDirection: 'row', width: '100%', justifyContent: "center", alignItems: 'center' }}>
            <View style={{ width: '20%', marginVertical: '2.5%', marginHorizontal: '3%' }}>
              <TouchableOpacity  onPress={() => this._assigndetail(item)} style={{ height: '55%', backgroundColor: item.orderStatus.status == "DELIVERED" ? '#1a5177' : "#FD6767", borderColor: "gray", borderWidth: 0.3, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }} >
                <Text style={{ color: item.orderStatus.status == "DELIVERED" ? '#FCE68F' : "white", fontSize: 12 }}>{item.orderStatus.status}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: '56%', alignItems: 'center', justifyContent: 'center' }}>
            <Text>{item.orderId == null ? "No content" : item.orderId}</Text>
            </ View>
            <Text style={{ width: '10%' }}>{item.currency} {item.totalAmountWithTax}</Text>
            <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
              <Icon
                name="ios-arrow-forward"
                size={20}
                style={{ color: "#1a5177" }}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  _clickmessage = () => {
    return (
      <View style={{ height: '90%', marginBottom: "5%", marginLeft: "3%", marginRight: "8%", backgroundColor: 'white',  marginTop: "8%",paddingHorizontal:'5%' }}>
        <View style={{ height: '100%',alignItems: 'center' }}>
          <Text style={{ color: '#1a5177', marginTop: '10%' }}>Click on an order to see details here.</Text>
        </View>
      </View>
    )
  }
  render() {
    return (

      <Container style={{ width: '100%', height: "100%", flexDirection: 'row' }}>
        <View style={{ width: '65%', height: "100%", }}>
          <FlatList
            style={{ marginLeft: '1%', marginTop: '2.5%', marginBottom: '3%' }}
            data={this.state.Data == null ? [] : this.state.Data}
            renderItem={({ item }) => this._CellListView(item)}

            keyExtractor={this.keyExtractor}
          />
        </View>

        <View style={{ width: '35%', height: "100%" }}>
          {
            this.state.DetailView ? this._navigateToShowData(this.state.object) : this._clickmessage()
          }
    
        </View>
      </Container>

    );
  };
}
