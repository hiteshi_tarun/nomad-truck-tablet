import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Alert, Image, ImageBackground, Modal, AsyncStorage } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  FooterTab,
  Item,
  Left,
  Right,
  Button,
  Body,
  Card,
  Label,
  Footer,
  Form,
  CardItem,
} from "native-base";

import ReaderSDK from 'react-native-sp-reader-sdk';
import UUIDGenerator from 'react-native-uuid-generator';
import Spinner from 'react-native-loading-spinner-overlay';
import { Grid, Row, Col } from "react-native-easy-grid";
import Orientation from 'react-native-orientation';
import { resetToMain } from "src/navReset";
import styles from "./styles";
import gbStyle from "@assets/css/global";
import util from "../../common/Util";
import database from "../../common/Database";
import ApiManager from "../../common/ApiManager";
import images from "@assets/images";

const readerSdk = "";
export default class paersonScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      additems: [],
      Data: [],
      Spinnervisible: true,
      total: 0,
      deleteitems: false,
      deleteIndex: null,
      height: null,
      width: null,
      ChackoutClick: false,
      UUID: null,
      showDefault: true,
      deleteIndexdata:'',
    }
    readerSdk = new ReaderSDK();

    // initialize the sdk
    readerSdk.initSdk().then(() => {
      // success
      console.log("sucessfully initialize in renderSDK");
    }).catch((err) => {
      // failure
      console.log("failed to initialize in renderSDK");
    })
  }
  componentDidMount = () => {
    util.getValueFromStore("AuthToken")
      .then(authtoken => {

        util.getValueFromStore("TruckId")
          .then(token => {
            //  truckID = token;
          //  alert('TruckId' + token);
            console.log('get All Food truckID ', token);
            ApiManager._getAllFoodMenu(token, authtoken)
              .then((response) => response.json())
              .then(res => {

                if (res.status == 200) {
                  this.setState({ Spinnervisible: false })
                  this.setState({ Data: res.data.menuItems });
                  console.log('get All Food Menu ', res.data.menuItems);
                } else {
                  this.setState({ Spinnervisible: false })
                  alert("There is something wrong status -" + res.status)
                }

              }).catch((error) => {
                this.setState({ Spinnervisible: false })
                console.log('Food menu error ' + error)
              });
          })
          .catch(err => {
            console.log("TruckId token error ", err);
          })
      })
      .catch(err => {
        console.log("AuthToken token error ", err);
      });



    this.DetectOrientation();
    AsyncStorage.getItem('height').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })
  }



  _navigateToAddItem = status => {

    //additems.push(status);
    this.setState({
      additems: [...this.state.additems, {
        name: status.name,
        price: status.price,
      }]
    });
    console.log('Shopping card item list ', this.state.additems)

    this.totalrupee(status.price);
  };
  totalrupee = (value) => {
    // var f1 = this.state.total;
    // var f2 = value ;
    //     const result =  f2 + f1 
    console.log("total" + this.state.total + "  " + value)
    var total_rupee = (this.state.total + value);
    console.log("total rupees  " + total_rupee)
    //  total_rupee = parseFloat(total_rupee).toFixed(1)
    this.setState({ total: total_rupee })
    console.log("total rupees  " + this.state.total)
  }
  totalrupeecut = (value) => {
    // var f1 = this.state.total;
    // var f2 = value ;
    //     const result =  f2 + f1 
    console.log("total" + this.state.total + "  " + value)
    var total_rupee = (this.state.total - value);
    console.log("total rupees  " + total_rupee)
    //  total_rupee = parseFloat(total_rupee).toFixed(1)
    this.setState({ total: total_rupee })
    console.log("total rupees  " + this.state.total)
  }

  _callApi = () => {

    ApiManager.getAccount()
      .then(response => {
        console.log('Api Call ', response)

      }).catch((error) => {
        console.log('Api Error ', error)

      });

  }
  navigateToAddItem = status => {

    //additems.push(status);
    this.setState({
      additems: [...this.state.additems, {
        name: status.name,
        rs: status.rs,
      }]
    });
    console.log('Api Call ', this.state.additems)

    this.totalrupee(status.rs);
  };
  onItemsPress = (item, index) => {
    console.log("delete item key", index, "", item)
    this.setState({ deleteitems: true })
    this.setState({ deleteIndex: index })
    this.setState({ deleteIndexdata: item })
  }

  _gridCellView = item => {
    //    console.log("item is ",item.picture.contentsContentType);
    var icon = this.state.showDefault ? require('../../../src/assets/imgs/image_placeholder.png') : { uri: `data:${item.picture.contentsContentType};base64,${item.picture.contents}` };
    return (
      <TouchableOpacity onPress={() => this._navigateToAddItem(item)}>
        <View style={{
          backgroundColor: "white",
          justifyContent: "center",
          width: this.state.width * .15,
          height: this.state.width * .15,
          margin: 15,
          borderWidth: 0.5,
          borderColor: "black"
        }}>
          <Image style={{
            width: "100%",
            height: this.state.width * .10,
          }}
            source={icon}
            onLoadStart={() => this.setState({ showDefault: true })}
            onLoad={() => this.setState({ showDefault: false })}
          />
          <Text style={styles.itemTitel}>{item.name}</Text>
          <Text style={styles.priceText}>$ {item.price/100}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  _dataCellView = (item, index) => {
    return (
      <TouchableOpacity onPress={() => this.onItemsPress(item, index)} style={{ width: '100%', height: this.state.height * .07, flexDirection: 'row' }}>
        <View style={{ width: '75%' }}><Text style={{ color: '#1a5177' }}>{item.name}*1</Text></View>
        <View style={{ width: '25%' }}><Text style={{ color: '#1a5177' }}>$ {item.price/100}</Text></View>
      </TouchableOpacity>
    );
  };

  deleteItems123 = () => {
    var array = [...this.state.additems]; // make a separate copy of the array
    // var index = array.indexOf("Chinesburger".target.value)
    console.log("deleted item index value", this.state.deleteIndex)
    console.log("delete prize",)
    array.splice(this.state.deleteIndex, 1);
    this.setState({ additems: array });
    this.setState({ deleteitems: false });

    this.totalrupeecut(this.state.deleteIndexdata.price);


  }
  DetectOrientation() {


    if (this.state.Width_Layout > this.state.Height_Layout) {

      // Write Your own code here, which you want to execute on Landscape Mode.

      this.setState({
        OrientationStatus: 'Landscape Mode'
      });
    }
    else {

      // Write Your own code here, which you want to execute on Portrait Mode.

      this.setState({
        OrientationStatus: 'Portrait Mode'
      });
    }

  }

  getAuthcode = () => {
    this.setState({ Spinnervisible: true })
    util.getValueFromStore("accesTokenSquare")
      .then(token => {
        //  truckID = token;
        console.log('get All Food truckID ', token);

        util.getValueFromStore("AuthToken")
          .then(authtoken => {

            util.getValueFromStore("TruckId")
              .then(token => {
                ApiManager._getReaderLocationID(authtoken, token)
                  .then((response) => response.json())
                  .then(res => {


                    if (res.status == 200) {
                      console.log("_getReaderLocationID",res.data[0].username);
                      ApiManager._CreateSquareMobileAuthorizationCode(res.data[0].username)
                        .then((response) => response.json())
                        .then(res => {
                          console.log('authorization-code', res.authorization_code);
                          readerSdk.authorizeWithCode(res.authorization_code)
                            .then(() => {
                              // success
                              console.log("sucessfully login in renderSDK");
                              console.log("checkoutWithAmount parameter", this.state.total, " ", this.state.UUID);
                              this.setState({ Spinnervisible: false })
                              readerSdk.checkoutWithAmount(
                                this.state.total, // amount to pay (in cents)
                                this.state.UUID, // transaction notes
                              ).then((result) => { // transactionRequestWithCentsAmount success

                                console.log("checkoutWithAmount success", result);
                                util.getValueFromStore("AuthToken")
                                  .then(authtoken => {

                                    ApiManager._Postshoppingcart(authtoken, this.state.UUID, result.transaction.transactionClientID,
                                      result.transaction.totalMoneyAmount, result.transaction.createdAt, result.transaction.totalTipMoneyAmount)
                                      .then(response => {
                                        alert('Done process ' + response)

                                      }).catch((error) => {
                                        alert('Api backend  Error ' + error)

                                      })
                                  }).catch((error) => {
                                    alert('AuthToken  Error ' + error)

                                  });
                                /*
                                 {
                                     "transaction": {
                                       createdAt:"2018-07-25T18:13:02+03:00"
                                       locationID:"18K28ZA1PZF1T"
                                       tenderCnt:1
                                       totalMoneyAmount:115 (in cents, aka 1,15$)
                                       totalMoneyCurrency:"USD"
                                       totalTipMoneyAmount:15 (in cents, aka 0,15$)
                                       totalTipMoneyCurrency:"USD"
                                       transactionClientID:"EE8E7FF7-D16E-4350-91AD-47F2S6C7B447"
                                       transactionID:"Wo5JKw2fOp7dfwai7Gv3FlO14D9eV"
                                     }
                                 }
                                 */

                                // Do something with the result
                              })
                                .catch((e) => { // checkoutWithAmount error
                                  alert("checkoutWithAmount error" + e);
                                  this.setState({ Spinnervisible: false })
                                });


                            }).catch((err) => {
                              // failure     
                              this.setState({ Spinnervisible: false })
                              console.log("failed to login in renderSDK", err);
                            })
                        }).catch((err) => {
                          // failure
                          this.setState({ Spinnervisible: false })
                          console.log("failed to initialize in renderSDK");
                        })
                      }
                    }).catch((error) => {
                      this.setState({ Spinnervisible: false })
                      alert('_getReaderLocationID  Error ' + error)
                    })
                    

              })
              .catch(err => {
                console.log("TruckId token error ", err);
              })
          })
          .catch(err => {
            console.log("AuthToken token error ", err);
          });





      })
      .catch(err => {
        alert("Please link your square account");
      });


  }

  UUIDgenerator = () => {
    UUIDGenerator.getRandomUUID().then((uuid) => {
      console.log("UUID generate", uuid);
      this.setState({ ChackoutClick: true, UUID: uuid });
    }).catch((error) => {
      console.log("UUID generate error", error);
    });
  }
  viewshow = () => {
    console.log("data_height", this.state.height, " ", this.state.Height_Layout);
    console.log("data_Width", this.state.width, " ", this.state.Width_Layout);
    if (this.state.OrientationStatus == 'Landscape Mode') {
      return (
        <View style={{ width: '100%', height: "100%", flexDirection: 'row' }}>
          <View style={{ width: '60%', height: "100%", }}>
            <FlatList
              style={styles.flatListStyle}
              data={this.state.Data == null ? "" : this.state.Data}
              renderItem={({ item }) => this._gridCellView(item)}
              numColumns={3}
            />
          </View>

          <View style={{ width: '40%', height: "100%" }}>
            <View style={{ backgroundColor: 'white', height: '77%', marginBottom: "5%", marginLeft: "8%", marginRight: "8%", marginTop: "8%" }}>
              <View style={{ height: '100%' }}>
                <View style={{ height: '90%', width: '100%' }}>
                  <FlatList
                    style={styles.flatListStyle}
                    data={this.state.additems}
                    renderItem={({ item, index }) => this._dataCellView(item, index)}
                  />
                </View>
                <View style={{ width: '100%', height: '10%', flexDirection: 'row' }}>
                  <View style={{ width: '65%', marginLeft: '10%' }}>
                    <Text style={{ color: '#1a5177', fontSize: 17, fontWeight: "bold" }}>Total</Text>
                  </View>
                  <View style={{ width: '25%' }}>
                    <Text style={{ color: '#1a5177', fontWeight: "bold" }}>${this.state.total/100}</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={{ backgroundColor: '#1a5177', height: '10%', marginLeft: "8%", marginRight: "8%", justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => this.UUIDgenerator()} >
                <Text style={{ color: '#ffffff', fontSize: 17, fontWeight: 'bold' }}>Checkout</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    }
  }
  // Component render method
  render() {
    return (
      <Container style={{ height: this.state.height }} onLayout={(event) => this.setState({
        Width_Layout: event.nativeEvent.layout.width,
        Height_Layout: event.nativeEvent.layout.height
      }, () => this.DetectOrientation())}>
        <Spinner visible={this.state.Spinnervisible} textContent={"Loading..."} color="#1a5177" overlayColor="rgba(256, 256, 256, 0.5)" textStyle={{ color: '#1a5177' }} />
        <Modal

          animationType="slide"
          transparent={true}
          visible={this.state.deleteitems}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' }}>

            <View style={{ height: this.state.height * 0.6, width: this.state.width * 0.5, backgroundColor: 'rgba(255,255,255, 0.95)', borderRadius: 10, borderWidth: 0.3, borderColor: '#7F7F7F', justifyContent: 'center', alignItems: 'center' }} >
              <View style={{ height: '80%', width: '70%', justifyContent: 'center', alignItems: 'center' }}>

                <Text style={{ color: '#1a5177', marginBottom: '15%', textAlign: 'center', fontSize: 20 }}>Are you sure you want to remove this itmes?</Text>

                <TouchableOpacity onPress={this.deleteItems123} style={{ backgroundColor: '#f96467', justifyContent: 'center', alignItems: 'center', width: '90%', height: '20%', borderRadius: 10, justifyContent: 'center', alignContent: 'center', marginBottom: '2%' }}>
                  <Text style={{ color: 'white', alignContent: 'center' }}>YES</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.setState({ deleteitems: false })} style={{ backgroundColor: '#c8c8c8', height: '20%', justifyContent: 'center', alignItems: 'center', width: '90%', borderRadius: 10, justifyContent: 'center', alignContent: 'center' }}>
                  <Text style={{ color: '#1a5177' }}>NO</Text>
                </TouchableOpacity>

              </View>

            </View>
          </View>
        </Modal>

        <Modal

          animationType="slide"
          transparent={true}
          visible={this.state.ChackoutClick}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{ alignItems: 'center', height: '100%', width: '100%', marginTop: '3%' }}>

            <View style={{ height: "85%", width: "35%", backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }} >
              <View style={{ backgroundColor: 'white', height: '97%', marginLeft: "5%", marginRight: "5%", }}>
                <View style={{ height: '100%' }}>
                  <View style={{ height: '70%', width: '100%' }}>
                    <FlatList
                      style={styles.flatListStyle}
                      data={this.state.additems}
                      renderItem={({ item }) => this._dataCellView(item)}
                    />
                  </View>
                  <View style={{ width: '100%', height: '10%', flexDirection: 'row' }}>
                    <View style={{ width: '65%', marginLeft: '10%' }}>
                      <Text style={{ color: '#1a5177', fontSize: 17, fontWeight: "bold" }}>Total</Text>
                    </View>
                    <View style={{ width: '25%' }}>
                      <Text style={{ color: '#1a5177', fontWeight: "bold" }}>$ {this.state.total/100}</Text>
                    </View>
                  </View>
                  <View style={{ width: '100%', height: '20%', }}>
                    <View style={{ height: '100%' }}>
                      <View style={{ backgroundColor: '#1a5177', height: '40%', marginVertical: "1%", justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={this.getAuthcode}>
                          <Text style={{ color: '#ffffff', fontSize: 17, fontWeight: 'bold' }}>Checkout</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ backgroundColor: '#c8c8c8', height: '40%', marginVertical: "1%", justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.setState({ ChackoutClick: false })} >
                          <Text style={{ color: '#1a5177', fontSize: 17, fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>

        {this.viewshow()}


      </Container >
    );
  };
}


