import React, { Component } from "react";
import { TouchableOpacity, FlatList, View, Image, Modal, AsyncStorage } from "react-native";
import {
  Container,
  Text,
} from "native-base";

import ReaderSDK from 'react-native-sp-reader-sdk';
import UUIDGenerator from 'react-native-uuid-generator';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./styles";
import util from "../../../common/Util";
import ApiManager from "../../../common/ApiManager";
import String from '../../../common/Localization'
import theme from "../../../themes/base-theme";
const readerSdk = "";
export default class itemCheckoutScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      additems: [],
      Data: [],
      SpinnerVisible: true,
      total: 0,
      deleteItems: false,
      deleteIndex: null,
      height: null,
      width: null,
      CheckOutClick: false,
      UUID: null,
      showDefault: true,
      deleteIndexData: '',
    }
    readerSdk = new ReaderSDK();

    // initialize the sdk
    readerSdk.initSdk().then(() => {
      // success
      console.log("Successfully initialize in renderSDK");
    }).catch((err) => {
      // failure
      console.log("failed to initialize in renderSDK");
    })
  }

  componentDidMount = () => {
    util.getValueFromStore("AuthToken")
      .then(authToken => {
        util.getValueFromStore("TruckId")
          .then(token => {
            console.log('get All Food truckID ', token);
            ApiManager._getAllFoodMenu(token, authToken)
              .then((response) => response.json())
              .then(res => {
                if (res.status == 200) {
                  this.setState({ SpinnerVisible: false, Data: res.data.menuItems })
                  console.log('get All Food Menu ', res.data.menuItems);
                } else {
                  this.setState({ SpinnerVisible: false })
                  alert("There is something wrong status -" + res.status)
                }
              }).catch((error) => {
                this.setState({ SpinnerVisible: false })
                console.log('Food menu error ' + error)
              });
          })
          .catch(err => {
            console.log("TruckId token error ", err);
          })
      })
      .catch(err => {
        console.log("AuthToken token error ", err);
      });
    this.DetectOrientation();

    AsyncStorage.getItem('height').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })
  }

  _navigateToAddItem = status => {

    //additems.push(status);
    this.setState({
      additems: [...this.state.additems, {
        name: status.name,
        price: status.price,
      }]
    });
    console.log('Shopping card item list ', this.state.additems)

    this.totalRupees(status.price);
  };
  totalRupees = (value) => {
    var total_rupee = (this.state.total + value);
    this.setState({ total: total_rupee })
    console.log("total rupees  " + this.state.total)
  }

  totalRupeesCut = (value) => {
    var total_rupee = (this.state.total - value);
    this.setState({ total: total_rupee })
    console.log("total rupees  " + this.state.total)
  }

  _callApi = () => {
    ApiManager.getAccount()
      .then(response => {
        console.log('Api Call ', response)
      }).catch((error) => {
        console.log('Api Error ', error)
      });

  }
  navigateToAddItem = status => {
    this.setState({
      additems: [...this.state.additems, {
        name: status.name,
        rs: status.rs,
      }]
    });
    console.log('additems ', this.state.additems)
    this.totalRupees(status.rs);
  };
  onItemsPress = (item, index) => {
    console.log("delete item key", index, "", item)
    this.setState({ deleteItems: true, deleteIndex: index, deleteIndexData: item })
  }

  _gridCellView = item => {
    var icon = this.state.showDefault ? require('../../../../src/assets/imgs/image_placeholder.png') : { uri: `data:${item.picture.contentsContentType};base64,${item.picture.contents}` };
    return (
      <TouchableOpacity onPress={() => this._navigateToAddItem(item)}>
        <View style={[{
          width: this.state.width * .15,
          height: this.state.width * .15,
        }, styles.itemCardBg]}>
          <Image style={[{
            height: this.state.width * .10,
          }, styles.itemImage]}
            source={icon}
            onLoadStart={() => this.setState({ showDefault: true })}
            onLoad={() => this.setState({ showDefault: false })}
          />
          <Text style={styles.itemTitle}>{item.name}</Text>
          <Text style={styles.priceText}>$ {item.price / 100}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  _dataCellView = (item, index) => {
    return (
      <TouchableOpacity onPress={() => this.onItemsPress(item, index)} style={[{ height: this.state.height * .07, }, styles.itemCardTextBg]}>
        <View style={styles.itemCardText1Bg}><Text style={styles.itemCardText}>{item.name}*1</Text></View>
        <View style={styles.itemCardText2Bg}><Text style={styles.itemCardText}>$ {item.price / 100}</Text></View>
      </TouchableOpacity>
    );
  };

  _deleteItems123 = () => {
    var array = [...this.state.additems]; // make a separate copy of the array
    // var index = array.indexOf("Chinesburger".target.value)
    console.log("deleted item index value", this.state.deleteIndex)
    console.log("delete prize")
    array.splice(this.state.deleteIndex, 1);
    this.setState({ additems: array });
    this.setState({ deleteItems: false });

    this.totalRupeesCut(this.state.deleteIndexData.price);

  }
  DetectOrientation() {

    if (this.state.Width_Layout > this.state.Height_Layout) {

      // Write Your own code here, which you want to execute on Landscape Mode.
      this.setState({
        OrientationStatus: 'Landscape Mode'
      });
    }
    else {
      // Write Your own code here, which you want to execute on Portrait Mode.
      this.setState({
        OrientationStatus: 'Portrait Mode'
      });
    }

  }

  _getAuthCode = () => {
    this.setState({ SpinnerVisible: true })
    util.getValueFromStore("accessTokenSquare")
      .then(token => {
        //  truckID = token;
        console.log('get All Food truckID ', token);
        util.getValueFromStore("AuthToken")
          .then(authToken => {
            util.getValueFromStore("TruckId")
              .then(token => {
                ApiManager._getReaderLocationID(authToken, token)
                  .then((response) => response.json())
                  .then(res => {
                    if (res.status == 200) {
                      console.log("_getReaderLocationID", res.data[0].username);
                      ApiManager._createSquareMobileAuthorizationCode(res.data[0].username)
                        .then((response) => response.json())
                        .then(res => {
                          console.log('authorization-code', res.authorization_code);
                          readerSdk.authorizeWithCode(res.authorization_code)
                            .then(() => {
                              // success
                              console.log("Successfully login in renderSDK");
                              console.log("checkoutWithAmount parameter", this.state.total, " ", this.state.UUID);
                              this.setState({ SpinnerVisible: false })
                              readerSdk.checkoutWithAmount(
                                this.state.total, // amount to pay (in cents)
                                this.state.UUID, // transaction notes
                              ).then((result) => { // transactionRequestWithCentsAmount success

                                console.log("checkoutWithAmount success", result);
                                util.getValueFromStore("AuthToken")
                                  .then(authToken => {

                                    ApiManager._postShoppingCart(authToken, this.state.UUID, result.transaction.transactionClientID,
                                      result.transaction.totalMoneyAmount, result.transaction.createdAt, result.transaction.totalTipMoneyAmount)
                                      .then(response => {
                                        alert('Done process ' + response)

                                      }).catch((error) => {
                                        alert('Api Error ' + error)
                                      })
                                  }).catch((error) => {
                                    alert('AuthToken  Error ' + error)

                                  });
                                /*
                                 {
                                     "transaction": {
                                       createdAt:"2018-07-25T18:13:02+03:00"
                                       locationID:"18K28ZA1PZF1T"
                                       tenderCnt:1
                                       totalMoneyAmount:115 (in cents, aka 1,15$)
                                       totalMoneyCurrency:"USD"
                                       totalTipMoneyAmount:15 (in cents, aka 0,15$)
                                       totalTipMoneyCurrency:"USD"
                                       transactionClientID:"EE8E7FF7-D16E-4350-91AD-47F2S6C7B447"
                                       transactionID:"Wo5JKw2fOp7dfwai7Gv3FlO14D9eV"
                                     }
                                 }
                                 */
                                // Do something with the result
                              })
                                .catch((error) => { // checkoutWithAmount error
                                  alert("checkoutWithAmount error" + error);
                                  this.setState({ SpinnerVisible: false })
                                });
                            }).catch((error) => {
                              // failure
                              this.setState({ SpinnerVisible: false })
                              console.log("failed to login in renderSDK", error);
                            })
                        }).catch((error) => {

                          this.setState({ SpinnerVisible: false })
                          console.log("_CreateSquareMobileAuthorizationCode", error);
                        })
                    }
                  }).catch((error) => {
                    this.setState({ SpinnerVisible: false })
                    alert('_getReaderLocationID  Error ' + error)
                  })
              })
              .catch(error => {
                console.log("TruckId token error ", error);
              })
          })
          .catch(error => {
            console.log("AuthToken token error ", error);
          });
      })
      .catch(error => {
        alert("Please link your square account", +error);
      });
  }

  _UUIDgenerator = () => {
    UUIDGenerator.getRandomUUID().then((uuid) => {
      console.log("UUID generate", uuid);
      this.setState({ CheckOutClick: true, UUID: uuid });
    }).catch((error) => {
      console.log("UUID generate error", error);
    });
  }
  
  _viewShow = () => {
    if (this.state.OrientationStatus == 'Landscape Mode') {
      return (
        <View style={styles.itemAreaBg}>
          <View style={styles.itemAreaBg2}>
            <FlatList
              style={styles.flatListStyle}
              data={this.state.Data == null ? "" : this.state.Data}
              renderItem={({ item }) => this._gridCellView(item)}
              numColumns={3}
            />
          </View>
          <View style={styles.shippingCardAreaBg}>
            <View style={styles.shippingCardAreaBg2}>
              <View style={styles.shippingCardAreaBg3}>
                <View style={styles.flatListBg}>
                  <FlatList
                    style={styles.flatListStyle}
                    data={this.state.additems}
                    renderItem={({ item, index }) => this._dataCellView(item, index)}
                  />
                </View>
                <View style={styles.totalBg}>
                  <View style={styles.totalNextBg}>
                    <Text style={styles.totalText}>{String.totalText}</Text>
                  </View>
                  <View style={styles.totalResultBg}>
                    <Text style={styles.totalResultText}>$ {this.state.total / 100}</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.firstCheckOutBtnBg}>
              <TouchableOpacity onPress={() => this._UUIDgenerator()} >
                <Text style={styles.checkOutBtnText}>{String.checkoutBtnText}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    }
  }
  // Component render method
  render() {
    return (
      <Container style={{ height: this.state.height }} onLayout={(event) => this.setState({
        Width_Layout: event.nativeEvent.layout.width,
        Height_Layout: event.nativeEvent.layout.height
      }, () => this.DetectOrientation())}>
        <Spinner
          visible={this.state.SpinnerVisible}
          textContent={String.loadingText}
          color={theme.appDefaultColor}
          overlayColor="rgba(256, 256, 256, 0.5)"
          textStyle={styles.spinnerText} />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.deleteItems}
        >
          <View style={styles.modal1View}>
            <View style={[{ height: this.state.height * 0.6, width: this.state.width * 0.5 }, styles.modal1View2]} >
              <View style={styles.modal1View3}>
                <Text style={styles.modalText}>{String.removeItemText}</Text>
                <TouchableOpacity onPress={this._deleteItems123} style={[styles.modalYesBtnBg,styles.yesAndNoBtnBg]}>
                  <Text style={styles.yesBtnText}>{String.yesBtnText}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ deleteItems: false })} style={[styles.modalNoBtnBg,styles.yesAndNoBtnBg]}>
                  <Text style={styles.noBtnText}>{String.noBtnText}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.CheckOutClick}>
          <View style={styles.modal2View1}>
            <View style={styles.modal2View2} >
              <View style={styles.modal2View3}>
                <View style={styles.modal2View4}>
                  <View style={styles.modalShippingCartBg}>
                    <FlatList
                      style={styles.flatListStyle}
                      data={this.state.additems}
                      renderItem={({ item }) => this._dataCellView(item)}
                    />
                  </View>
                  <View style={styles.modalTotalBtnBg}>
                    <View style={styles.modalTotalBtnBg2}>
                      <Text style={styles.totalText}>{String.totalText}</Text>
                    </View>
                    <View style={styles.totalResultBg}>
                      <Text style={styles.totalResultText}>$ {this.state.total / 100}</Text>
                    </View>
                  </View>
                  <View style={styles.modalCheckOutBtnBg}>
                    <View style={styles.modalCheckOutBtnBg2}>
                      <View style={styles.secondCheckOutBtnBg}>
                        <TouchableOpacity onPress={this._getAuthCode}>
                          <Text style={styles.checkOutBtnText}>{String.checkoutBtnText}</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.cancelBtnBg}>
                        <TouchableOpacity onPress={() => this.setState({ CheckOutClick: false })} >
                          <Text style={styles.cancelBtnText}>{String.cancelBtnText}</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        {this._viewShow()}
      </Container >
    );
  };
}
