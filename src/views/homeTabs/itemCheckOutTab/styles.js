import theme from "../../../themes/base-theme";
export default {
    spinnerText: {
        color: theme.appDefaultColor
    },
    //Item view style
    itemAreaBg: {
        width: '100%',
        height: "100%",
        flexDirection: 'row'
    },
    itemAreaBg2: {
        width: '60%',
        height: "100%",
    },

    itemCardText:{
        color:  theme.appDefaultColor
    },
    itemCardTextBg:{
        width: '100%',
        flexDirection: 'row'
    },
    itemCardText1Bg:{
        width: '75%'
    },
    itemCardText2Bg:{
        width: '25%'
    },

    shippingCardAreaBg: {
        width: '40%',
        height: "100%"
    },
    shippingCardAreaBg2: {
        backgroundColor: 'white',
        height: '77%',
        marginBottom: "5%",
        marginLeft: "8%",
        marginRight: "8%",
        marginTop: "8%"
    },
    shippingCardAreaBg3: {
        height: '100%'
    },
    flatListBg: {
        height: '90%',
        width: '100%'
    },
    itemCardBg: {
        backgroundColor: "white",
        justifyContent: "center",
        margin: 15,
        borderWidth: 0.5,
        borderColor: "black"
    },
    itemImage: {
        width: "100%",
    },
    itemTitle: {
        fontWeight: "500",
        marginLeft: 15,
        fontSize: 16,
        color: "gray",
        marginTop: 5
    },

    //Shipping card view style
    priceText: {
        marginLeft: 15,
        color: "gray",
        fontSize: 12,
        marginBottom: 5
    },
    cancelBtnBg: {
        backgroundColor: theme.buttonGrayBg,
        height: '40%',
        marginVertical: "1%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelBtnText: {
        color: theme.appDefaultColor,
        fontSize: 17,
        fontWeight: 'bold'
    },
    secondCheckOutBtnBg: {
        backgroundColor:  theme.appDefaultColor,
        height: '40%',
        marginVertical: "1%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkOutBtnText: {
        color:theme.white,
        fontSize: 17,
        fontWeight: 'bold'
    },
    firstCheckOutBtnBg: {
        backgroundColor:  theme.appDefaultColor,
        height: '10%',
        marginLeft: "8%",
        marginRight: "8%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    flatListStyle: {
        marginLeft: '4%', marginTop: '2.5%', marginBottom: '3%'
    },

    //Modal design
    modal1View: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%'
    },
    modal1View2: {
        backgroundColor: theme.transparentWhite,
        borderRadius: 10,
        borderWidth: 0.3,
        borderColor:theme.buttonGrayBg,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal1View3: {
        height: '80%',
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal2View1: {
        alignItems: 'center',
        height: '100%',
        width: '100%',
        marginTop: '3%'
    },
    modal2View2: {
        height: "85%",
        width: "35%",
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal2View3: {
        backgroundColor: 'white',
        height: '97%',
        marginLeft: "5%",
        marginRight: "5%",
    },
    modal2View4: {
        height: '100%'
    },
    modalText: {
        color:  theme.appDefaultColor,
        marginBottom: '15%',
        textAlign: 'center',
        fontSize: 20
    },

    modalShippingCartBg: {
        height: '70%',
        width: '100%'
    },
    modalTotalBtnBg: {
        width: '100%',
        height: '10%',
        flexDirection: 'row'
    },
    modalTotalBtnBg2: {
        width: '65%',
        marginLeft: '10%'
    },
    modalCheckOutBtnBg: {
        width: '100%',
        height: '20%'
    },
    modalCheckOutBtnBg2: {
        height: '100%'
    },
    shippingCartBg: {
        height: '70%',
        width: '100%'
    },
    yesAndNoBtnBg:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: '20%',
        borderRadius: 10,
        alignContent: 'center',
    },
    modalYesBtnBg: {
        backgroundColor:theme.buttonPinkBg,
        marginBottom: '2%'
    },
    yesBtnText: {
        color: theme.white,
        marginBottom:'2%'
    },
    modalNoBtnBg: {
        backgroundColor:theme.buttonGrayBg,
    },
    noBtnText: {
        color:  theme.appDefaultColor
    },
    totalText: {
        color:  theme.appDefaultColor,
        fontSize: 17,
        fontWeight: "bold"
    },
    totalResultText: {
        color:  theme.appDefaultColor,
        fontWeight: "bold"
    },
    totalResultBg: {
        width: '25%'
    },
    totalBg: {
        width: '100%',
        height: '10%',
        flexDirection: 'row'
    },
    totalNextBg: {
        width: '65%',
        marginLeft: '10%'
    }
};
