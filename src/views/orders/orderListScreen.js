import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Card,
  CardItem
} from "native-base";

import { View, TextInput, Image, FlatList, TouchableOpacity, Alert } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import util from "../../common/Util";
const data = [
  "Andy",
  "French Fries",
  "Cheesburger",
  "Chinesburger"
  
];class OrderListScreen extends Component {
  static navigationOptions = {
    title: 'CURRENT ORDERS'
  };
  
   render(){
    const { props: { name, index, list, navigation } } = this;
    return(<Container style={global.container}>
        <Header style={{backgroundColor:'white'}}>
        <Left>
        <Button
        transparent
        onPress={() => this.props.navigation.openDrawer()} >
        <Icon active name="menu" style={{ color:"#2A409A"}} />
        </Button>
        </Left>
        <Body >
        <Title style={global.headerTitleText}>ORDERS</Title>
        </Body>
        </Header>
    
        <FlatList
            style={styles.flatListStyle}
            data={data}
            renderItem={ ({item}) => {return(
              <TouchableOpacity onPress={()=>  navigation.navigate("OrderDetailScreen")}>
              <Card style={styles.cardViewBg} >
                <CardItem >
                <View style={styles.leftViewBg}>
                
                <Text style={global.defaultBlueColorText}>{item.toUpperCase()}</Text>
                <Text style={[{fontSize:14, marginTop:2}, global.defaultBlueColor]}>$ 26.00</Text>
                <Text style={{color:'gray', fontSize:12, marginBottom:5}}>#3</Text>
              { data.map((item, key)=>(
                <Text key={key} style={{color:'gray', fontSize:14}} 
                >{key+1}x { item } </Text>)
                )}

                
                </View>
                <View style={{ 
                 height:util.getHeight(26), width:'51%', top:0, right:0,
                 alignItems:'flex-end'}}>
                
              <Text style={styles.newBtn} >NEW</Text>
               <View style={{ height:util.getHeight(22), justifyContent:'flex-end'}}>
                <Text style={{textAlign:'right',color:'gray', fontSize:11, bottom:0, right:0}}>
                  CREATED {'\n'} 07-25-2018 3.26 p.m.
                </Text>
                </View> 
                </View>
                </CardItem>
              </Card>
              </TouchableOpacity>
            )}}
            />

        
   </Container>)
   }

}
export default OrderListScreen;
