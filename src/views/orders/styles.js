const React = require("react-native");

import theme from "src/themes/base-theme";
import util from "../../common/Util";

export default {
  //Current Order List Screen
  newBtn:{
    backgroundColor:'#1FFFE1', 
    padding:2, borderRadius:5, width:80,height:20, textAlign:'center',
     fontSize:10
  },
  flatListStyle:{
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: "white",
    marginTop: 10
  },
  cardViewBg:{
    height:util.getHeight(30)
  },
  leftViewBg:{
    backgroundColor:'white',
    height:util.getHeight(26), width:'51%',
    justifyContent:'flex-start', alignItems:'flex-start' 
  },
  //OrderDetail Screen Style
  newBtnTextBg:{
    backgroundColor: "#1FFFE1",
    alignSelf: "center",
    marginTop: 30,
    padding: 2,
    borderRadius: 5,
    width: 90,
    height: 20,
    textAlign: "center",
    fontSize: 10
  },
  contentBg: {
    marginLeft: 30,
    marginRight: 30
  },
  btnStyle: {
    backgroundColor: theme.buttonGrayBg,
    width: util.getWidth(75),
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 2
  },
  btnBg: {
    flex: 1,
    marginTop: util.getHeight(8),
    height: util.getHeight(11),
    justifyContent: "center"
  },
  priceText: {
    fontSize: 18,
    marginTop: 2,
    textAlign: "right",
    width: "50%"
  },
  quantityText: {
    color: theme.buttonGrayBg,
    fontSize: 14,
    marginBottom: 5
  },
  itemText: {
    color: theme.buttonGrayBg,
    paddingTop: 5,
    fontSize: 16
  },
  createdText: {
    color: theme.buttonGrayBg,
    fontSize: 15,
    marginTop:util.getHeight(8)
  },
  btnTextStyle: {
    textAlign: "center",
    fontWeight: "500",
    color: "#FF785F"
  },
  topView: {
    flexDirection: "row",
    marginTop: util.getHeight(10)
  },
// Modal view dialog
    shadowBg:{
        backgroundColor:'white',
        borderRadius:10,
        borderWidth:0.5,
        shadowColor: 'rgba(0, 174 , 239, 0.1)',
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 1,
        shadowOpacity: 1,
        elevation: 10,
    },
    modalParentView:{
        height:util.getHeight(100),alignItems:'center', justifyContent:'center'
    },
    modalCenterView:{
        borderWidth:0, width:util.getWidth(90), 
        height:util.getHeight(50), alignItems:'center'
    },
    titleText:{
        marginTop:util.getHeight(12),
                color:theme.buttonGrayBg, fontSize:17,
                paddingLeft:40, paddingRight:40, textAlign:'center'
    }

};
