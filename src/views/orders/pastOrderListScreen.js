import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Card,
  CardItem
} from "native-base";

import { View, TextInput, Image, FlatList } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import util from "../../common/Util";
const data = [
  "Andy",
  "French Fries",
  "Cheesburger",
  "Chinesburger"
];

class PastOrderListScreen extends Component {
  static navigationOptions = {
    title: 'PAST ORDERS'
  };
  
   render(){
    const { props: { name, index, list,navigation } } = this;
    return(<Container style={global.container}>
        <Header style={{backgroundColor:'white'}}>
        <Left>
        <Button
        transparent
        onPress={() => navigation.navigate("DrawerOpen")} >
        <Icon active name="menu" style={{ color:"#2A409A"}} />
        </Button>
        </Left>
        <Body >
        <Title style={global.headerTitleText}>   PAST ORDERS</Title>
        </Body>
        </Header>
    
        <FlatList
            style={{
              marginRight: 10,
              marginLeft: 10,
              backgroundColor: "white",
              marginTop: 10
            }}
            data={data}
            renderItem={ ({item}) => {return(
              <Card style={{height:util.getHeight(30)}} >
                <CardItem >
                <View style={{backgroundColor:'white',
                 height:util.getHeight(26), width:'51%',
                 justifyContent:'flex-start', alignItems:'flex-start' }}>
                
                <Text style={global.defaultBlueColorText}>{item.toUpperCase()}</Text>
                <Text style={[{fontSize:14, marginTop:2}, global.defaultBlueColor]}>$ 26.00</Text>
                <Text style={{color:'gray', fontSize:12, marginBottom:5}}>#3</Text>
              { data.map((item, key)=>(
                <Text key={key} style={{color:'gray', fontSize:14}} 
                >{key+1}x { item } </Text>)
                )}

                
             </View>
                <View style={{ 
                 height:util.getHeight(26), width:'51%', top:0, right:0,
                 alignItems:'flex-end'}}>
                
              <Text 
               style={{ backgroundColor:'white', borderWidth:1,
                  borderColor:'#2A409A', padding:2, borderRadius:6, 
                  width:80,height:20, textAlign:'center',color:'#2A409A',
                  fontSize:10}} >PICKED UP</Text>
               <View style={{ height:util.getHeight(22), justifyContent:'flex-end'}}>
                <Text style={{textAlign:'right',color:'gray', fontSize:11, bottom:0, right:0}}>
                  DELIVERED {'\n'} 07-25-2018 3.26 p.m.
                </Text>
                </View> 
                </View>
                </CardItem>
              </Card>
             
            )}}
            />

        
   </Container>)
   }

}
export default PastOrderListScreen;
