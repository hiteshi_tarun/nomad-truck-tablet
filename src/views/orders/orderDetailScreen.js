import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Card,
  CardItem
} from "native-base";

import { View, TextInput, Image, FlatList, Alert, Modal } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import util from "../../common/Util";
import theme from "src/themes/base-theme";
import keyStrings from '../../common/Localization';
const data = ["Andy", "French Fries", "Cheesburger", "Chinesburger"];

class OrderDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isReadyModalVisible: false,
            isPrepareModalShow: false,
            preparingConfirmStatus: false
        };
      }

      _confirmButton = () =>{
        if (this.state.isPrepareModalShow) {
            Alert.alert('Preparing your order now.');
            this.setState({
                preparingConfirmStatus: true,
                modalVisible: false
              })
        } else {
            Alert.alert('Declined your order.');
            this.setState({
                preparingConfirmStatus: false,
                modalVisible: false
              })
        }
      }
      _readyModalYesButton = (navigation) =>{
        Alert.alert('Order is ready to picked up now.!!');
            this.setState({
                 isReadyModalVisible: false,
              })
              navigation.navigate("TabsRouter");
      }
 
      _showModalView = (status)=> {
      if (status == 'PREPARING') {
          this.setState({
              isPrepareModalShow: true,
              modalVisible: true
            })
      }  else  if (status == 'READY') {
        this.setState({
            isReadyModalVisible: true
          })
     } else if (status == 'DECLINE'){
          this.setState({
              isPrepareModalShow: false,
              modalVisible: true
            }) 
      }
    }

    _returnButtonView = () =>{
        if (this.state.preparingConfirmStatus) {
            return(<View>
                <View style={styles.btnBg}>
                    <Button 
                    onPress={()=> this._showModalView('READY')}
                    style={[styles.btnStyle, { backgroundColor:  theme.appDefaultColor  }]}>
                    <Text style={[styles.btnTextStyle, { color: "white" }]}> READY </Text>
                    </Button>
                    </View>

            </View>);
        } else {
            return(<View>
                    <View style={styles.btnBg}>
                    <Button 
                    onPress={()=> this._showModalView('PREPARING')}
                    style={[styles.btnStyle, { backgroundColor:  theme.yellowBtnBg  }]}>
                    <Text style={styles.btnTextStyle}> BEGIN PREPARING </Text>
                    </Button>
                    </View>
                    <View style={[styles.btnBg, { marginTop: 1 }]}>
                    <Button 
                    onPress={()=> this._showModalView('DECLINE')}
                    style={[styles.btnStyle, { backgroundColor: theme.redBtnBg }]}>
                    <Text style={[styles.btnTextStyle, { color: "white" }]}>
                    {" "}
                    DECLINE{" "}
                    </Text>
                    </Button>
                    </View>
               </View>);
        }
    }

  render() {
    const {
      props: { name, index, list, navigation }
    } = this;
    return (
      <Container style={global.container}>
        <Header style={{ backgroundColor: "white" }}>
          <Left>
            <Button
              transparent
              onPress={() => navigation.navigate("TabsRouter")}
            >
              <Icon active name="arrow-back" style={global.defaultBlueColor} />
            </Button>
          </Left>
          <Body>
            <Title style={global.headerTitleText}> ORDERS DETAILS</Title>
          </Body>
        </Header>

        <Content >
        <View style={styles.contentBg}>
          <Text  style={[styles.newBtnTextBg, 
          {backgroundColor: this.state.preparingConfirmStatus ? theme.yellowBtnBg : theme.lightGreenBg,
           color: this.state.preparingConfirmStatus ? theme.redBtnBg :theme.black}]}>
           { this.state.preparingConfirmStatus ? 'PREPARING' : 'NEW'}</Text>
          <View style={styles.topView}>
            <Text style={[global.defaultBlueColorText, { width: "50%" }]}>
              {"andy".toUpperCase()}
            </Text>
            <Text style={[styles.priceText, global.defaultBlueColor]}>
              $ 26.00
            </Text>
          </View>
          <View >
            <Text style={styles.quantityText}>#4</Text>
            {data.map((item, key) => (
              <Text key={key} style={styles.itemText}>
                {key + 1}x {item}{" "}
              </Text>
            ))}
          </View>
          <Text style={styles.createdText}>
            CREATED {"\n"} July 25th 2018 6.26 p.m.
          </Text>
            {this._returnButtonView()}
          </View>
       
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}>

          <View style={styles.modalParentView} >
          <View style= {[styles.shadowBg, styles.modalCenterView]} >

            <Text style={styles.titleText} >
              { this.state.isPrepareModalShow ? keyStrings.preparingText :
             keyStrings.declineText}</Text>
           
             <View style={[styles.btnBg]}>
            <Button 
               onPress={()=> this._confirmButton()}
               style={[styles.btnStyle, {backgroundColor: this.state.isPrepareModalShow ? theme.yellowBtnBg : theme.redBtnBg  }]}>
              <Text style={[styles.btnTextStyle, {color:this.state.isPrepareModalShow ? theme.redBtnBg : theme.white}]}> YES </Text>
            </Button>
          </View>
            <View style={[styles.btnBg, { marginTop:-15 }]}>
              <Button 
                     onPress={()=> this.setState({modalVisible: !this.state.modalVisible})}
                    style={[styles.btnStyle, { backgroundColor: theme.buttonGrayBg }]}>
              <Text style={[styles.btnTextStyle, { color: "white", fontSize:10 }]}>
                {" "}
                No{" "}
              </Text>
            </Button>
          </View>
          </View>
          </View>
          </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isReadyModalVisible}>

          <View style={styles.modalParentView} >
          <View style= {[styles.shadowBg, styles.modalCenterView]} >

            <Text style={styles.titleText} >
              { keyStrings.readyText}</Text>
           
             <View style={[styles.btnBg]}>
            <Button 
               onPress={()=> this._readyModalYesButton(navigation)}
               style={[styles.btnStyle, {backgroundColor:  theme.appDefaultColor  }]}>
              <Text style={[styles.btnTextStyle, {color: theme.white}]}> YES </Text>
            </Button>
          </View>
            <View style={[styles.btnBg, { marginTop:-15 }]}>
              <Button 
                     onPress={()=> this.setState({isReadyModalVisible: !this.state.isReadyModalVisible})}
                    style={[styles.btnStyle, { backgroundColor: theme.buttonGrayBg }]}>
              <Text style={[styles.btnTextStyle, { color: "white", fontSize:10 }]}>
                {" "}
                No{" "}
              </Text>
            </Button>
          </View>
          </View>
          </View>
          </Modal>

          </Content>
      </Container>
    );
  }
}
export default OrderDetailScreen;
