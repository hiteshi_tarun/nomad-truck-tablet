const React = require("react-native");
const { StyleSheet } = React;
import util from "../../../common/Util";
import theme from "src/themes/base-theme";
export default {
  container: {
    height: util.getHeight(90) ,
  },
  textInput: {
    borderColor: "gray",
    borderWidth: 0.3,
    marginTop: 5,
    borderRadius:5,
    padding: 12,
    backgroundColor:'white',
    width:'70%',
    paddingVertical:0,
    height: util.getHeight(5),
    color: theme.appDefaultColor,
    fontSize: 14
  },
  row: {
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  mt: {
    marginTop: 18
  },
  itemTitel: {
    fontWeight: "500",
    fontSize: 16,

    color: "#1a5177",

  },
  priceText: {
    marginLeft: 15,
    color: "gray",
    fontSize: 12
  },
  gridBg: {
    backgroundColor: "white",
    justifyContent: "center",
    width: util.getWidth(25),
    height: util.getWidth(25),
    margin: 15,
    borderWidth: 0.5,
    borderColor: "black"
  },
  gridIcon: {
    width: util.getWidth(23),
    height: util.getWidth(15),
    marginLeft: 5,
    marginRight: 5
  },
  addItemTopView: {
    alignItems: "center",
    justifyContent: "center"
  },
  plusBtnBg: {
    borderRadius: 40,
    backgroundColor: "#2A409A",
    width: 60,
    height: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  plusTextStyle: {
    textAlign: "center",
    color: "white",
    paddingBottom: 5,
    fontSize: 30,
    color: "white"
  },
  flatListStyle: {
  
   marginTop:'2.5%' ,marginBottom:'3%' 
  }
};
