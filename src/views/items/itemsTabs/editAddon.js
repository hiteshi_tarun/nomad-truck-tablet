import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Footer,
    Icon,
    Left,
    Right,
    Body,
    CheckBox
} from "native-base";

import { View, TextInput, Image, Alert, TouchableOpacity, ImageBackground, ScrollView, Modal, AsyncStorage } from "react-native";
import global from '@assets/css/global';
import { resetToMain } from 'src/navReset';
import styles from "./styles";
import util from "../../../common/Util"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import IconIonic from "react-native-vector-icons/Ionicons.js";
import Icon1 from 'react-native-vector-icons/Entypo';
comeFromStatus = ''
import images from "@assets/images";

class AddItemScreen extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
            deleteitems: false
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value) => {
            this.setState({ height: JSON.parse(value) })
        })
    }

    hidedeletealert = () => {
        this.setState({ deleteitems: false })
    }
    onItemsPress = () => {
        this.setState({ deleteitems: true })
    }
    render() {
        return (
            <Container style={{ height: this.state.height }}>
                <Content>
                <ImageBackground source={images.bg_l} style={{ width: "100%", height: this.state.height }}>
                    <Modal

                        animationType="slide"
                        transparent={true}
                        visible={this.state.deleteitems}
                        onRequestClose={() => {
                            alert('Modal has been closed.');
                        }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' }}>

                            <View style={{ height: util.getHeight(40), width: util.getWidth(40), backgroundColor: 'rgba(255,255,255, 0.95)', borderRadius: 10, borderWidth: 0.3, borderColor: '#7F7F7F', justifyContent: 'center', alignItems: 'center' }} >
                                <View style={{ height: '80%', width: '70%', justifyContent: 'center', alignItems: 'center' }}>

                                    <Text style={{ color: '#1a5177', marginBottom: '20%', textAlign: 'center', fontSize: 18, width: "80%" }}>Are you sure you want to delete this itme?</Text>

                                    <TouchableOpacity onPress={this.deleteItems} style={{ backgroundColor: '#f96467', justifyContent: 'center', alignItems: 'center', width: '90%', height: '20%', borderRadius: 10, justifyContent: 'center', alignContent: 'center', marginBottom: '2%' }}>
                                        <Text style={{ color: 'white', alignContent: 'center' }}>YES</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={this.hidedeletealert} style={{ backgroundColor: '#c8c8c8', height: '20%', justifyContent: 'center', alignItems: 'center', width: '90%', borderRadius: 10, justifyContent: 'center', alignContent: 'center' }}>
                                        <Text style={{ color: '#1a5177' }}>NO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <View style={{ flexDirection: 'row', width: "100%", height: "100%" }}>

                        <View style={{ width: "6%", alignItems: 'center', borderColor: "gray", borderWidth: 0.3, height: "100%", backgroundColor: 'white' }}>
                            <TouchableOpacity style={{ marginTop: 3 }}

                                onPress={() => this.props.navigation.openDrawer()} >


                                <Icon1 active name="menu" size={35} color="#1a5177" />
                            </TouchableOpacity>
                            <View style={{ height: '15%', width: "70%", marginBottom: '8%', position: "absolute", bottom: 0, }}>
                                <Image style={{ height: '100%', width: '80%' }} source={images.makeImg} />
                            </View>

                        </View>
                        <View style={{ width: "94%", height: "100%", }}>

                            <View style={{ height: "10%", width: '100%', flexDirection: 'row' }}>
                                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate("ItemsScreen") }} style={{ marginTop: 3 }} >
                                        <IconIonic active name="md-arrow-back" size={25} color="#1a5177" />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Addons</Text>
                                </View>
                                {/* <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Save</Text>
                                </View> */}
                            </View>

                            <View
                                style={{ height: "70%", marginVertical: "3%", flexDirection: 'row', width: "94%", marginHorizontal: "3%", }}>
                                <Content contentContainerStyle={{}} >
                                    <View
                                        style={{ width: '95%', marginLeft: '5%' }}>

                                        <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Addons Set</Text>
                                        <TextInput
                                            style={styles.textInput}
                                            underlineColorAndroid='transparent'
                                            placeholderTextColor='#2A409A'
                                            returnKeyType={'next'}
                                        />


                                        <View style={{ flexDirection: 'row', marginTop: '5%', width: "100%", }}>
                                            <View style={{ marginRight: '1%', width: "43%" }}>
                                                <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Addons Name</Text>
                                                <TextInput
                                                    style={[styles.textInput, {
                                                        width: "100%",
                                                        marginTop: '2%'
                                                    }]}
                                                    underlineColorAndroid='transparent'
                                                    placeholder=''
                                                    returnKeyType={'next'}
                                                />
                                            </View>
                                            <View style={{ marginLeft: '1%', width: "25%" }}>
                                                <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Price</Text>
                                                <TextInput
                                                    style={[styles.textInput, {
                                                        width: "100%",
                                                        marginTop: '2%'
                                                    }]}
                                                    underlineColorAndroid='transparent'
                                                    placeholder='$0.00'
                                                    returnKeyType={'next'}
                                                />
                                            </View>
                                            <View style={{ marginLeft: '1%', width: "20%", paddingTop: '4%', }}>
                                                <TouchableOpacity
                                                    style={{
                                                        borderColor: "gray",
                                                        borderWidth: 0.3,
                                                        marginTop: 5,
                                                        borderRadius: 5,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: '#f96467',
                                                        width: '100%',
                                                        paddingVertical: 0,
                                                        height: util.getHeight(5),
                                                    }}>
                                                    <Text style={{ color: 'white' }}>Delete</Text>

                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    style={{
                                                        borderColor: "gray",
                                                        borderWidth: 0.3,
                                                        marginTop: 5,
                                                        borderRadius: 5,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: '#1a5177',
                                                        width: '100%',
                                                        paddingVertical: 0,
                                                        height: util.getHeight(5),


                                                    }}>
                                                    <Text style={{ color: 'white' }}>Add</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Content>

                                <View
                                    style={{ width: '40%', alignItems: 'center', }}>
                                    <View
                                        style={{ width: '60%', height: '80%', alignItems: 'center', backgroundColor: 'white' }}>
                                    </View>
                                </View>

                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', height: util.getHeight(10), width: '100%', position: 'absolute', bottom: 0 }}>
                                <TouchableOpacity onPress={this.onItemsPress} style={{ borderColor: "gray", borderWidth: 0.3, borderRadius: 5, justifyContent: 'center', alignItems: 'center', width: '25%', height: '60%', backgroundColor: '#f96467' }}>
                                    <Text style={{ color: 'white' }}>Delete</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>


                    {/* <ImageBackground source={images.bg_l} style={{ width: util.getWidth(100), height: util.getHeight(100) }}> */}

                    {/* </Content> */}

                </ImageBackground>
                </Content>
            </Container>
        );
    }
}

export default AddItemScreen;


