import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    List,
    ListItem,
    Left,
    Right,
    Body,
    CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage } from "react-native";
import global from "@assets/css/global";
import images from "@assets/images";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import IconTruck from "react-native-vector-icons/MaterialCommunityIcons.js";
import util from '../../../common/Util'




const blueColor = '#1a5177';

export default class AddonsScreens extends Component {
    constructor() {
        super();
        this.state = {
            height: null,
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('height').then((value)=>{
          this.setState({height: JSON.parse(value)})
        } )
      }


    render() {
      
        return (
            <Container style={{height:this.state.height}}>
                <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity  style={{ marginTop: 3 }} >
                            <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Addons</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    </View>
                </View>
                <Content style={{ width: '90%',marginHorizontal:'5%',marginVertical:'5%'}}>
                    <View style={{ width: '90%', flexDirection: 'row',justifyContent:'center',height:util.getHeight(8), }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', flexDirection: 'row', height: '85%', backgroundColor: 'white', marginLeft: '2%', borderRadius: 5, borderWidth: 0.5, borderColor: '#7f7f7f' }}>
                            <Text style={{ color: blueColor, flex: 1, fontSize: 13, marginLeft: 10 }}>Addon Set Name</Text>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate("EditAddonsScreen")}}style={{ width: '10%', margin: '1%',  justifyContent: 'center', alignItems: 'center' }}>
                                <IconIonic
                                    name="ios-arrow-forward"
                                    size={20}
                                    style={{ color: "#1a5177" }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>

                <TouchableOpacity  onPress={() => {this.props.navigation.navigate("AddAddonsScreen")}} style={{ borderWidth: 0.5, borderColor: '#7f7f7f', borderRadius: 2, justifyContent: 'center', alignItems: 'center', height: '10%', width: '100%', position: 'absolute', bottom: 0, backgroundColor: 'white' }}>
                    <Text style={{ color: blueColor, fontSize: 30, }}>+</Text>
                </TouchableOpacity>


            </Container>)
    }

}

