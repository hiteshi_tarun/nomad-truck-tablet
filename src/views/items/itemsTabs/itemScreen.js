import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Button,
    Icon,
    List,
    ListItem,
    Left,
    Right,
    Body,
    CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity,FlatList, ImageBackground, AsyncStorage } from "react-native";
import Spinner from 'react-native-loading-spinner-overlay';
import global from "@assets/css/global";
import images from "@assets/images";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import IconTruck from "react-native-vector-icons/MaterialCommunityIcons.js";
import util from '../../../common/Util';
import ApiManager from "../../../common/ApiManager";


import styles from "./styles";


const blueColor = '#1a5177';

export default class ItemsScreens extends Component {
    constructor() {
        super();
        this.state = {
          Data:[],
          Spinnervisible: true,
          showDefault:true
        }
    }

    componentDidMount = () => {
      // var truckID = "";  
      util.getValueFromStore("AuthToken")
      .then(authtoken => {

       util.getValueFromStore("TruckId")
       .then(token => {
       //  truckID = token;
         console.log('get Food truckID ', token);
            ApiManager._getFoodMenuItems(token,authtoken)
           .then((response) => response.json())
           .then(res => {

            if (res.status == 200) {
                this.setState({Spinnervisible:false})
                this.setState({Data:res.data});
                 console.log('_getFoodMenuItems ', res.data);
            }else{
                this.setState({Spinnervisible:false})
                 console.log('_getFoodMenuItems error status ', res.status);
            }
          
           }).catch((error) => {
              this.setState({Spinnervisible:false})
               console.log('_getFoodMenuItems error ', error)
           });
     })
     .catch(err => {
       console.log("TruckId token error ", err);
     })
    })
    .catch(err => {
      console.log("AuthId token error ", err);
    });
    }
    
    _gridCellView = item => {
        var icon = this.state.showDefault ? require('../../../../src/assets/imgs/image_placeholder.png') : {uri: `data:${item.picture.contentsContentType};base64,${item.picture.contents}`};
        return (
          <TouchableOpacity  onPress={() => {this.props.navigation.navigate("EditItemScreen",{data:item})}}>
            <View style={{
              backgroundColor: "white",

              alignItems:'center',
              width: util.getWidth(13),
              height:util.getWidth(12),
              margin: 25,
             
              borderWidth: 0.5,
              borderColor: "black"
            }}>
              <Image style={{
                width:util.getWidth(13),
                height:util.getWidth(8.5),
      
              }}    source={icon}
              onLoadStart={() => this.setState({showDefault: true})}  
              onLoad={() => this.setState({showDefault: false})}/>
              <View style={{ justifyContent: "center",
              alignItems:'center',flex:1}}>
              <Text style={styles.itemTitel}>{item.name}</Text>
              {/* <Text style={styles.priceText}>$ {item.rs}</Text> */}
              </View>
            </View>
          </TouchableOpacity>
        );
      };

    render() {

        return (
            <Container>
                   <Spinner visible={this.state.Spinnervisible} textContent={"Loading..."} color="#1a5177" overlayColor="rgba(256, 256, 256, 0.5)" textStyle={{ color: '#1a5177' }} />
                 <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity  onPress={() => {this.props.navigation.goBack()}} style={{ marginTop: 3 }} >
                            <IconIonic active name="md-arrow-back" size={35} color="#1a5177" />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', justifyContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Items</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    </View>
                </View>

                <View style={{ width: '100%', height: "80%",justifyContent:'center',alignItems:'center' }}>
                    <FlatList
                        style={styles.flatListStyle}
                  
                        data={this.state.Data == null ? "" : this.state.Data}
                        renderItem={({ item }) => this._gridCellView(item)}
                        numColumns={3}
                    />
                </View>
                <TouchableOpacity  onPress={() => {this.props.navigation.navigate("AddItemScreen")}} style={{ borderWidth: 0.5, borderColor: '#7f7f7f', borderRadius: 2, justifyContent: 'center', alignItems: 'center', height: '10%', width: '100%', position: 'absolute', bottom: 0, backgroundColor: 'white' }}>
                    <Text style={{ color: blueColor, fontSize: 30, }}>+</Text>
                </TouchableOpacity>


            </Container>)
    }

}

