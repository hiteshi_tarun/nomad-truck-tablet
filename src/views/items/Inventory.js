import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  CheckBox,
} from "native-base";

import { View, TextInput, Image, Alert, Switch, TouchableOpacity, ImageBackground, AsyncStorage } from "react-native";
import global from "@assets/css/global";
import { resetToMain } from "src/navReset";
import styles from "./styles";
import images from "@assets/images";
import util from "../../common/Util";
import Icons from "react-native-vector-icons/MaterialIcons.js";
import IconIonic from "react-native-vector-icons/Ionicons.js";
import IconTruck from "react-native-vector-icons/MaterialCommunityIcons.js";
import Icon1 from 'react-native-vector-icons/Entypo';

// tabs pages
import ItemsScreen from './itemsTabs/itemScreen';
import AddonsScreen from './itemsTabs/addonScreen';

const blueColor = '#1a5177';

export default class InventoryScreen extends Component {
  constructor() {
    super();
    this.state = {
      SwitchOnValueHolder: false,
      height: null,
      width: null,
      index: 0
    }
  }
  componentDidMount() {
    AsyncStorage.getItem('height').then((value) => {
      this.setState({ height: JSON.parse(value) })
    })
    AsyncStorage.getItem('width').then((value) => {
      this.setState({ width: JSON.parse(value) })
    })
  }

  render() {

    let AppComponent = null;


    switch (this.state.index) {
      case 0:
        AppComponent = ItemsScreen

        break;
      case 1:
        AppComponent = AddonsScreen
        break;


    }
    const { props: { name, index, list, navigation } } = this;
    return (<Container style={global.container} >


      <ImageBackground source={images.bg_p} style={{ width: '100%', height: this.state.height }}>

        <View style={{ width: '100%', height: '100%', flexDirection: 'row' }}>
          <View style={{ width: '30%', backgroundColor: 'white', height: '100%', borderWidth: 0.5, borderColor: '#7F7F7F' }}>
            <View style={{ width: '100%', height: '6%', flexDirection: 'row', marginTop: '2%', marginBottom: '2%' }}>
              <View style={{ width: '20%', alignItems: 'center' }}>
                <TouchableOpacity style={{ marginTop: 3 }}

                  onPress={() => this.props.navigation.openDrawer()} >


                  <Icon1 active name="menu" size={35} color="#1a5177" />
                </TouchableOpacity>
              </View>
              <View style={{ width: '60%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#1a5177', fontWeight: 'bold' }}>Inventory</Text>
              </View>
              <View style={{ width: '20%' }}></View>
            </View>
            <View>

              {/* // first */}
              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '10%', marginBottom: '2%' }}></View>
              <TouchableOpacity onPress={() => this.setState({ index: 0 })} style={{ width: '100%', height: '20%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="md-grid"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Items
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>
              {/* //second  */}
              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%', marginBottom: '3%' }}></View>
              {/* <TouchableOpacity onPress={() => this.setState({ index: 1 })} style={{ width: '100%', height: '13%', flexDirection: 'row' }}>

                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <Icons
                    name="add-box"
                    size={25}
                    style={{ color: "#1a5177" }}
                  />
                </View>
                <View style={{ width: '70%', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: "#1a5177",
                      fontSize: 14,

                      marginLeft: '2%'
                    }}
                  >
                    Addons
                      </Text>
                </View>
                <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                  <IconIonic
                    name="ios-arrow-forward"
                    size={20}
                    style={{ color: "#1a5177" }}
                  />
                </View>
              </TouchableOpacity>


              <View style={{ width: '100%', height: 1, backgroundColor: '#7F7F7F', marginTop: '4%' }}></View> */}


            </View>
            <View style={{ width: '100%', height: '30%', position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={images.makeImg_p}
                style={{ width: '50%', height: '20%' }} />
            </View>
          </View>

          <View style={{ width: '70%' }}>

            <AppComponent navigation={this.props.navigation} />

          </View>


        </View>
      </ImageBackground>


    </Container>)
  }

}

