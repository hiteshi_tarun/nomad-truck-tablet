import React from "react";
import { AppRegistry, Image, TouchableOpacity, View, Alert, AsyncStorage } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon,
  Footer
} from "native-base";
import Icons from "react-native-vector-icons/FontAwesome.js";
import Icon1 from 'react-native-vector-icons/Entypo';
import util from "../../common/Util";
import { resetToLogin } from "src/navReset";
import ApiManager from "../../common/ApiManager";
import images from "../../assets/images";
const routers = [
  { key: "Home", text: "Checkout", icon: "bars" },
  { key: "Home", text: "Orders", icon: "envelope" },
  { key: "ItemsScreen", text: "Items", icon: "bars" },
  { key: "SettingScreen", text: "Settings", icon: "cog" },
];
const routers1 = [
  { key: "Help", text: "Help", icon: "cog" },
  { key: "Logout", text: "LOGOUT", icon: "sign-out" }
];

export default class DrawBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      truckName: '',
      ImageUrl: '',
      ImageType: '',

    }

  }
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    util.getValueFromStore("AuthToken")
      .then(authtoken => {

        util.getValueFromStore("TruckId")
          .then(token => {
            ApiManager._getTruckDescription(token, authtoken)
              .then((response) => response.json())
              .then(res => {
                this.setState({ Spinnervisible: true })
                if (res.status == 200) {
                  console.log('Get truck info ', res.data);
                  this.setState({
                    truckName: res.data[0].truckName,
                    ImageUrl: res.data[0].profilePicture.contents, ImageType: res.data[0].profilePicture.contentsContentType
                  })
                }
                else {
                  alert("Get truck info" + res.status)
                }
              }).catch((error) => {
                console.log('Get truck info ', error)
              });
          })
          .catch(err => {
            console.log("TruckId token error ", err);
          });
      })
      .catch(err => {
        console.log("AuthId token error ", err);
      });
  }

  _logout = () => {
    var fbSignOut = util.fbLogout();
    fbSignOut.then((resp) => {
      console.log('LogOut response ', resp);
      AsyncStorage.clear();
      this.props.navigation.dispatch(resetToLogin)
    })
      .catch((error) => { Alert.alert(error); });
  }


  render() {
   // console.log("image", `data:${this.state.ImageType};base64,${this.state.ImageUrl}`)
    var icon =  { uri: `data:${this.state.ImageType};base64,${this.state.ImageUrl}` };
    return (
      <Container>
        <View style={{ flexDirection: 'row', width: '100%', height: '100%', }}>
          <View style={{ width: '80%', height: '100%' }}>
            <View
              style={{
                height: "30%",
                alignItems: "center",
                backgroundColor: "#1a5177",
                paddingTop: "5%"
              }}
            >
              <View
                style={{
                  borderRadius: util.getWidth(20),
                  width: util.getWidth(8),
                  height: util.getWidth(8),
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#fbe48d"
                }}
              >
         
                <Image source={icon} style={{height:60,width:100}} />
              </View>
              <View style={{ height: "30%", justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "500", marginTop: "3%" }}>
                  {" "}
                  {this.state.truckName}
                </Text>
                {/* <Text style={{ color: "white", fontSize: 10 }}>
                {" "}
                San Francisco, CA
              </Text> */}
              </View>
              <View style={{ height: "1%", width: '80%', backgroundColor: '#ffffff', marginTop: '2%' }}></View>
            </View>
            <Content style={{ backgroundColor: "#1a5177" }}>
              <View style={{ marginLeft: util.getHeight(3) }}>
                <List
                  dataArray={routers}
                  renderRow={data => {
                    return (
                      <ListItem style={{ borderBottomWidth: 0, padding: 3 }}
                        button
                        onPress={() => {
                          this.props.navigation.navigate(data.key)
                        }}
                      >
                        <Icons
                          name={data.icon}
                          size={20}
                          style={{ color: "white" }}
                        />
                        <Text
                          style={{
                            color: "white",
                            fontSize: 14,
                            fontWeight: "500",
                            marginLeft: util.getWidth(7)
                          }}
                        >
                          {data.text}
                        </Text>
                      </ListItem>
                    );
                  }}
                />
              </View>
            </Content>
            <Footer
              style={{
                backgroundColor: "#1a5177",
                height: "30%",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <View style={{ marginLeft: util.getHeight(3) }}>
                <List
                  dataArray={routers1}
                  renderRow={data => {
                    return (
                      <ListItem style={{ borderBottomWidth: 0, padding: 3 }}
                        button
                        onPress={() => {
                          data.key == 'Logout' ? Alert.alert(
                            'Logout',
                            'Are you sure want to Logout?',
                            [
                              { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                              { text: 'OK', onPress: () => this._logout() },
                            ],
                            { cancelable: false }
                          ) : this.props.navigation.navigate(data.key)
                        }}
                      >
                        <Icons
                          name={data.icon}
                          size={20}
                          style={{ color: "white" }}
                        />
                        <Text
                          style={{
                            color: "white",
                            fontSize: 14,
                            fontWeight: "500",
                            marginLeft: util.getWidth(7)
                          }}
                        >
                          {data.text}
                        </Text>
                      </ListItem>
                    );
                  }}
                />
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ color: "white", textAlign: "center", fontSize: 14 }}>
                    {" "}
                    FOLLOW NOMAD{" "}
                  </Text>
                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <Icons
                      size={17}
                      name="facebook-f"
                      style={{ color: "white", padding: 10 }}
                    />
                    <Icons
                      size={17}
                      name="instagram"
                      style={{ color: "white", padding: 10, marginLeft: 15 }}
                    />
                    <Icons
                      size={17}
                      name="twitter"
                      style={{ color: "white", padding: 10, marginLeft: 15 }}
                    />
                  </View>
                </View>
              </View>
            </Footer>
          </View>
          <View style={{ width: '20%', height: '100%', alignItems: 'center' }}>
            <TouchableOpacity style={{ marginTop: 3 }}

              onPress={() => this.props.navigation.closeDrawer()} >

              <Icon1 active name="menu" size={35} color="#2A409A" />
            </TouchableOpacity>
            <View style={{ height: '15%', width: "70%", marginBottom: '5%', position: "absolute", bottom: 0 }}>
              <Image style={{ height: '100%', width: '90%' }} source={images.makeImg} />
            </View>

          </View>
        </View>
      </Container>
    );
  }
}
