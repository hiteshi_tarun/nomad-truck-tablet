import React, { Component } from "react";
import { StyleSheet } from "react-native";
import CodePush from "react-native-code-push";

import { Container, Content, Text, View } from "native-base";
import Modal from "react-native-modalbox";
import MainStackRouter from "./routers/MainStackRouter";
import ProgressBar from "./components/loaders/ProgressBar";
import firebase from 'react-native-firebase';
import { Notification } from 'react-native-firebase';
import theme from "./themes/base-theme";
import util from "./common/Util";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null
  },
  modal: {
    justifyContent: "center",
    alignItems: "center"
  },
  modal1: {
    height: 300
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDownloadingModal: false,
      showInstalling: false,
      downloadProgress: 0
    };
  }
  
  componentDidMount() {
    CodePush.sync(
      { updateDialog: true, installMode: CodePush.InstallMode.IMMEDIATE },
      status => {
        switch (status) {
          case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
            this.setState({ showDownloadingModal: true });
            this._modal.open();
            break;
          case CodePush.SyncStatus.INSTALLING_UPDATE:
            this.setState({ showInstalling: true });
            break;
          case CodePush.SyncStatus.UPDATE_INSTALLED:
            this._modal.close();
            this.setState({ showDownloadingModal: false });
            break;
          default:
            break;
        }
      },
      ({ receivedBytes, totalBytes }) => {
        this.setState({ downloadProgress: receivedBytes / totalBytes * 100 });
      }
    );
    firebase.messaging().hasPermission()
    .then(enabled => {
      if (enabled) {
        firebase.messaging().getToken().then(token => {
          console.log("LOG token: ", token);
         
          util.setStore('DeviceToken', token)
          .then(res => {

          })
          .catch((error) => {
            console.log('Token Error ', error)
        });
        })
        // user has permissions
      } else {
        firebase.messaging().requestPermission()
          .then(() => {
            alert("User Now Has Permission")
          })
          .catch(error => {
            alert("Error", error)
            // User has rejected permissions  
          });
      }
    });

    
this.notificationListener = firebase.notifications().onNotification((notification) => {

  // Process your notification as required
  const {
    body,
    data,
    notificationId,
    sound,
    subtitle,
    title
  } = notification;
  console.log("LOG Check: ", title, body, JSON.stringify(data))
});




}
componentWillUnmount() {

this.notificationListener();
}
componentWillMount() {
  this.notificationListener = firebase.notifications().onNotification((notification) => {

    // Process your notification as required
    const {
      body,
      data,
      notificationId,
      sound,
      subtitle,
      title
    } = notification;
    console.log("LOG Check: ", title, body, JSON.stringify(data))
  });
  
 
  }
   

  render() {
    if (this.state.showDownloadingModal) {
      return (
        <Container
          theme={theme}
          style={{ backgroundColor: theme.defaultBackgroundColor }}
        >
          <Content style={styles.container}>
            <Modal
              style={[styles.modal, styles.modal1]}
              backdrop={false}
              ref={c => {
                this._modal = c;
              }}
              swipeToClose={false}
            >
              <View
                style={{
                  flex: 1,
                  alignSelf: "stretch",
                  justifyContent: "center",
                  padding: 20
                }}
              >
                {this.state.showInstalling
                  ? <Text
                      style={{
                        color: theme.brandPrimary,
                        textAlign: "center",
                        marginBottom: 15,
                        fontSize: 15
                      }}
                    >
                      Installing update...
                    </Text>
                  : <View
                      style={{
                        flex: 1,
                        alignSelf: "stretch",
                        justifyContent: "center",
                        padding: 20
                      }}
                    >
                      <Text
                        style={{
                          color: theme.brandPrimary,
                          textAlign: "center",
                          marginBottom: 15,
                          fontSize: 15
                        }}
                      >
                        Downloading update...
                        {" "}
                        {`${parseInt(this.state.downloadProgress, 10)} %`}
                      </Text>
                      <ProgressBar
                        color="theme.brandPrimary"
                        progress={parseInt(this.state.downloadProgress, 10)}
                      />
                    </View>}
              </View>
            </Modal>
          </Content>
        </Container>
      );
    }

    return <MainStackRouter />;
  }
}

export default App;


// https://medium.com/@paul.allies/react-native-firebase-cloud-messaging-ios-97b59e5f28ec                                                  