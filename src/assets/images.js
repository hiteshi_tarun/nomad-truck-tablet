const images = {
	logoImg: require('./imgs/app_icon.png'),
	fullLogo: require('./imgs/full_logo.png'),
	burgerIcon: require('./imgs/tilburger.jpg'),
	splashIcon: require('./imgs/splash_icon.png'),
	bg_l: require('./imgs/bg_l.png'),
	bg_p: require('./imgs/bg_p.png'),
	makeImg: require('./imgs/order.png'),
	makeImg_p: require('./imgs/order_p.png'),
};


export default images;
