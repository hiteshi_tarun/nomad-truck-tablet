
const React = require('react-native');
import theme from "src/themes/base-theme";
import util from '../../common/Util'
// const { StyleSheet, Dimensions } = React;
// const deviceHeight = Dimensions.get('window').height;

export default {
  container: {
    flex: 1,
    // backgroundColor: theme.appBgColor,
  },
  content:{
    margin:15
  },
  defaultBlueColor:{
    color:'#2A409A',
  },
  defaultGrayColor:{
    color:'#BBBEC0',
  },
  defaultBlueColorText:{
    color:'#2A409A',
    fontWeight:'500',
    fontSize: 18
  },
  headerTitleText:{
    color:'#2A409A', 
    fontSize:14, 
    textAlign:'center'
  },
  appHeadColor:{
    color:'#1a5177',
  },
  viewCenter:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewFull:{
    height:'100%',
    width:'100%'
  },
};
