// const baseUrl = "https://nomadstage.ny-1.paas.massivegrid.net"
const baseUrl = "https://staging.nomadapplication.com/nomadorders";

import util from "./Util";
import moment from "moment"
const stripeTokenForTruck = 'rk_live_UtTsXAKeBErJQpxj6Ouk0DMR';// truck

var serializeJSON = function (data) {
  return Object.keys(data).map(function (keyName) {
    return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
  }).join('&');
}
var ApiManager = {
  //Call from login/signUp screen 
  _getAuthToken: async function (email, uid) {
    console.log('Register Email token ', email, 'uid', uid)
    const url = baseUrl + "/mobile/authentication/truck/authenticate";
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        "email": email,
        "idToken": uid
      })
    });
  },

  _updateCurrentLocation: async function (authToken, lat, long, truckId) {
    //POST /api/geo-locations/truck/update-last-location
    const url = baseUrl + "/api/geo-locations/truck/update-last-location";
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + authToken
      },
      body: JSON.stringify({
        truckId: truckId,
        newLocation: {
          lat: lat,
          lng: long
        }
      })
    });
  },
  //Call from login/signUp screen 
  _registerUser: async function (DeviceToken, AuthToken, TruckID, truckName, email, phone, firstName, lastName, BusinessName, street, addStreet, city,
    zipCode, Country, state, BusinessOwnerFirstName, BusinessOwnerLastName, password, ein, date_of_birth, ssnNumber) {

    const url = baseUrl + "/api/food-trucks";
    return await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "business": { // <-- MUST NOT HAVE AN ID
          "agreedTermsOfService": true,
          "agreedToSOn": moment(new Date()).format('YYYY-MM-DDTHH:MM:SSZ'),
          "contact": {
            "city": city,
            "country": Country,
            "phonenumber": phone,
            "state": state,
            "street1": street,
            "street2": addStreet,
            "zipCode": zipCode
          },
          "dateOfBirth": date_of_birth, // PLEASE PUT IN THIS FORM 1970-01-01 YEAR-MONTH-DATE
          "businessOwnerFirstName": BusinessOwnerFirstName,
          "lastFourDigitsSSN": ssnNumber,
          "businessOwnerLastName": BusinessOwnerLastName,
          "name": BusinessName,
          "taxNumber": ein,
          "tosIpAddress": DeviceToken
        },
        "email": email,
        "id": TruckID, // <-- TRUCK ID, needs to be set. This is the only allowed ID
        "firstName": firstName,
        "lastName": lastName,
        "truckName": truckName
      }
      )
    });
  },

  _getAllFoodMenu: async function (TruckID, AuthToken) {
    // GET /v1/customers/{ID}/sources?object=card  , customerID = cus_DTDww1Rh0GYEUA
    //By stripe account get all saved card details-
    //  const url = "https://api.stripe.com/v1/customers/cus_DTDww1Rh0GYEUA/sources?object=card";
    const url = baseUrl + "/api/food-menu/truck/" + TruckID;
    console.log("_getAllFoodMenu", url);
    console.log("_getAllFoodMenu authToken", AuthToken);

    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + AuthToken
      }
    });
  },

  _getCurrentFoodOrders: async function (TruckID, AuthToken) {

    const url = baseUrl + "/api/current-food-orders/truck/" + TruckID;
    console.log("_getCurrentFoodOrders", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + AuthToken
      }
    });
  },

  _getLastFoodOrders: async function (TruckID, AuthToken) {

    const url = baseUrl + "/api/last-food-orders/truck/" + TruckID;
    console.log("_getLastFoodOrders", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + AuthToken
      }
    });
  },

  _getTruckInfo: async function (TruckID, AuthToken) {

    const url = baseUrl + "/api/businesses/public-info/" + TruckID;
    console.log("_getTruckInfo url", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + AuthToken
      }
    });
  },

  //Get truck description 
  _getTruckDescription: async function (TruckID, AuthToken) {

    const url = baseUrl + "/api/food-trucks/public-profiles/" + TruckID;
    console.log("_getTruckDescription url", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + AuthToken
      }
    });
  },

  // Update truck description
  _updateTruckDescription: async function (description, id, truckName, AuthToken) {
    const url = baseUrl + "/api/food-trucks/update-public-info";
    console.log("truck _UpdateTruckDescription JSON", {
      "description": description,
      "id": id,
      "truckName": truckName
    })
    return await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "description": description,
        "id": id,
        "truckName": truckName
      })
    });
  },

  // Update truck description
  _getFoodMenuItems: async function (truckID, AuthToken) {
    const url = baseUrl + "/api/food-menu-items/truck/" + truckID;
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
    });
  },

  // Add Items
  _addFoodMenuItems: async function (truckID, AuthToken, description, name, price, totalStockCount) {
    const url = baseUrl + "/api/food-menu-items/" + truckID;
    console.log(AuthToken, "_addFoodMenuItems", url);
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "description": description,
        "enabled": true,
        "foodMenu": {
          "id": truckID
        },
        "name": name,
        "price": price,
        "totalStockCount": totalStockCount
      })
    });
  },

  // edit Items
  _editFoodMenuItems: async function (truckID, AuthToken, itemId, description, name, price, totalStockCount) {
    const url = baseUrl + "/api/food-menu-items/";
    console.log("JSON _editFoodMenuItems", {
      "description": description,
      "enabled": true,
      "foodMenu": {
        "id": truckID
      },
      "id": itemId,
      "name": name,
      "price": price,
      "totalStockCount": totalStockCount

    })
    return await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "description": description,
        "enabled": true,
        "foodMenu": {
          "id": truckID
        },
        "id": itemId,
        "name": name,
        "price": price,
        "totalStockCount": totalStockCount

      })
    });
  },

  // Accept state of order...
  _sendAcceptOrderState: async function (AuthToken, orderId, orderDbId) {
    const url = baseUrl + "/mobile/secure/truck/order/events/accept";

    console.log('_sendAcceptOrderState ', url);
    console.log('_sendAcceptOrderState data pass ', {
      "orderDbId": parseInt(orderDbId),
      "orderId": orderId,
      "orderState": "ACCEPTED"
    })
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "orderDbId": parseInt(orderDbId),
        "orderId": orderId,
        "orderState": "ACCEPTED"

      })
    });
  },

  _sendDeclineOrderState: async function (AuthToken, orderId, orderDbId) {
    const url = baseUrl + "/mobile/secure/truck/order/events/decline";
    console.log('_sendDeclineOrderState ', url)
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "orderDbId": parseInt(orderDbId),
        "orderId": orderId,
        "orderState": "DECLINED"

      })
    });
  },

  _sendFinishOrderState: async function (AuthToken, orderId, orderDbId) {
    const url = baseUrl + "/mobile/secure/truck/order/events/finish";
    console.log('_sendDeclineOrderState ', url)
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "orderDbId": parseInt(orderDbId),
        "orderId": orderId,
        "orderState": "READY"

      })
    });
  },
  // send squareCardNonce with the help of below API
  _sendCardNonce: async function (AuthToken, truckID, accessToken) {
    console.log('Square Card Nonce ', accessToken)
    const url = baseUrl + "/api/additional-credentials/food-truck/" + truckID;
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "credentialsType": "SQUARE",
        "password": accessToken,
        "username": "oauth"

      })
    });
  },

  _getReaderLocationID: async function (AuthToken, truckID) {
    console.log('Square Card _getLocationID ', AuthToken)
    const url = baseUrl + "/api/additional-credentials/food-truck/" + truckID;
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },

    });
  },

  // send squareCardNonce with the help of below API https://connect.squareup.com/mobile/authorization-code
  _createSquareMobileAuthorizationCode: async function (location) {
    const url = "https://connect.squareup.com/mobile/authorization-code";
    console.log("_CreateSquareMobileAuthorizationCode", location)
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer sq0atp-alPyXo6bAdXrxqx3d9M9fg"
      },
      body: JSON.stringify({
        "location_id": location
      })
    });
  },

  // send squareCardNonce with the help of below API https://connect.squareup.com/mobile/authorization-code
  _payNowTrigger: async function (authToken, TruckId) {
    const url = baseUrl + "/api/businesses/trigger-payout/" + TruckId;
    console.log("_passAccountToken url", url);
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + authToken
      },
    });
  },

  //Add card functions

  _getStripeToken: async function (cHName, cCardNumber, expMonth, expYear, cvcNumber) {
    console.log(" Card All Details: ", cHName, cCardNumber, expMonth, expYear, cvcNumber)
    const url = "https://api.stripe.com/v1/tokens";
    var cardDetails = {
      "card[name]": cHName,
      "card[number]": parseInt(cCardNumber),
      "card[exp_month]": parseInt(expMonth),
      "card[exp_year]": parseInt(expYear),
      "card[cvc]": parseInt(cvcNumber),
    };
    console.log("URL ", url, " Card details ", cardDetails);
    // sk_test_4eC39HqLyjWDarjtT1zdp7dc test id
    // rk_test_KfCxjzpO7IJa2CxLNxbMrd9J stripe id given by client
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer rk_test_KfCxjzpO7IJa2CxLNxbMrd9J"
      },
      body: serializeJSON(cardDetails)
    });
  },

  _getAccountStripeToken: async function (Routing_number, account_number) {
    console.log(" Account All Details: ", Routing_number, account_number)
    const url = "https://api.stripe.com/v1/tokens";
    var cardDetails = {
      "bank_account[country]": "US",
      "bank_account[currency]": "usd",
      "bank_account[routing_number]": Routing_number,
      "bank_account[account_number]": account_number

    };
    console.log("URL ", url, " Card details ", cardDetails);
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer rk_test_KfCxjzpO7IJa2CxLNxbMrd9J"
      },
      body: serializeJSON(cardDetails)
    });
  },

  _passAccountToken: async function (authToken, TruckId, token) {
    console.log("_passAccountToken request ", authToken, TruckId, token)
    const url = baseUrl + "/api/businesses/update-payment/" + TruckId;
    console.log("_passAccountToken url", url);
    return await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + authToken
      },
      body: JSON.stringify({
        "accountToken": token,
        "truckId": TruckId
      })
    });
  },

  _getBankAccount: async function (authToken, TruckId) {
    console.log("_passAccountToken request ", authToken, TruckId)
    const url = baseUrl + "/api/businesses/bankaccount/" + TruckId;
    console.log("_passAccountToken url", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + authToken
      },
    });
  },

  _subCard: async function (AuthToken, TruckId, cardToken, subscription) {

    const url = baseUrl + "/api/payment-data/truck/payout-and-subscription-config/" + TruckId;
    console.log("_subCard request ", {
      "accountNumber": cardToken,
      "subscription": subscription
    })
    console.log("_addCard", url);
    return await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "accountNumber": cardToken,
        "subscription": subscription
      })
    });
  },

  _getCardDetails: async function (AuthToken, TruckId) {
    const url = baseUrl + "/api/payment-data/food-truck/" + TruckId;
    console.log("_getCardDetails", url);
    return await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
    });
  },

  _subscriptionDetails: async function (AuthToken, TruckId, amount) {
    console.log("_subscription amount ", amount)
    const url = baseUrl + "/api/businesses/set-subscription/" + TruckId;
    console.log("_subscriptionDetails", url);
    return await fetch(url, {
      method: "POST ",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "accountNumber": "token"
      })
    });
  },

  //_postShoppingCart API
  _postShoppingCart: async function (AuthToken, UUID, squareCardNonce, totalMoneyAmount, createdAt, totalTipMoneyAmount) {

    const url = baseUrl + "/api/food-orders/trigger-in-person/" + squareCardNonce;
    console.log("_addCard", url);
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + AuthToken
      },
      body: JSON.stringify({
        "id": 26,
        "orderId": UUID,
        "createdOn": createdAt,
        "totalPrice": totalMoneyAmount,
        "nomadFee": null,
        "isNotified": null,
        "tipAmount": totalTipMoneyAmount,
        "orderStatus": {
          "id": 26,
          "createdOn": createdAt,
          "status": "SHOPPING_CART"
        },
        "notified": null
      })
    });
  },
}

module.exports = ApiManager