import LocalizedStrings from 'react-native-localization';

const keyStrings = new LocalizedStrings({

    en: {

        //Common Text
        yesBtnText: 'Yes',
        noBtnText: 'No',
        totalText: 'Total',
        loadingText: 'Loading...',

        //Login Screen
        noaccountText: "Don't have an account?",
        userNlable: "Username",
        passWlable: "Password",
        loginBtnText: "LOGIN",
        //Signup Screen
        titleText: "Create an Account",
        truckName: "Truck's name",
        email: "Email",
        phone: 'Phone',
        firstName: "First Name",
        lastName: "Last Name",

        BusinessName: "Business Name",
        street: "Street",
        addStreet: 'Additional Street',
        city: 'City',
        Country: 'Country',
        state: 'State',
        zipCode: 'Zip Code',

        BusinessOwnerFirstName: "Business Owner First Name",
        BusinessOwnerLastName: "Business Owner Last Name",
        password: 'Password',
        rePassword: "Repeat Password",
        ein: 'Emaployer Identification Number (EIN)',
        date_of_birth: 'Date of Birth',
        ssnNumber: 'Last 4 Digits of SSN',
        nextBtn: "NEXT",
        signUpBtn: 'SIGN UP',

        //Square Login Screen
        squareUname: 'SQUARE USERNAME',
        squarePword: 'SQUARE PASSWORD',

        // ItemCheckOut Tab screen strings
        removeItemText: 'Are you sure want to remove this item?',
        checkoutBtnText: 'Checkout',
        cancelBtnText: 'Cancel',

        // Order Detail screen strings
        pickUpMessage: 'Are you sure want to make this order ready for pick up?',
        preparingMessage: 'Are you sure want to start preparing this order?',
        declineMessage: 'Are you sure want to decline this order?',
        detailsMessage: 'Click on an order to see details here.',
        reasonInputPlaceholder: 'Reason (optional)',
        customerText: 'Customer name',
        // -> Button text
        readyBtn: 'READY',
        preparingBtn: 'BEGIN PREPARING',
        declineBtn: 'DECLINE',

        // -> Order Status massage  
        receivedText: 'RECEIVED',
        deliveredText: 'DELIVERED'

    },
    de: {
        loginTitle: "Login ",
    }

})

export default keyStrings