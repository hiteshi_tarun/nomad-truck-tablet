import Database from "./Schema";

const DatabaseFunctions = {

  createSchema: async function() {
  const db = Database.getConnection();
  console.log('table creating')
	await db.transaction(tx => {
      tx.executeSql(
          "create table if not exists register (id integer primary key not null, " + 
          "user_id text , " +
          "email text, " +
          "ein text, " +
          "password text " +
          "first_name text,"+
           "last_name text,"+ 
           "truck_name text,"+
            "business_name text,"+
            "street_name text,"+
            "additional_street text,"+
            "city_name text,"+
            "zipcode text,"+
            "country text,"+
            "state text,"+
            "date_of_birth text,"+
            "SSNnumber text,"+
            "BusinessOwner_firstName text,"+
            "BusinessOwner_lastName text,"+
          ");");
      //   Add all the schemas here and call this function from the Splash or login/Register component
    });
  },
  fetchDataFromTable: async function() {
    const db = Database.getConnection();
    return await db.transaction(tx => {
      tx.executeSql("SELECT * FROM register", [], (tx, results) => { 
        console.log("Query completed", results);
        // Get rows with Web SQL Database spec compliance.
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          let row = results.rows.item(i);
          console.log(`Register name: ${row.full_name}, Dept Name: ${row.email}`);
        }
      });
    });
  },


  insertRegisterRecords: async function( fbUId, truckName,email,phone,firstName,lastName,BusinessName, street,addStreet,city, zipCode,Country,state,BusinessOwnerfirstName,BusinessOwnerlastName,password,ein,date_of_birth,ssnNumber) {
    console.log('insertRegisterRecords ', truckName)
  const db = Database.getConnection();
  console.log('insertRegisterRecords ', truckName)
    return await db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO register (user_id, first_name, last_name, truck_name, email,business_name,street,additional_street,city,zipcode,country,state,BusinessOwner_firstName,BusinessOwner_lastName,password,ein,date_of_birth,SSNnumber) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);',
        [fbUId,firstName,lastName,truckName,email, BusinessName, street,addStreet,city,
          zipCode,Country,state,password,ein,date_of_birth,ssnNumber]
      ,(_,res)=>{
        console.log("ins res",res)
      },err=>console.log("ins err",err));
    });
  },

  dropTable: async function() {
    const db = Database.getConnection();
      return await db.transaction(tx => {
        tx.executeSql(
          'DROP TABLE IF EXIST user_data',
          (_,res)=>{
          console.log("TABLE DROP ",res)
        },err=>console.log("TABLE DROP err",err));
      });
    }
};

module.exports = DatabaseFunctions;
