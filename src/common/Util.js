"use strict";

import { Dimensions, Platform, AsyncStorage } from "react-native";

// const config = {
//   // apiKey: "AIzaSyAYnzbc5_rrgJx1wYd1l0BsB8EeWjUb178",
//   // authDomain: "nomad-204811.firebaseapp.com",
//   // databaseURL: "https://nomad-204811.firebaseio.com",
//   // projectId: "nomad-204811",
//   // storageBucket: "",
//   // messagingSenderId: "950334060002"

//   //My Account credentials
//   // apiKey: "AIzaSyDhhCGR1fxYt_TuRQFVFh-EMm5GzE1nU18",
//   // authDomain: "ftdstruckapp.firebaseapp.com",
//   // databaseURL: "https://ftdstruckapp.firebaseio.com",
//   // projectId: "ftdstruckapp",
//   // storageBucket: "ftdstruckapp.appspot.com",
//   // messagingSenderId: "513240199105"
//   //New credentials by client
//     apiKey: "AIzaSyB9PBmLSrvpCFTUYlJSsDk4j32JEHfUK3o",
//     authDomain: "nomadapp-209616.firebaseapp.com",
//     databaseURL: "https://nomadapp-209616.firebaseio.com",
//     projectId: "nomadapp-209616",
//     storageBucket: "nomadapp-209616.appspot.com",
//     messagingSenderId: "478645998439"
// };


// const firebase = require("firebase");
//       firebase.initializeApp(config);
// const database = firebase.database();
import firebase from 'react-native-firebase';
const Util = {
  getHeight: percente => {
    percente = !percente ? 100 : percente;
    return (Util.getWindowSize().height * percente) / 100;
  },
  getWidth: percente => {
    percente = !percente ? 100 : percente;
    return (Util.getWindowSize().width * percente) / 100;
  },
  getHeightL: percente => {
    percente = !percente ? 100 : percente;
      AsyncStorage.getItem('height').then((value)=>{
        console.log("height",data) 
        return (JSON.parse(value* percente) / 100)
      } )
  },
  getWidthL: (percente) => {
     percente = !percente ? 100 : percente;
      AsyncStorage.getItem('width').then((value)=>{
        console.log("width",JSON.parse(value* percente) / 100) 
        return (JSON.parse(value* percente) / 100)
      } )
  },
  getWindowSize: () => {
    return Dimensions.get("window");
  },
  getOS: () => {
    return Platform.OS ? Platform.OS : "ios";
  },
  emailPatternMatcher:(email) =>{
    console.log("eMAIL VALUE ", email)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(reg.test(email) === false){
        return false;
    }else{
      return true;
    }
  },
  firbaseAuthToken: () => {
    return new Promise ((resolve)=>{
      resolve(
          firebase.auth()
      )
     })
  },
  
  
  firbaseGetCurrentUser: (cb) => {
    var auth = firebase.auth()
     console.log("auth in util", auth)
    // auth.onAuthStateChanged((user)=>cb(user))
  },

  fbLogout: ()=>{
    return new Promise((resolve,reject)=>{
      firebase.auth().signOut()
      .then(() => {
         console.log('SignOut')
         resolve()
      })
      .catch((error) => {
        console.log('SignOut error ', error)
        reject()
      });
    })
  },
  fireBase: {
    firebaseInit: () => {
      return firebase;
    },
    firebaseLogin: (email, password) => {
      return new Promise((resolve,reject) => {
         firebase.auth()
          .signInAndRetrieveDataWithEmailAndPassword(email, password)
          .then(res => {
            console.log(res);
            resolve(res);
          })
          .catch(res => {
            console.log(res);
            reject(res);
          });
      });
    },
    
    firebaseUid: ()=>{
      console.log('In funcitons');
      const fbCUserUid = firebase.auth().currentUser;
      console.log(' UId from fun ',fbCUserUid.uid);
      return fbCUserUid.uid;
    },
   
    firebaseRegister: (email,password) =>{
      return new Promise((resolve,reject) => {
   
        firebase.auth()
         .createUserAndRetrieveDataWithEmailAndPassword(email, password)
         .then(res => {
           console.log(res);
           const fbCuser = firebase.auth().currentUser;
               console.log(' UId ',fbCuser.uid);
              
              //  firebase.database().ref(fbCuser.uid).set({
              //   "Firstname": firstName,
              //   "Lastname": lastName,
              //   "TruckName": truckName,
              //   "Email": email,
              //   "Phone":phone,
              //   "BusinessName": BusinessName,
              //   "Atreet": street,
              //   "AdditionalStreet":addStreet,
              //   "City":city,
              //   "ZipCode": zipCode,
              //   "Country": Country,
              //   "State":state,
              //   "EIN": ein,
              //   "Date_of_birth": date_of_birth,
              //   "SsnNumber":ssnNumber
              //  })
              //  firebase.database().ref(fbCuser.uid+"/"+"Email").set(email)
              //  firebase.database().ref(fbCuser.uid+"/"+"Password").set(password)
              //  firebase.database().ref(fbCuser.uid+"/"+"TaxId").set(eid)
           resolve(res);
         })
         .catch(res => {
           console.log(res);
           reject(res);
         });
     });
    }
    },
    setStore: (key, value)=>{
        try {
          AsyncStorage.setItem(key, value);
          console.log('Set Asyn Value ', value)
        } catch (error) {
        // Error saving data
          console.log('not store in persistent data');
      }
      },
    getValueFromStore: (key)=>{
        return new Promise((resolve,reject) => {
          console.log('In promis fun')
          AsyncStorage.getItem(key).then(result =>{
            console.log('getStored Value ', result)
            resolve(result)
          }).catch( err =>{
            console.log('error ',err);
            reject(err)
          })
        })
    }
};

module.exports = Util;
