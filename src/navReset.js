
import { NavigationActions,StackActions } from "react-navigation";

export const resetToMain = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'First'})
  ],
  key: null
});

export const resetToLogin = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Login'})
  ],
  key: null
});

export const resetToDrawNav = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'DrawStack'})
  ],
  key: null
});

export const resetToAuthStack = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'authStack'})
  ],
  key: null
});

export const resetToInfoStack = StackActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'infoStack'})
  ],
  key: null
});


